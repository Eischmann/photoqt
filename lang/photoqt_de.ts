<?xml version="1.0" ?><!DOCTYPE TS><TS language="de" version="2.1">
<context>
    <name>About</name>
    <message>
        <location filename="../../qml/other/About.qml" line="10"/>
        <source>About PhotoQt</source>
        <translation>Über PhotoQt</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="36"/>
        <source>PhotoQt is a simple image viewer, designed to be good looking, highly configurable, yet easy to use and fast.</source>
        <translation>PhotoQt ist ein Bildbetrachter mit dem Ziel gut auszusehen und intuitiv und schnell benutzbar zu sein, gleichzeitig jedoch höchst konfigurierbar zu bleiben.</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="38"/>
        <source>Another image viewer?</source>
        <translation>Ein weiterer Bildbetrachter?</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="39"/>
        <source>There are many good image viewers out there. But PhotoQt is a little different than all of them. Its interface is kept very simple, yet there is an abundance of settings to turn PhotoQt from AN image viewer into YOUR image viewer.</source>
        <translation>Es gibt viele gute Bildbetrachter. Aber PhotoQt ist nicht wie die anderen. Die Oberfläche ist einfach gehalten, doch gleichzeitig gibt es Unmengen an Einstellungen um das Aussehen und Verhalten anzupassen und PhotoQt zu DEINEM Bildbetrachter zu machen.</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="40"/>
        <source>Occasionally someone comes along because they think PhotoQt is &apos;like Picasa&apos;. However, if you take a good look at it then you see that they are in fact quite different. I myself have never used Picasa, and don&apos;t have any intention to copy Picasa. With PhotoQt I want to do my own thing, and to do that as good as I can.</source>
        <translation>Hin und wieder kommt es vor, dass jemand PhotoQt mit Picasa vergleicht. Wenn man aber genauer hinschaut sieht man dass beide doch ziemlich verschieden sind. Ich selbst habe Picasa nie verwendet und habe auch nicht vor Picasa zu kopieren. Mit PhotoQt möchte ich mein eigenes Ding machen, und das so gut wie ich kann.</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="42"/>
        <source>So then, who are you?</source>
        <translation>Und wer bist du dann?</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="47"/>
        <source>If you find a bug or if you have a question or suggestion, please tell me. I&apos;m open to any feedback I get!</source>
        <translation>Wenn du einen Bug findest oder einen Vorschlag hast, dann schreib es mir. Ich freue mich über Rückmeldung jeder Art!</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="99"/>
        <source>If you want to support PhotoQt with a donation, you can do so via PayPal here:</source>
        <translation>Wenn du PhotoQt mit einer Spende unterstützen möchtest, dann kann du dies hier mit PayPal tun:</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="123"/>
        <source>Okay, take me back</source>
        <translation>Okay, bring mich zurück</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="45"/>
        <source>Don&apos;t forget to check out the website:</source>
        <translation>Nicht vergessen, einen Blick auf die Internetseite zu werfen:</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="43"/>
        <source>I am Lukas Spies, the sole developer of PhotoQt. Born and raised in the southwest of Germany, I left my home country for university shortly after finishing school. Since then I have live for some years in Ireland, Canada, USA, and France, studying and doing research in Mathematics and Scientific Computing. I started playing around with programming since I was about 15 years old. So most of my programming knowledge is self-taught through books and websites. The past few years of my studies I also did a lot of programming as part of my research. Through all of that I gained a good bit of experience in programming using different programming languages. This becomes especially apparent when looking at how PhotoQt has changed since it started at the end of 2011.</source>
        <translation>Ich bin Lukas Spies, der alleinige Entwickler von PhotoQt. Ich bin im Südwesten von Deutschland aufgewachsen, habe aber mein Heimatland kurz nach dem Ende der Schule verlassen um im Ausland zu studieren. Seitdem habe ich für einige Jahre in Irland, Kanada, USA und in Frankreich gelebt, studiert und geforscht. Mein Studiengebiet ist Mathematik und Scientific Computing. Mit 15 Jahren habe ich angefangen zu programmieren und habe ich mir daher den Großteil meines Programmier-Wissens mit Büchern und durch Webseiten selbst beigebracht. Die letzten paar Jahre meines Studiums habe ich auch für meine Forschung viel programmiert. Durch all das habe ich viel wertvolle Erfahrung mit verschiedenen Programmiersprachen sammeln können. Diese Entwicklung ist besonders daran deutlich wie PhotoQt sich seit Ende 2011 entwickelt hat.</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="82"/>
        <source>Thanks to everybody who contributed to PhotoQt and/or translated PhotoQt to another language! You guys rock!</source>
        <translation>Danke an alle, die zu PhotoQt beigetragen und/oder es in eine andere Sprache übersetzt haben! Ihr seid super!</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="99"/>
        <source>You want to join the team and do something, e.g. translating PhotoQt to another language? Drop me and email (%1), and for translations, check the project page on Transifex:</source>
        <extracomment>Don't forget to add the %1 in your translation, it is a placeholder for the email address!!</extracomment>
        <translation>Du möchtest Teil des Teams werden und auch etwas beitragen, z.B. Übersetzen von PhotoQt in eine andere Sprache? Schreib mir eine E-Mail (%1), und für Übersetzungen siehe dir die Projektseite bei Transifex an: http://transifex.com/p/photo.</translation>
    </message>
</context>
<context>
    <name>Animation</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="18"/>
        <source>Animation and Window Geometry</source>
        <translation>Animationen und Fenstergeometrie</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="19"/>
        <source>Save and restore of Window Geometry: On quitting PhotoQt, it stores the size and position of the window and can restore it the next time started.</source>
        <translation>Fenstergeometrie speichern und wiederherstellen: Wenn PhotoQt beendet wird, dann speichert es die Größe und Position des Fensters und kann dies wiederherstellen beim nächsten Start.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="19"/>
        <source>Keep PhotoQt above all other windows at all time</source>
        <translation>Halte PhotoQt immer im Vordergrund</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="19"/>
        <source>There are three things that can be adjusted here:</source>
        <translation>Es gibt drei Dinge, die hier angepasst werden können:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="19"/>
        <source>Animations of elements and items (like fade-in, etc.)</source>
        <translation>Animationen der Elemente und Fenster (z.B. das Einblenden, etc.)</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="33"/>
        <source>Enable Animations</source>
        <translation>Animationen aktivieren</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="41"/>
        <source>Save and restore window geometry</source>
        <translation>Fenstergeometrie speichern und wiederherstellen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="49"/>
        <source>Keep above other windows</source>
        <translation>Über anderen Fenstern halten</translation>
    </message>
</context>
<context>
    <name>Available</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Available.qml" line="104"/>
        <source>Click to add shortcut</source>
        <translation>Klicke hier um einen Kurzbefehl hinzuzufügen</translation>
    </message>
</context>
<context>
    <name>Background</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="18"/>
        <source>Background</source>
        <translation>Hintergrund</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="19"/>
        <source>The background of PhotoQt is the part, that is not covered by an image. It can be made either real (half-)transparent (using a compositor), or faked transparent (instead of the actual desktop a screenshot of it is shown), or a custom background image can be set, or none of the above. Please note: Fake transparency currently only really works when PhotoQt is run in fullscreen/maximised!</source>
        <translation>Der Hintergrund PhotoQts ist der Teil, der nicht von einem Bild bedeckt wird. Dieser kann entweder echt (halb-)transparent (mittels Compositor) dargestellt werden, falsch-transparent (anstatt der tatsächlichen Arbeitsfläche wird ein Bildschirmfoto davon gezeigt), oder es kann auch ein benutzerdefiniertes Hintergrundbild gesetzt werden. Oder auch keines von all dem. Bitte beachte: Der falsch-tranparente Hintergrund funktioniert nur richtig im Vollbildmodus.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="37"/>
        <source>(Half-)Transparent background</source>
        <translation>(Halb-)transparenter Hintergrund</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="43"/>
        <source>Faked transparency</source>
        <translation>Falsche Transparenz</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="48"/>
        <source>Custom background image</source>
        <translation>Eigenes Hintergrundbild</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="53"/>
        <source>Monochrome, non-transparent background</source>
        <translation>Einfarbigen, nicht-transparenten Hintergrund verwenden</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="99"/>
        <source>No image selected</source>
        <translation>Kein Bild ausgewählt</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="124"/>
        <source>Scale to fit</source>
        <extracomment>Refers to a background image, scale it to fit</extracomment>
        <translation>Skalieren</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="131"/>
        <source>Scale and Crop to fit</source>
        <extracomment>Refers to a background image, crop and scale it to fit perfectly</extracomment>
        <translation>Skalieren und beschneiden</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="137"/>
        <source>Stretch to fit</source>
        <extracomment>Refers to a background image, stretch it to fit perfectly</extracomment>
        <translation>Strecken</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="143"/>
        <source>Center image</source>
        <extracomment>Refers to a background image, center it</extracomment>
        <translation>Bild zentrieren</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="149"/>
        <source>Tile image</source>
        <extracomment>Refers to a background image, tile it to fill everything</extracomment>
        <translation>Bild kacheln</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/Background.qml" line="67"/>
        <source>Open a file to begin</source>
        <translation>Eine Datei öffnen, um zu starten</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/Background.qml" line="83"/>
        <source>Folder is now empty</source>
        <translation>Der Ordner ist jetzt leer</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/Background.qml" line="99"/>
        <source>No image matches selected filter</source>
        <translation>Kein Bild stimmt mit dem gewählten Filter überein</translation>
    </message>
</context>
<context>
    <name>BorderAroundImage</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/BorderAroundImage.qml" line="18"/>
        <source>Margin Around Image</source>
        <translation>Abstand um das Bild</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/BorderAroundImage.qml" line="19"/>
        <source>Whenever you load an image, the image is per default not shown completely in fullscreen, i.e. it&apos;s not stretching from screen edge to screen edge. Instead there is a small margin around the image of a couple pixels. Here you can adjust the width of this margin (set to 0 to disable it).</source>
        <translation>Wenn immer ein Bild geladen wird, dann wird es nicht ganz als Vollbild angezeigt, d.h. es reicht nicht von Bildschirmrand zu Bildschirmrand. Stattdessen befindet sich ein kleiner Rahmen von einigen Pixeln um das Bild herum. Hier kann die Breite dieses Rahmen angepasst werden.</translation>
    </message>
</context>
<context>
    <name>BreadCrumbs</name>
    <message>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="60"/>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="68"/>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="109"/>
        <source>Go backwards in history</source>
        <extracomment>The history is the list of visited folders in the element for opening files</extracomment>
        <translation>Zurück in der Chronik</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="69"/>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="101"/>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="110"/>
        <source>Go forwards in history</source>
        <extracomment>The history is the list of visited folders in the element for opening files</extracomment>
        <translation>Vorwärts in der Chronik</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="141"/>
        <source>Close element</source>
        <extracomment>The element in this case is the element for opening files</extracomment>
        <translation>Element schließen</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="228"/>
        <source>Go directly to subfolder of</source>
        <extracomment>Used as in &quot;Go directly to subfolder of '/path/to/somewhere'&quot;</extracomment>
        <translation>Gehe direkt zum Unterordner von</translation>
    </message>
</context>
<context>
    <name>Cache</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="17"/>
        <source>Thumbnail Cache</source>
        <translation>Zwischenspeicher der Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="22"/>
        <source>2) Database Caching</source>
        <extracomment>This refers to a type of cache for the thumbnails</extracomment>
        <translation>2) Zwischenspeicher in Datenbank</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="24"/>
        <source>Both ways have their advantages and disadvantages:</source>
        <extracomment>The two ways are the two types of thumbnail caching (files and database)</extracomment>
        <translation>Beide Wege haben ihr Vor- und Nachteile:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="18"/>
        <source>Thumbnails can be cached in two different ways:</source>
        <translation>Vorschaubilder können auf zwei verschiedenen Weisen zwischengespeichert werden:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="20"/>
        <source>1) File Caching (following the freedesktop.org standard)</source>
        <extracomment>This refers to a type of cache for the thumbnails</extracomment>
        <translation>1) Zwischenspeicher in Dateien (nach dem Standard von freedesktop.og)</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="26"/>
        <source>File Caching is done according to the freedesktop.org standard and thus different applications can share the same thumbnail for the same image file.</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation>Die Implementierung des Zwischenspeichers in Dateien folgt dem freedesktop.org-Standard und daher können verschiedene Anwendungen von denselben Vorschaubildern Gebrauch machen.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="28"/>
        <source>Database Caching doesn&apos;t have the advantage of sharing thumbnails with other applications (and thus every thumbnails has to be newly created for PhotoQt), but it allows PhotoQt to have more control over existing thumbnails and works better on systems like Windows.</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation>Der Zwischenspeicher in einer Datenbank hat den Nachteil dass die Vorschaubilder nicht mit anderen Anwendungen geteilt werden können (und daher muss jedes Vorschaubild für PhotoQt neu erstellt werden), aber damit hat PhotoQt etwas mehr Kontrolle über bestehende Vorschaubilder. Dies ist die empfohlene Variante für Windows.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="30"/>
        <source>PhotoQt works with either option, though the first way is set as default and strongly recommended.</source>
        <extracomment>The options talked about are the two ways to cache thumbnails (files and database)</extracomment>
        <translation>PhotoQt funktioniert gut mit beiden Optionen, wobei der erste Weg  voreingestellt ist und wärmstens empfohlen wird.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="32"/>
        <source>Although everybody is encouraged to use at least one of the two options, caching can be completely disabled altogether. However, this means that each thumbnail has to be recreated everytime it is needed.</source>
        <extracomment>Talking about thumbnail caching with its two possible options, files and database caching</extracomment>
        <translation>Obwohl es jedem empfohlen wird eine der beiden Varianten zu benutzen, kann der Zwischenspeicher komplett ausgeschaltet werden. Dies bedeutet jedoch, dass jedes Mal wenn ein Vorschaubild benötigt wird dies neu erstellt werden muss.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="49"/>
        <source>Enable Cache</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation>Zwischenspeicher einschalten</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="78"/>
        <source>File Caching</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation>Zwischenspeicher in Dateien</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="85"/>
        <source>Database Caching</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation>Zwischenspeicher in Datebank</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="115"/>
        <source>Database filesize:</source>
        <extracomment>The database refers to the database used for caching thumbnail images</extracomment>
        <translation>Dateigröße der Datenbank:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="145"/>
        <source>Entries in database:</source>
        <extracomment>The database refers to the database used for caching thumbnail images (the entries)</extracomment>
        <translation>Einträge in Datenbank:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="173"/>
        <source>CLEAN UP</source>
        <extracomment>Refers to cleaning up the database for thumbnail caching</extracomment>
        <translation>AUFRÄUMEN</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="187"/>
        <source>ERASE</source>
        <extracomment>Refers to emptying the database for thumbnail caching</extracomment>
        <translation>ENTFERNEN</translation>
    </message>
</context>
<context>
    <name>CenterOn</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/CenterOn.qml" line="18"/>
        <source>Keep in Center</source>
        <extracomment>the center is the center of the screen edge. The thing talked about are the thumbnails.</extracomment>
        <translation>Mittig halten</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/CenterOn.qml" line="19"/>
        <source>If this option is set, then the current thumbnail (i.e., the thumbnail of the currently displayed image) will always be kept in the center of the thumbnail bar (if possible). If this option is not set, then the active thumbnail will simply be kept visible, but not necessarily in the center.</source>
        <translation>Mit dieser Option wird das aktuelle Vorschaubild (d.h., das Vorschaubild des momentan angezeigten Bildes) immer in der Mitte des des Bildschirms gehalten (soweit möglich). Wenn diese Option nicht gesetzt wird, dann stellt PhotoQt schlicht sicher, dass das aktive Vorschaubild sichtbar aber nicht unbedingt mittig ist.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/CenterOn.qml" line="29"/>
        <source>Center on Current Thumbnail</source>
        <translation>Aktuelles Vorschaubild mittig halten</translation>
    </message>
</context>
<context>
    <name>CloseOnClick</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/CloseOnClick.qml" line="17"/>
        <source>Click on Empty Area</source>
        <extracomment>The empty area is the area around the main image</extracomment>
        <translation>Klick auf leere Fläche</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/CloseOnClick.qml" line="18"/>
        <source>This option makes PhotoQt behave a bit like the JavaScript image viewers you find on many websites. A click outside of the image on the empty background will close the application. This way PhotoQt will feel even more like a &apos;floating layer&apos;, however, this can easily be triggered accidentally. Note that if you use a mouse click for a shortcut already, then this option wont have any effect!</source>
        <translation>Durch diese Option wird PhotoQt ein wenig ähnlicher zu manchen JavaScript-Bildbetrachtern die man auf einigen Webseiten finden kann. Ein Klick außerhalb des Bildes auf die leere Fläche schließt die Anwendung. Dadurch fühlt sich PhotoQt erst recht wie eine &apos;schwebende Ebene&apos; an, gleichzeitig kann dies aber auch unabsichtlich ausgelöst werden. Bitte beachte, dass diese Option nicht berücksichtigt wird wenn ein Mausklick als Teil eines Kurzbefehls verwendet wird!</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/CloseOnClick.qml" line="28"/>
        <source>Close on click in empty area</source>
        <extracomment>The empty area is the area around the main image</extracomment>
        <translation>Schließen wenn auf leere Fläche geklickt wird</translation>
    </message>
</context>
<context>
    <name>ClosingX</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="19"/>
        <source>Exit button (&apos;x&apos; in top right corner)</source>
        <translation>Schließknopf (&apos;x&apos; in der Ecke oben rechts)</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="20"/>
        <source>There are two looks for the exit button: a normal &apos;x&apos; or a plain text&apos;x&apos;. The normal &apos;x&apos; fits in better with the overall design of PhotoQt, but the plain text &apos;x&apos; is smaller and more discreet.</source>
        <translation>Der Knopf zum Beenden kann entweder normal oder als Klartext dargestellt werden. Der normale Stil fügt sich etwas besser in den allgemeinen Stil von PhotoQt ein, als Klartext ist er aber etwas kleiner und diskreter.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="36"/>
        <source>Normal</source>
        <extracomment>This is a type of exit button ('x' in top right screen corner)</extracomment>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="42"/>
        <source>Plain</source>
        <extracomment>This is a type of exit button ('x' in top right screen corner), showing a simple text 'x'</extracomment>
        <translation>Einfach</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="59"/>
        <source>Small Size</source>
        <extracomment>The size of the exit button ('x' in top right screen corner)</extracomment>
        <translation>Klein</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="83"/>
        <source>Large Size</source>
        <extracomment>The size of the exit button ('x' in top right screen corner)</extracomment>
        <translation>Groß</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/ClosingX.qml" line="61"/>
        <source>Show counter</source>
        <extracomment>The counter shows the position of the currently loaded image in the folder</extracomment>
        <translation>Zeige Zähler</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/ClosingX.qml" line="65"/>
        <source>Show filepath</source>
        <translation>Zeige Dateipfad</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/ClosingX.qml" line="69"/>
        <source>Show filename</source>
        <translation>Zeige Dateinamen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/ClosingX.qml" line="74"/>
        <source>Show closing &apos;x&apos;</source>
        <extracomment>The clsoing 'x' is the button in the top right corner of the screen for closing PhotoQt</extracomment>
        <translation>Zeige &apos;x&apos; zum Schließen</translation>
    </message>
</context>
<context>
    <name>Copy</name>
    <message>
        <location filename="../../qml/filemanagement/Copy.qml" line="17"/>
        <source>Use the file dialog to select a destination location.</source>
        <translation>Wähle einen Zielort in dem angezeigten Fenster aus.</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Copy.qml" line="36"/>
        <source>Copy Image to...</source>
        <translation>Kopiere Bild nach...</translation>
    </message>
</context>
<context>
    <name>CustomConfirm</name>
    <message>
        <location filename="../../qml/elements/CustomConfirm.qml" line="130"/>
        <source>Don&apos;t ask again</source>
        <translation>Nicht erneut fragen</translation>
    </message>
</context>
<context>
    <name>CustomEntries</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="20"/>
        <source>Custom Entries in Main Menu</source>
        <translation>Eigene Einträge im Hauptmenü</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="21"/>
        <source>Here you can adjust the custom entries in the main menu. You can simply drag and drop the entries, edit them, add a new one and remove an existing one.</source>
        <translation>Hier können die eigenen Einträge im Hauptmenü angepasst werden. Die Einträge können durch einfaches Klicken und Ziehen verschoben, der Text bearbeitet, oder ein Eintrag hinzugefügt/entfernt werden.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="76"/>
        <source>Executable</source>
        <translation>Ausführbare Datei</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="94"/>
        <source>Menu Text</source>
        <translation>Menü-Text</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="272"/>
        <source>Click here to drag entry</source>
        <translation>Zum Ziehen eines Eintrages hier klicken</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="388"/>
        <source>quit</source>
        <extracomment>KEEP THIS STRING SHORT! It is displayed for external applications of main menu as an option to quit PhotoQt after executing it</extracomment>
        <translation>beenden</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="484"/>
        <source>Add new entry</source>
        <translation>Neuen Eintrag hinzufügen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="501"/>
        <source>Set default entries</source>
        <translation>Setze Standardeinträge</translation>
    </message>
</context>
<context>
    <name>CustomFileSelect</name>
    <message>
        <location filename="../../qml/elements/CustomFileSelect.qml" line="52"/>
        <source>Click here to select a configuration file</source>
        <translation>Klicke hier um eine Konfigurationsdatei auszuwählen...</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomFileSelect.qml" line="82"/>
        <source>Select PhotoQt config file...</source>
        <extracomment>PhotoQt config file = configuration file</extracomment>
        <translation>Wähle eine Konfigurationsdatei von PhotoQt aus...</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomFileSelect.qml" line="84"/>
        <source>PhotoQt Config Files</source>
        <extracomment>PhotoQt config file = configuration file</extracomment>
        <translation>PhotoQt Konfigurationsdateien</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomFileSelect.qml" line="85"/>
        <source>All Files</source>
        <translation>Alle Dateien</translation>
    </message>
</context>
<context>
    <name>CustomLineEdit</name>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="89"/>
        <source>Undo</source>
        <extracomment>As in 'Undo latest change'</extracomment>
        <translation>Zurücknehmen</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="92"/>
        <source>Redo</source>
        <extracomment>As in 'Redo latest change'</extracomment>
        <translation>Wiederholen</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="97"/>
        <source>Cut selection</source>
        <extracomment>selection = selected text</extracomment>
        <translation>Auswahl ausschneiden</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="100"/>
        <source>Copy selection to clipboard</source>
        <extracomment>selection = selected text</extracomment>
        <translation>Kopiere Auswahl in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="102"/>
        <source>Paste clipboard content</source>
        <translation>Inhalt der Zwischenablage einfügen</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="105"/>
        <source>Delete content</source>
        <extracomment>content refers to text content in a line edit</extracomment>
        <translation>Inhalt löschen</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="110"/>
        <source>Select all</source>
        <extracomment>Refering to all text</extracomment>
        <translation>Alles auswählen</translation>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="113"/>
        <source>Select all and copy</source>
        <extracomment>In the sense of 'Selecting all text and copying it to clipboard'</extracomment>
        <translation>Alles auswählen und kopieren</translation>
    </message>
</context>
<context>
    <name>Delete</name>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="20"/>
        <source>Delete File</source>
        <translation>Datei löschen</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="42"/>
        <source>Do you really want to delete this file?</source>
        <translation>Möchtest du diese Datei wirklich löschen?</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="73"/>
        <source>Move to Trash</source>
        <extracomment>In the sense of 'move the current image into the trash'</extracomment>
        <translation>In den Müll verschieben</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="75"/>
        <source>Delete</source>
        <extracomment>As in 'Delete the current image'</extracomment>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="84"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="106"/>
        <source>Delete permanently</source>
        <extracomment>In the sense of 'Delete the current image permanently'</extracomment>
        <translation>Permanent löschen</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="124"/>
        <source>Enter = Move to Trash, Shift+Enter = Delete permanently, Escape = Cancel</source>
        <translation>Enter = in Papierkorb schieben, Shift+Enter = dauerhaft löschen, Escape = Abbrechen</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="125"/>
        <source>Enter = Delete, Escape = Cancel</source>
        <translation>Enter = Löschen, Escape = Abbrechen</translation>
    </message>
</context>
<context>
    <name>DetectShortcut</name>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="139"/>
        <source>Shortcut also already set for the following:</source>
        <translation>Der Kurzbefehl ist ebenfalls schon gesetzt für folgendes:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="189"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="218"/>
        <source>Perform any mouse action or press any key combination.</source>
        <translation>Führe irgendeine Mausaktion durch oder drücke irgendeine Tastenkombination.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="219"/>
        <source>When your satisfied, click the button to the right.</source>
        <translation>Wenn du zufrieden bist, dann klicke rechts auf den Knopf.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="242"/>
        <source>Ok, set shortcut</source>
        <translation>Ok, setze Kurzbefehl</translation>
    </message>
</context>
<context>
    <name>Disable</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Disable.qml" line="17"/>
        <source>Disable thumbnails</source>
        <translation>Vorschaubilder deaktivieren</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Disable.qml" line="18"/>
        <source>If you just don&apos;t need or don&apos;t want any thumbnails whatsoever, then you can disable them here completely. This will increase the speed of PhotoQt, but will make navigating with the mouse harder.</source>
        <translation>Wenn überhaupt keine Vorschaubilder gewollt/gebraucht werden, dann können diese hier komplett deaktiviert werden. Dies erhöht die Geschwindigkeit von PhotoQt etwas, aber es ist gleichzeitig auch etwas schwieriger mit der Maus zu navigieren.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Disable.qml" line="29"/>
        <source>Disable Thumbnails altogether</source>
        <translation>Vorschaubilder generell deaktivieren</translation>
    </message>
</context>
<context>
    <name>Enlightenment</name>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="32"/>
        <source>Warning: It seems that the &apos;msgbus&apos; (DBUS) module is not activated! It can be activated in the settings console:</source>
        <extracomment>&quot;msgbus&quot; and &quot;DBUS&quot; are fixed names, please don't translate</extracomment>
        <translation>Warnung: Es scheint so, als ob das Modul &apos;msgbus&apos; (DBUS) nicht aktiviert ist. Es kann in der Einstellungskonsole aktiviert werden:</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="44"/>
        <source>Warning: &apos;enlightenment_remote&apos; doesn&apos;t seem to be available! Are you sure Enlightenment is installed?</source>
        <extracomment>&quot;enlightenment_remote&quot; and &quot;Enlightenment&quot; are fixed names, please don't translate</extracomment>
        <translation>Warnung: &apos;enlightenment_remote&apos; scheint nicht verfügbar zu sein! Bist du dir sicher, dass Enlightenment installiert ist?</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="55"/>
        <source>The wallpaper can be set to any of the available monitors (one or any combination).</source>
        <translation>Dieses Hintergrundbild kann auf einem beliebigen Bildschirm gesetzt werden (und auch jeder Kombination).</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="72"/>
        <source>Screen</source>
        <extracomment>Used as in 'Screen #4', the screen is not referring to multiple desktops/workspaces, but actual (physical) screens</extracomment>
        <translation>Bildschirm</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="107"/>
        <source>You can set the wallpaper to any sub-selection of workspaces</source>
        <translation>Du kannst ein Hintergrundbild zu einer beliebigen Auswahl an Arbeitsflächen setzen</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="128"/>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="131"/>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="133"/>
        <source>Workspace</source>
        <translation>Arbeitsfläche</translation>
    </message>
</context>
<context>
    <name>ExportImport</name>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="65"/>
        <source>Export/Import settings and shortcuts</source>
        <translation>Einstellungen und Kurzbefehle ex-/importieren</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="75"/>
        <source>Here you can export all settings and shortcuts into a single packed file and, e.g., import it in another installation of PhotoQt.</source>
        <translation>Hier können alle Einstellungen und Kurzbefehle in eine einzelne gepackte Datei exportiert werden und, z.B., auf einem anderen Computer importiert werden.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="93"/>
        <source>Export everything to file</source>
        <extracomment>Everything refers to all settings and shortcuts</extracomment>
        <translation>Exportiere alles in eine Datei</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="129"/>
        <source>Import settings and shortcuts</source>
        <translation>Importiere Einstellungen und Kurzbefehle</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="153"/>
        <source>PhotoQt will attempt to automatically restart after a successful import!</source>
        <translation>Nach einem erfolgreichen Import wird PhotoQt versuchen automatisch neuzustarten!</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="167"/>
        <source>I don&apos;t want to do this</source>
        <translation>Ich möchte das lieber nicht tun</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="180"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="184"/>
        <source>Exporting the configuration file failed with the following error message:</source>
        <translation>Das Exportieren der Konfigurationsdatei ist mit folgender Fehlermeldung fehlgeschlagen:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="185"/>
        <source>Importing the configuration file failed with the following error message:</source>
        <translation>Das Importieren der Konfigurationsdatei ist mit folgender Fehlermeldung fehlgeschlagen:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="186"/>
        <source>Oh, okay</source>
        <translation>Oh, okay</translation>
    </message>
</context>
<context>
    <name>FileTypesExtras</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="19"/>
        <source>File Formats</source>
        <translation>Dateiformate</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="21"/>
        <source>Extras</source>
        <extracomment>These are extra (special) file formats</extracomment>
        <translation>Extras</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="22"/>
        <source>The following filetypes are supported by means of other third party tools. You first need to install them before you can use them.</source>
        <translation>Die folgenden Dateitypen werden mit Hilfe anderer Programme angezeigt. Diese müssen installiert sein bevor die Formate benutzt werden können.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="23"/>
        <source>Please note that if an image format is also provided by GraphicsMagick/Qt, then PhotoQt first chooses the external tool (if enabled).</source>
        <translation>Sollte ein Dateiformat ebenfalls von GraphicsMagick/Qt unterstützt werden, dann bevorzugt PhotoQt die externen Programme (soweit aktiviert).</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="81"/>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="83"/>
        <source>Makes use of</source>
        <extracomment>Used as in 'Makes use of tool abc'</extracomment>
        <translation>Benutzt</translation>
    </message>
</context>
<context>
    <name>FileTypesGM</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGM.qml" line="19"/>
        <source>File Formats</source>
        <translation>Dateiformate</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGM.qml" line="21"/>
        <source>disabled</source>
        <extracomment>Used as in 'disabled category'</extracomment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGM.qml" line="23"/>
        <source>PhotoQt makes use of GraphicsMagick for support of many different image formats. The list below are all those formats, that were successfully displayed using test images. There are a few formats, that were not tested in PhotoQt (due to lack of a test image). You can find those in the &apos;Untested&apos; category below.</source>
        <translation>PhotoQt benutzt GraphicsMagick, um viele verschiedene Dateitypen anzuzeigen. Die unten stehenden Dateiformate wurden erfolgreich getestet. Es gibt ein paar Formate die (aus Mangel an Testbildern) nicht getestet werden konnten. Diese stehen in der Kategorie &apos;ungetestet&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGM.qml" line="24"/>
        <source>PhotoQt was built without GraphicsMagick support!</source>
        <translation>PhotoQt wurde ohne Unterstützung für GraphicsMagick erstellt!</translation>
    </message>
</context>
<context>
    <name>FileTypesGMGhostscript</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGMGhostscript.qml" line="19"/>
        <source>File Formats</source>
        <translation>Dateiformate</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGMGhostscript.qml" line="21"/>
        <source>disabled</source>
        <extracomment>Used as in 'disabled category'</extracomment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGMGhostscript.qml" line="23"/>
        <source>The following file types are supported by GraphicsMagick, and they have been tested and should work. However, they require Ghostscript to be installed on the system.</source>
        <translation>Die folgenden Dateitypen werden von GraphicsMagick unterstützt, und sie wurden erfolgreich getestet. Für diese muss jedoch zusätzlich Ghostscript auf dem System installiert sein.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGMGhostscript.qml" line="24"/>
        <source>PhotoQt was built without GraphicsMagick support!</source>
        <translation>PhotoQt wurde ohne Unterstützung für GraphicsMagick erstellt!</translation>
    </message>
</context>
<context>
    <name>FileTypesQt</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesQt.qml" line="19"/>
        <source>File Formats</source>
        <translation>Dateiformate</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesQt.qml" line="20"/>
        <source>These are the file types natively supported by Qt. Make sure, that you&apos;ll have the required libraries installed (e.g., qt5-imageformats), otherwise some of them might not work on your system.</source>
        <translation>Dies sind die Dateitypen, die Qt von Haus aus unterstützen kann. Stelle aber sicher, dass die benötigten Bibliotheken installiert sind (z.B., qt5-imageformats), ansonsten könnten manche nicht funktionieren.</translation>
    </message>
</context>
<context>
    <name>FileTypesRaw</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesRaw.qml" line="19"/>
        <source>File Formats</source>
        <translation>Dateiformate</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesRaw.qml" line="21"/>
        <source>disabled</source>
        <extracomment>As in 'disabled category'</extracomment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesRaw.qml" line="23"/>
        <source>PhotoQt can open and display most raw image formats. Here you can adjust the list of fileformats known to PhotoQt.</source>
        <translation>PhotoQt kann die meisten RAW Dateiformate öffnen. Hier kann die Liste bekannter Formate anggepasst werden.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesRaw.qml" line="24"/>
        <source>PhotoQt was built without LibRaw support!</source>
        <translation>PhotoQt wurde ohne Unterstützung für LibRaw erstellt!</translation>
    </message>
</context>
<context>
    <name>FileTypesUntested</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="19"/>
        <source>File Formats</source>
        <translation>Dateiformate</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="21"/>
        <source>Untested</source>
        <extracomment>As in 'Untested file formats'</extracomment>
        <translation>Ungetestet</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="23"/>
        <source>disabled</source>
        <extracomment>As in 'disabled category'</extracomment>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="25"/>
        <source>These are some file types that are supported by GraphicMagick, but have not been tested in PhotoQt. They might work, but no guarantee can be given!</source>
        <translation>Diese Dateitypen werden zwar von GraphicsMagick unterstützt, wurden aber nicht in PhotoQt getestet. Es kann sein, dass sie funktionieren, aber eine Garantie kann nicht gegeben werden!</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="26"/>
        <source>PhotoQt was built without GraphicsMagick support!</source>
        <translation>PhotoQt wurde ohne Unterstützung für GraphicsMagick erstellt!</translation>
    </message>
</context>
<context>
    <name>FilenameOnly</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/FilenameOnly.qml" line="19"/>
        <source>Filename Thumbnail</source>
        <translation>Dateinamen als Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/FilenameOnly.qml" line="20"/>
        <source>If you don&apos;t want PhotoQt to always load the actual image thumbnail in the background, but you still want to have something for better navigating, then you can set a filename-only thumbnail, i.e. PhotoQt wont load any thumbnail images but simply puts the file name into the box. You can also adjust the font size of this text.</source>
        <translation>Wenn es bevorzugt wird dass PhotoQt keine vollen Vorschaubilder im Hintergrund lädt, aber dennoch etwas für einfachere Navigation gewünscht wird, dann kann hier ein einfaches Vorschaubild bestehend aus dem Dateinamen aktiviert werden. Die Schriftgröße kann ebenfalls angepasst werden.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/FilenameOnly.qml" line="34"/>
        <source>Use filename-only thumbnail</source>
        <translation>Benutze Dateinamen als Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/FilenameOnly.qml" line="46"/>
        <source>Fontsize</source>
        <translation>Schriftgröße</translation>
    </message>
</context>
<context>
    <name>FilesView</name>
    <message>
        <location filename="../../qml/openfile/FilesView.qml" line="96"/>
        <source>This protocol is currently not supported</source>
        <extracomment>Protocol refers to a file protocol (e.g., for network folders)</extracomment>
        <translation>Dieses Protokoll wird momentan nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/FilesView.qml" line="98"/>
        <source>No image files found</source>
        <extracomment>Can also be expressed as 'zero subfolders' or '0 subfolders'. It is also possible to drop the 'sub' leaving 'folders' if that works better</extracomment>
        <translation>Keine Bilddateien gefunden</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/FilesView.qml" line="353"/>
        <source>Name</source>
        <extracomment>Refers to the filename. Keep string short!</extracomment>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/FilesView.qml" line="355"/>
        <source>Size</source>
        <extracomment>Refers to the filesize. Keep string short!</extracomment>
        <translation>Größe</translation>
    </message>
</context>
<context>
    <name>Filter</name>
    <message>
        <location filename="../../qml/other/Filter.qml" line="21"/>
        <source>Filter images in current directory</source>
        <translation>Filtere Bilder in aktuellem Verzeichnis</translation>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="36"/>
        <source>Enter here the term you want to filter the images by. Separate multiple terms by a space.</source>
        <translation>Gib hier deine Filterbegriffe ein. Trenne mehrere Suchbegriffe durch Leerzeichen.</translation>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="43"/>
        <source>If you want to limit a term to file extensions, prepend a dot &apos;.&apos; to the term.</source>
        <translation>Wenn du einen Suchbegriff auf Dateierweiterungen begrenzen möchtest, dann beginne ihn mit einem Punkt &apos;.&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="92"/>
        <source>Filter</source>
        <extracomment>As in 'Go ahead and filter images'</extracomment>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="99"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="113"/>
        <source>Remove Filter</source>
        <translation>Filter entfernen</translation>
    </message>
</context>
<context>
    <name>FitInWindow</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/FitInWindow.qml" line="17"/>
        <source>Fit in Window</source>
        <extracomment>Along the lines of 'zoom small images until they fill the window'</extracomment>
        <translation>An Fenster anpassen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/FitInWindow.qml" line="18"/>
        <source>If the image dimensions are smaller than the screen dimensions, PhotoQt can automatically zoom those images to make them fit into the window.</source>
        <translation>Falls die Größe eines Bildes kleiner als die Bildschirmgröße ist, dann kann PhotoQt diese Bilder automatisch an die Bildschirmgröße anpassen.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/FitInWindow.qml" line="27"/>
        <source>Fit Smaller Images in Window</source>
        <translation>Kleinere Bilder in Fenster anpassen</translation>
    </message>
</context>
<context>
    <name>Folders</name>
    <message>
        <location filename="../../qml/openfile/Folders.qml" line="73"/>
        <source>This protocol is currently not supported</source>
        <extracomment>Protocol refers to a file protocol (e.g., for network folders)</extracomment>
        <translation>Dieses Protokoll wird momentan nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/Folders.qml" line="75"/>
        <source>No subfolders</source>
        <extracomment>Can also be expressed as 'zero subfolders' or '0 subfolders'. It is also possible to drop the 'sub' leaving 'folders' if that works better</extracomment>
        <translation>Keine Unterordner</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/Folders.qml" line="212"/>
        <source>image</source>
        <extracomment>Used as in '(1 image)'. This string is always used for the singular, exactly one image</extracomment>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/Folders.qml" line="214"/>
        <source>images</source>
        <extracomment>Used as in '(11 images)'. This string is always used for multiple images (at least 2)</extracomment>
        <translation>Bilder</translation>
    </message>
</context>
<context>
    <name>FontSize</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/FontSize.qml" line="17"/>
        <source>Font Size</source>
        <translation>Schriftgröße</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/FontSize.qml" line="18"/>
        <source>The fontsize of the metadata element can be adjusted independently of the rest of the application.</source>
        <translation>Die Schriftgröße des Elements für die Metadaten kann unabhängig vom Rest der Anwendung angepasst werden.</translation>
    </message>
</context>
<context>
    <name>GetAndDoStuffContext</name>
    <message>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="18"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="20"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="22"/>
        <source>Edit with</source>
        <extracomment>Used as in 'Edit with tool abc'</extracomment>
        <translation>Bearbeiten mit</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="24"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="26"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="28"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="30"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="32"/>
        <source>Open in</source>
        <translation>Öffnen in</translation>
    </message>
</context>
<context>
    <name>GetAndDoStuffFile</name>
    <message>
        <location filename="../../cplusplus/scripts/getanddostuff/file.cpp" line="8"/>
        <source>Please select image file</source>
        <translation>Wähle bitte eine Bilddatei aus</translation>
    </message>
</context>
<context>
    <name>GetMetaData</name>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="391"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="394"/>
        <source>Daylight</source>
        <translation>Tageslicht</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="397"/>
        <source>Fluorescent</source>
        <translation>Fluoreszierend</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="400"/>
        <source>Tungsten (incandescent light)</source>
        <translation>weißglühendes Wolfram Licht</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="403"/>
        <source>Flash</source>
        <translation>Blitz</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="406"/>
        <source>Fine weather</source>
        <translation>Gutes Wetter</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="409"/>
        <source>Cloudy Weather</source>
        <translation>Bewölkt</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="412"/>
        <source>Shade</source>
        <translation>Schatten</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="415"/>
        <source>Daylight fluorescent</source>
        <translation>Fluoreszierendes Tageslicht</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="418"/>
        <source>Day white fluorescent</source>
        <translation>Fluoreszierendes Tagesweiß</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="421"/>
        <source>Cool white fluorescent</source>
        <translation>Kühl-Weiß-Fluroszierend</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="424"/>
        <source>White fluorescent</source>
        <translation>Weiß-Fluroszierend</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="427"/>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="430"/>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="433"/>
        <source>Standard light</source>
        <translation>Standard-Licht</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="446"/>
        <source>Other light source</source>
        <translation>Andere Lichtquelle</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="449"/>
        <source>Invalid light source</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation>Ungültige Lichtquelle</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="458"/>
        <source>yes</source>
        <extracomment>This string identifies that flash was fired, stored in image metadata</extracomment>
        <translation>ja</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="460"/>
        <source>no</source>
        <extracomment>This string identifies that flash wasn't fired, stored in image metadata</extracomment>
        <translation>nein</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="462"/>
        <source>No flash function</source>
        <extracomment>This string refers to the absense of a flash, stored in image metadata</extracomment>
        <translation>Keine Blitzfunktion</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="464"/>
        <source>strobe return light not detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>keine Reflektion erkannt</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="466"/>
        <source>strobe return light detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>Reflektion erkannt</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="468"/>
        <source>compulsory flash mode</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>festgelegter Blitzmodus</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="470"/>
        <source>auto mode</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>automatisch</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="472"/>
        <source>red-eye reduction mode</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>Rote-Augen-Reduktion</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="474"/>
        <source>return light detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>Reflektion erkannt</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="476"/>
        <source>return light not detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>keine Reflektion erkannt</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="524"/>
        <source>Invalid flash</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>Ungültiger Blitz</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="534"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="537"/>
        <source>Landscape</source>
        <translation>Landschaft</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="540"/>
        <source>Portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="543"/>
        <source>Night Scene</source>
        <translation>Nachtszene</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="546"/>
        <source>Invalid Scene Type</source>
        <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
        <translation>Ungültiger Szenentyp</translation>
    </message>
</context>
<context>
    <name>GnomeUnity</name>
    <message>
        <location filename="../../qml/wallpaper/modules/GnomeUnity.qml" line="30"/>
        <source>Warning: &apos;gsettings&apos; doesn&apos;t seem to be available! Are you sure Gnome/Unity is installed?</source>
        <extracomment>&quot;gsettings&quot;, &quot;Gnome&quot; and &quot;Unity&quot; are fixed names, please don't translate</extracomment>
        <translation>Warnung: &apos;gsettings&apos; scheint nicht verfügbar zu sein! Sicher, dass Gnome/Unity installiert ist?</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/GnomeUnity.qml" line="41"/>
        <source>There are several picture options that can be set for the wallpaper image.</source>
        <extracomment>'picture options' refers to options like stretching the image to fill the background, or tile the image, center it, etc.</extracomment>
        <translation>Es gibt verschiedene Optionen, die für dieses Hintergrundbild gesetzt werden können.</translation>
    </message>
</context>
<context>
    <name>Histogram</name>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="140"/>
        <source>Histogram</source>
        <extracomment>A histogram visualises the color distribution in an image</extracomment>
        <translation>Histogramm</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="174"/>
        <source>Loading...</source>
        <extracomment>As in 'Loading the histogram for the current image'</extracomment>
        <translation>Lade...</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="207"/>
        <source>Click-and-drag to move. Right click to switch version.</source>
        <extracomment>Used for the histogram. The version refers to the type of histogram that is available (colored and greyscale)</extracomment>
        <translation>Klicken-und-ziehen zum verschieben. Rechtsklick um zwischen den Versionen zu wechseln.</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="318"/>
        <source>Click to switch between coloured and greyscale histogram. You can also switch by doing a right-click onto the histogram.</source>
        <translation>Klicke hier um zwischen dem Farb- und Graustufenhistogramm zu wechseln. Dies kann auch mittels eines Rechtsklick auf das Histogramm getan werden.</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="362"/>
        <source>Click to hide histogram. It can always be shown again from the main menu.</source>
        <translation>Klicke hier um das Histogramm zu verstecken. Es kann jederzeit im Hauptmenü wieder angezeigt werden.</translation>
    </message>
</context>
<context>
    <name>HotEdge</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/HotEdge.qml" line="19"/>
        <source>Size of &apos;Hot Edge&apos;</source>
        <extracomment>The hot edge refers to the left and right screen edge. When the mouse cursor enters the hot edge area, then the main menu/metadata element is shown</extracomment>
        <translation>Größe der &apos;Heißen Kante&apos;</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/HotEdge.qml" line="20"/>
        <source>Here you can adjust the sensitivity of the metadata and main menu elements. The main menu opens when your mouse cursor gets close to the right screen edge, the metadata element when you go to the left screen edge. This setting controls how close to the screen edge you have to get before they are shown.</source>
        <translation>Hier kann die Empfindlichkeit der Elemente für die Metadaten und das Hauptmenü angepasst werden. Das Hauptmenü öffnet sich wenn der Mauszeiger sich der rechten Bildschirmkante nähert, die Metadaten an der linken Kante. Diese Einstellung setzt fest wie viel sich der Mauszeiger der Bildschirmkante nähern muss damit eines der beiden Elemente erscheint.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/HotEdge.qml" line="34"/>
        <source>Small</source>
        <extracomment>This refers to the size of the hot edge, you have to get very close to the screen edge to trigger the main menu or metadata element</extracomment>
        <translation>Klein</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/HotEdge.qml" line="57"/>
        <source>Large</source>
        <extracomment>This refers to the size of the hot edge, you don't have to get very close to the screen edge to trigger the main menu or metadata element</extracomment>
        <translation>Groß</translation>
    </message>
</context>
<context>
    <name>Imgur</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="19"/>
        <source>Here you can connect PhotoQt to your imgur.com account for uploading images directly to it. Alternatively, you can always upload images anonymously to imgur.com without any user account. In either case, PhotoQt will return the image URL to you.</source>
        <translation>Hier kann PhotoQt mit einem Benutzerkonto auf imgur.com verbunden werden um Bilder direkt dahin hochzuladen. Unabhängig davon können Bilder immer anonym auf imgur.com hochgeladen werden (ohne Benutzerkonto). In beiden Fällen teilt PhotoQt die URL des Bildes mit.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="46"/>
        <source>Authenticated with account</source>
        <extracomment>Account refers to an imgur.com user account</extracomment>
        <translation>Authentisiert mit Konto</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="57"/>
        <source>not authenticated</source>
        <extracomment>As in &quot;not authenticated with imgur.com user account&quot;</extracomment>
        <translation>Nicht authentisiert</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="66"/>
        <source>authenticated on</source>
        <extracomment>As in &quot;authenticated with imgur.com user account on 1991-07-23, 13:31&quot;</extracomment>
        <translation>authentisiert am</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="86"/>
        <source>Connect to Account</source>
        <extracomment>Account refers to imgur.com user account</extracomment>
        <translation>Verbinde mit Konto</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="88"/>
        <source>Connect to New Account</source>
        <extracomment>Account refers to imgur.com user account</extracomment>
        <translation>Verbinde mit neuem Konto</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="94"/>
        <source>Forget Account</source>
        <extracomment>Account refers to imgur.com user account</extracomment>
        <translation>Vergiss Konto</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="142"/>
        <source>Not connected to internet</source>
        <translation>Keine Internetverbindung</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="147"/>
        <source>Go to this URL:</source>
        <translation>Besuche diese URL:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="154"/>
        <source>loading...</source>
        <translation>lade...</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="157"/>
        <source>open link</source>
        <translation>Link öffnen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="163"/>
        <source>Paste PIN here</source>
        <translation>PIN hier einfügen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="170"/>
        <source>Connect</source>
        <translation>Verbinde</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="178"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>ImgurFeedback</name>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="57"/>
        <source>Uploading image to imgur.com anonymously</source>
        <translation>Bilder auf imgur.com anonym hochladen</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="58"/>
        <source>Uploading image to imgur.com account:</source>
        <translation>Bilder in das Benutzerkonto auf imgur.com hochladen:</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="88"/>
        <source>Cancel upload</source>
        <translation>Hochladen abbrechen</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="128"/>
        <source>Obtaining image url</source>
        <extracomment>The image url is the url returned from imgur.com after uploading</extracomment>
        <translation>Beziehe URL des Bildes</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="162"/>
        <source>Please wait!</source>
        <translation>Bitte warten!</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="175"/>
        <source>This seems to take a long time... If it doesn&apos;t work, then there might be a problem with the imgur.com servers at the moment. In that case the only solution is to try again at some later point!</source>
        <extracomment>Refers to uploading an image to imgur.com</extracomment>
        <translation>Dies scheint eine lange Zeit zu dauern... Falls es nicht funktioniert, dann kann es sein dass die Server von imgur.com momentan Probleme haben. Wenn das so ist, dann ist die einzige Lösung zu warten und später noch einmal zu probieren!</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="197"/>
        <source>I don&apos;t want to know it!</source>
        <translation>Mir ist das egal!</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="241"/>
        <source>An Error occured while uploading image!</source>
        <extracomment>Refers to uploading an image to imgur.com</extracomment>
        <translation>Beim Hochladen ist ein Fehler aufgetreten!</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="242"/>
        <source>Error code:</source>
        <translation>Fehlercode:</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="249"/>
        <location filename="../../qml/other/ImgurFeedback.qml" line="295"/>
        <source>Oh, ok, got it!</source>
        <translation>Oh, okay, alles klar!</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="288"/>
        <source>You don&apos;t seem to be able to be connected to the internet... Unable to upload!</source>
        <extracomment>Refers to uploading an image to imgur.com</extracomment>
        <translation>Es scheint keine Internetverbindung zu geben... Hochladen nicht möglich!</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="334"/>
        <source>Image successfully uploaded!</source>
        <extracomment>Refers to uploading an image to imgur.com</extracomment>
        <translation>Bild wurde erfolgreich hochgeladen!</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="355"/>
        <source>URL for accessing image</source>
        <extracomment>The url is returned from imgur.com after uploading</extracomment>
        <translation>URL um das Bild anzuschauen</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="367"/>
        <location filename="../../qml/other/ImgurFeedback.qml" line="403"/>
        <source>visit link</source>
        <translation>Link besuchen</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="391"/>
        <source>URL for deleting image</source>
        <extracomment>The url is returned from imgur.com after uploading</extracomment>
        <translation>URL um das Bild zu löschen</translation>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="413"/>
        <source>Got it!</source>
        <translation>Alles klar!</translation>
    </message>
</context>
<context>
    <name>Interpolation</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Interpolation.qml" line="19"/>
        <source>Interpolation</source>
        <extracomment>The type of interpolation to use for small images</extracomment>
        <translation>Interpolation</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Interpolation.qml" line="20"/>
        <source>There are many different interpolation algorithms out there. Depending on the choice of interpolation algorithm, the image (when zoomed in) will look slightly differently. PhotoQt uses mipmaps to get the best quality for images. However, for very small images, that might lead to too much blurring causing them to look rather ugly. For those images, the &apos;Nearest Neighbour&apos; algorithm tends to be a better choice. The threshold defines for which images to use which algorithm.</source>
        <translation>Es gibt so einige verschiedene Algorithmen für die Interpolation. Je nach Wahl sieht das Bild (wenn vergrößert) etwas anders aus. PhotoQt benutzt Mip-Maps um die beste Bildqualität zu erzielen. Jedoch für besonders kleine Bilder kann dies zu eher unschönem Verwischen führen. Für diese Bilder erzielt gewöhnlich der &apos;Nächste Nachbar&apos;-Algorithmus bessere Ergebnisse. Der Schwellwert definiert für welche Bildgröße welcher Algorithmus verwendet wird.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Interpolation.qml" line="35"/>
        <source>Threshold:</source>
        <extracomment>When to trigger an action, below which threshold</extracomment>
        <translation>Schwellwert:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Interpolation.qml" line="68"/>
        <source>Use &apos;Nearest Neighbour&apos; algorithm for upscaling</source>
        <extracomment>'Nearest Neighbour' is the name of a specific algorithm</extracomment>
        <translation>Benutze den &apos;Nächster Nachbar&apos; Algorithmus für das Hochskalieren</translation>
    </message>
</context>
<context>
    <name>KDE4</name>
    <message>
        <location filename="../../qml/wallpaper/modules/KDE4.qml" line="23"/>
        <source>Sorry, KDE4 doesn&apos;t offer the feature to change the wallpaper except from their own system settings. Unfortunately there&apos;s nothing I can do about that.</source>
        <extracomment>&quot;KDE4&quot; is a fixed name, please don't translate</extracomment>
        <translation>Das tut mir leid, aber KDE4 bietet nicht die Möglichkeit ein Hintergrundbild zu setzen, außer über deren eigene Systemeinstellungen. Daran kann ich leider nichts ändern.</translation>
    </message>
</context>
<context>
    <name>Keep</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Keep.qml" line="17"/>
        <source>Keep between images</source>
        <extracomment>Refers to keeping zoom/rotation/flip/position when switching images</extracomment>
        <translation>Zwischen Bildern beibehalten</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Keep.qml" line="18"/>
        <source>By default, PhotoQt resets the zoom, rotation, flipping/mirroring and position when switching to a different image. For certain tasks, for example for comparing two images, it can be helpful to keep these properties.</source>
        <translation>Wenn ein neues Bild geladen wird, dann setzt PhotoQt standardmäßig den Zoom, Drehung, Spiegelung und die Position zurück. Unter gewissen Umständen, zum Beispiel beim Vergleichen zweier Bilder, kann es hilfreich sein diese Eigenschaften beizubehalten.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Keep.qml" line="31"/>
        <source>Keep Zoom, Rotation, Flip, Position</source>
        <extracomment>Remember all these levels when switching between images</extracomment>
        <translation>Behalte Vergrößerung, Drehung, Spiegelung, Position bei</translation>
    </message>
</context>
<context>
    <name>KeepVisible</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/KeepVisible.qml" line="16"/>
        <source>Visibility</source>
        <translation>Sichtbarkeit</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/KeepVisible.qml" line="17"/>
        <source>The thumbnails normally fade out when not needed, however, they can be set to stay visible. The main image is shrunk to fit into the free space. When it is zoomed in the thumbnails can be set to fade out automatically.</source>
        <translation>Die Vorschaubilder verstecken sich normalerweise wenn sie nicht benötigt werden. Sie können aber auch dauerhaft angezeigt werden. In diesem Fall wird das Hauptbild verkleinert angezeigt um in den verbliebenen freien Platz zu passen. Je nach Wunsch können die Vorschaubilder trotz allem noch versteckt werden wenn das Hauptbild vergrößert wird.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/KeepVisible.qml" line="48"/>
        <source>Keep thumbnails visible as long as the main image is not zoomed in</source>
        <translation>Halte die Vorschaubilder sichtbar solange das Hauptbild nicht vergrößert wurde</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/KeepVisible.qml" line="34"/>
        <source>Keep thumbnails visible, don&apos;t hide them past screen edge</source>
        <translation>Halte die Vorschaubilder sichtbar, verstecke sie nicht hinter der Bildschirmkante</translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Label.qml" line="19"/>
        <source>Label on Thumbnails</source>
        <translation>Aufschrift auf Vorschaubildern</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Label.qml" line="20"/>
        <source>PhotoQt can write a label with some information on the thumbnails. Currently, only the filename is available. The slider adjusts the fontsize of the text for the filename.</source>
        <translation>PhotoQt kann gewisse Informationen auf den Vorschaubildern anzeigen. Momentan besteht die mögliche Auswahl nur aus dem Dateinamen. Mit dem Schieber kann die Schriftgröße des Textes angepasst werden</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Label.qml" line="36"/>
        <source>Write Filename</source>
        <extracomment>Settings: Write the filename on a thumbnail</extracomment>
        <translation>Schreibe Dateinamen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Label.qml" line="49"/>
        <source>Fontsize</source>
        <extracomment>Settings: Write the filename with this fontsize on a thumbnail</extracomment>
        <translation>Schriftgröße</translation>
    </message>
</context>
<context>
    <name>Language</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Language.qml" line="19"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Language.qml" line="20"/>
        <source>There are a good few different languages available. Thanks to everybody who took the time to translate PhotoQt!</source>
        <translation>Es gibt schon eine kleine Auswahl an Sprachen. Danke an alle, die sich die Zeit genommen haben um PhotoQt zu übersetzen!</translation>
    </message>
</context>
<context>
    <name>LiftUp</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/LiftUp.qml" line="19"/>
        <source>Lift-Up of Thumbnails</source>
        <translation>Hochsprung der Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/LiftUp.qml" line="20"/>
        <source>When a thumbnail is hovered, it is lifted up some pixels. Here you can increase/decrease this value according to your personal preference.</source>
        <translation>Wenn ein Vorschaubild mit der Maus überfahren wird, dann springt es ein paar Pixels hoch. Hier kann dieser Wert erhöht/verringert werden, ganz wie es gewünscht wird.</translation>
    </message>
</context>
<context>
    <name>LoadingIndicator</name>
    <message>
        <location filename="../../qml/mainview/LoadingIndicator.qml" line="98"/>
        <source>Loading</source>
        <extracomment>Used as in 'Loading the image at the moment'. Please try to keep as short as possible!</extracomment>
        <translation>Lade</translation>
    </message>
</context>
<context>
    <name>Loop</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Loop.qml" line="17"/>
        <source>Looping</source>
        <extracomment>Refers to looping through the folder, i.e., from the last image go back to the first one (and vice versa)</extracomment>
        <translation>Schleife</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Loop.qml" line="18"/>
        <source>PhotoQt can loop over the images in the folder, i.e., when reaching the last image it continues to the first one and vice versa. If disabled, it will stop at the first/last image.</source>
        <translation>PhotoQt kann die Bilder im Ordner in einer Schleife durchlaufen. D.h., wenn das letzte Bild erreicht wurde, dann fängt PhotoQt wieder von vorne an (und andersherum). Wenn deaktiviert, dann stoppt es am ersten/letzten Bild.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Loop.qml" line="27"/>
        <source>Loop through images in folder</source>
        <translation>Bilder im Ordner in Schleife durchlaufen</translation>
    </message>
</context>
<context>
    <name>MainHandler</name>
    <message>
        <location filename="../../cplusplus/mainhandler.cpp" line="133"/>
        <location filename="../../cplusplus/mainhandler.cpp" line="363"/>
        <source>Image Viewer</source>
        <translation>Bildbetrachter</translation>
    </message>
    <message>
        <location filename="../../cplusplus/mainhandler.cpp" line="368"/>
        <source>Hide/Show PhotoQt</source>
        <translation>Zeige/Verstecke PhotoQt</translation>
    </message>
    <message>
        <location filename="../../cplusplus/mainhandler.cpp" line="370"/>
        <source>Quit PhotoQt</source>
        <translation>PhotoQt beenden</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="36"/>
        <source>Open File</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="38"/>
        <source>Settings</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="40"/>
        <source>Set as Wallpaper</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Als Hintergrundbild einstellen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="42"/>
        <source>Slideshow</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Diaschau</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="44"/>
        <source>setup</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'setting up a slideshow'. Keep short!</extracomment>
        <translation>einrichten</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="46"/>
        <source>quickstart</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'quickstarting a slideshow'. Keep short!</extracomment>
        <translation>schnellstart</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="48"/>
        <source>Filter Images in Folder</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Setze Dateifilter</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="50"/>
        <source>Show/Hide Metadata</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Metadaten anzeige/verbergen </translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="52"/>
        <source>Show/Hide Histogram</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Zeige/Verstecke Histogramm</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="54"/>
        <source>About PhotoQt</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Über PhotoQt</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="56"/>
        <source>Hide (System Tray)</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Verstecken (Systemablage)</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="58"/>
        <source>Quit</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="63"/>
        <source>Go to</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Go To some image'. Keep short!</extracomment>
        <translation>Gehe zu</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="65"/>
        <source>previous</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'go to previous image'. Keep short!</extracomment>
        <translation>vorheriges</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="67"/>
        <source>next</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'go to next image'. Keep short!</extracomment>
        <translation>nächstes</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="69"/>
        <source>first</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'go to first image'. Keep short!</extracomment>
        <translation>erstes</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="71"/>
        <source>last</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'go to last image'. Keep short!</extracomment>
        <translation>letztes</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="73"/>
        <source>Zoom</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Zoom image'. Keep short!</extracomment>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="85"/>
        <location filename="../../qml/mainview/MainMenu.qml" line="93"/>
        <source>reset</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Reset rotation of image'. Keep short!
----------
This is an entry in the main menu on the right, used as in 'Reset flip/mirror of image'. Keep short!</extracomment>
        <translation>zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="79"/>
        <source>Rotate</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Rotate image'. Keep short!</extracomment>
        <translation>Drehen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="81"/>
        <source>left</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Rotate image left'. Keep short!</extracomment>
        <translation>links</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="83"/>
        <source>right</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Rotate image right'. Keep short!</extracomment>
        <translation>rechts</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="87"/>
        <source>Flip</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Flip/Mirror image'. Keep short!</extracomment>
        <translation>Spiegeln</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="89"/>
        <source>horizontal</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Flip/Mirror image horizontally'. Keep short!</extracomment>
        <translation>horizontal</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="91"/>
        <source>vertical</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Flip/Mirror image vertically'. Keep short!</extracomment>
        <translation>vertikal</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="95"/>
        <source>File</source>
        <extracomment>This is an entry in the main menu on the right, used to refer to the current file (specifically the file, not directly the image). Keep short!</extracomment>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="97"/>
        <source>rename</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'rename file'. Keep short!</extracomment>
        <translation>umbenennen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="99"/>
        <source>copy</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'copy file'. Keep short!</extracomment>
        <translation>kopieren</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="101"/>
        <source>move</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'move file'. Keep short!</extracomment>
        <translation>verschieben</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="103"/>
        <source>delete</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'delete file'. Keep short!</extracomment>
        <translation>löschen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="108"/>
        <source>Scale Image</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Bild skalieren</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="110"/>
        <source>Open in default file manager</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Im Dateimanager öffnen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="149"/>
        <source>Close PhotoQt</source>
        <translation>Schließe PhotoQt</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="165"/>
        <source>Main Menu</source>
        <translation>Hauptmenü</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="332"/>
        <source>Click here to go to the online manual for help regarding shortcuts, settings, features, ...</source>
        <translation>Klicke hier um zu dem Online-Handbuch für mehr Informationen bezüglich Kurzbefehle, Einstellungen, etc. zu gehen...</translation>
    </message>
</context>
<context>
    <name>Management</name>
    <message>
        <location filename="../../qml/filemanagement/Management.qml" line="47"/>
        <source>Copy</source>
        <extracomment>As in 'Copy current image to a new location'. Keep string short!</extracomment>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Management.qml" line="52"/>
        <source>Delete</source>
        <extracomment>As in 'Delete current image'. Keep string short!</extracomment>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Management.qml" line="57"/>
        <source>Move</source>
        <extracomment>As in 'Move current image to a new location'. Keep string short!</extracomment>
        <translation>Verschieben</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Management.qml" line="62"/>
        <source>Rename</source>
        <extracomment>As in 'Rename current image'. Keep string short!</extracomment>
        <translation>Umbenennen</translation>
    </message>
</context>
<context>
    <name>MetaData</name>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="72"/>
        <source>Metadata</source>
        <translation>Metadaten</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="112"/>
        <source>No File Loaded</source>
        <translation>Keine Datei geladen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="138"/>
        <source>File Format Not Supported</source>
        <translation>Dateiformat wird nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="164"/>
        <source>Invalid File</source>
        <translation>Ungültige Datei</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="227"/>
        <source>Keep Open</source>
        <extracomment>Used as in 'Keep the metadata element open even if the cursor leaves it'</extracomment>
        <translation>Offen bleiben</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="272"/>
        <source>Click to open GPS position with online map</source>
        <translation>Klicke um GPS-Position mit einer Online-Karte zu öffnen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="19"/>
        <source>Meta Information</source>
        <translation>Metainformationen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="20"/>
        <source>PhotoQt can display a number of meta information about the image. Here you can choose which ones to show and which ones to hide.</source>
        <translation>PhotoQt kann so einige Informationen über ein Bild anzeigen. Hier kann ausgesucht werden welche Informationen angezeigt werden sollen und welche nicht.</translation>
    </message>
</context>
<context>
    <name>MouseTrigger</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MouseTrigger.qml" line="18"/>
        <source>Enable &apos;Hot Edge&apos;</source>
        <extracomment>The hot edge refers to the left and right screen edge (here in particular only to the left one). When the mouse cursor enters the hot edge area, then the metadata element is shown</extracomment>
        <translation>&apos;Heiße Kante&apos; aktivieren</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MouseTrigger.qml" line="19"/>
        <source>Per default the info widget can be shown two ways: Moving the mouse cursor to the left screen edge to fade it in temporarily (as long as the mouse is hovering it), or permanently by clicking the checkbox (checkbox only stored per session, can&apos;t be saved permanently!). Alternatively the widget can also be triggered by shortcut or main menu item. On demand the mouse triggering can be disabled, so that the widget would only show on shortcut/menu item.</source>
        <translation>Standardgemäß kann das Info-Fenster auf zwei Arten gezeigt werden: Durch Bewegen des Mauszeigers zum linken Bildschirmrand um es vorrübergehend anzuzeigen (solange der Mauszeiger sich dort befindet) oder auch dauerhaft (durch das Aktivieren der sich dort befindenden Checkbox). Alternativ kann das Fenster auch durch einen Kurzbefehl angezeigt werden. Falls gewünscht kann das Auslösen durch den Mauszeiger deaktiviert werden, so dass das Fenster nur noch durch den Kurzbefehl bzw. das Hauptmenü sichtbar gemacht werden kann.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MouseTrigger.qml" line="31"/>
        <source>DISABLE Hot Edge</source>
        <extracomment>The hot edge refers to the area on the very left of the screen that triggers the showing of the metadata element when the mouse enters it</extracomment>
        <translation>&apos;Heiße Kante&apos; DEAKTIVIEREN</translation>
    </message>
</context>
<context>
    <name>MouseWheelSensitivity</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/MouseWheelSensitivity.qml" line="18"/>
        <source>Mouse Wheel Sensitivity</source>
        <translation>Empfindlichkeit des Mausrades</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/MouseWheelSensitivity.qml" line="19"/>
        <source>The mouse can be used for various things, including many types of shortcuts. The sensitivity of the mouse wheel defines the distance the wheel has to be moved before triggering a shortcut.</source>
        <translation>Die Maus kann für verschiedenes benutzt werden, unklusive vieler Arten von Kurzbefehlen. Die Sensitivität des Mausrads definiert die Entfernung die das Mausrad bewegt werden muss bevor ein Kurzbefehl ausgelöst wird. </translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/MouseWheelSensitivity.qml" line="34"/>
        <source>Not at all sensitive</source>
        <extracomment>Refers to the sensitivity of the mouse wheel</extracomment>
        <translation>Unempfindlich</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/MouseWheelSensitivity.qml" line="59"/>
        <source>Very sensitive</source>
        <extracomment>Refers to the sensitivity of the mouse wheel</extracomment>
        <translation>Äußerst empfindlich</translation>
    </message>
</context>
<context>
    <name>Move</name>
    <message>
        <location filename="../../qml/filemanagement/Move.qml" line="18"/>
        <source>Use the file dialog to select a destination location.</source>
        <extracomment>The destination location is a location on the computer to move a file to</extracomment>
        <translation>Wähle einen Zielort in dem angezeigten Fenster aus.</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Move.qml" line="37"/>
        <source>Move Image to...</source>
        <translation>Verschiebe Bild nach...</translation>
    </message>
</context>
<context>
    <name>OnlineMap</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/OnlineMap.qml" line="17"/>
        <source>Online Map for GPS</source>
        <translation>Online-Karte für GPS</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/OnlineMap.qml" line="18"/>
        <source>If your image includes a GPS location, then a click on the location text will load this location in an online map using your default external browser. Here you can choose which online service to use (suggestions for other online maps always welcome).</source>
        <translation>Wenn ein Bild eine GPS-Position beinhaltet und auf die Position geklickt wird, dann wird diese auf einer Online-Karte angezeigt (mittels Standard-Browser). Hier kann ausgewählt werden, welcher Online-Service genutzt werden soll (Vorschläge für andere Angebote immer willkommen).</translation>
    </message>
</context>
<context>
    <name>Opacity</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/Opacity.qml" line="17"/>
        <source>Opacity</source>
        <translation>Deckkraft</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/Opacity.qml" line="18"/>
        <source>By default, the metadata widget is overlapping the main image, thus you might prefer a different alpha value for opacity to increase/decrease readability. Values can be in the range of 0-255.</source>
        <translation>Standardmäßig überlappt das Fenster für die Metadaten das Hauptbild. Daher könntest du einen anderen Alpha-Wert für die Deckkraft bevorzugen um die Lesbarkeit der Metadaten anzupassen. Der Wert kann zwischen 0 und 255 liegen.</translation>
    </message>
</context>
<context>
    <name>OpenFile</name>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="185"/>
        <source>Move focus between Places/Folders/Fileview</source>
        <extracomment>Refers to the three areas in the element for opening files</extracomment>
        <translation>Bewege Fokus zwischen Orte/Ordner/Dateien</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="187"/>
        <source>Go up/down an entry</source>
        <extracomment>Entry refers to the list of files and folders loaded in the element for opening files</extracomment>
        <translation>Gehe einen Eintrag hoch/runter</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="189"/>
        <source>Move 5 entries up/down</source>
        <extracomment>Entry refers to the list of files and folders loaded in the element for opening files</extracomment>
        <translation>Gehe 5 Einträge hoch/runter</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="191"/>
        <source>Move to the first/last entry</source>
        <extracomment>Entry refers to the list of files and folders loaded in the element for opening files</extracomment>
        <translation>Gehe zu dem ersten/letzten Eintrag</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="193"/>
        <source>Go one folder level up</source>
        <extracomment>This refers to loading the parent folder of the currently loaded folder in the element for opening files</extracomment>
        <translation>Gehe eine Ordnerebene hoch</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="195"/>
        <source>Go backwards/forwards in history</source>
        <extracomment>The history is the list of visited folders in the element for opening files</extracomment>
        <translation>Gehe zurück/vorwärts in der Chronik</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="197"/>
        <source>Load the currently highlighted item</source>
        <extracomment>Item refers to the image highlighted in the element for opening files</extracomment>
        <translation>Lade das momentan aktive Element</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="199"/>
        <source>Zoom files in/out</source>
        <extracomment>The files is the list of files in the element for opening files</extracomment>
        <translation>Dateien vergrößern/verkleinern</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="201"/>
        <source>or</source>
        <extracomment>The files/folders is the list of files/folders in the element for opening files</extracomment>
        <translation>oder</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="201"/>
        <source>Show/Hide hidden files/folders</source>
        <translation>Zeige/Verstecke versteckte Dateien/Ordner</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="202"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Other</name>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="30"/>
        <source>Warning: &apos;feh&apos; doesn&apos;t seem to be installed!</source>
        <extracomment>&quot;feh&quot; is a fixed name (name of a tool), please don't translate</extracomment>
        <translation>Warnung: &apos;feh&apos; scheint nicht installiert zu sein!</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="42"/>
        <source>Warning: &apos;nitrogen&apos; doesn&apos;t seem to be installed!</source>
        <extracomment>&quot;nitrogen&quot; is a fixed name (name of a tool), please don't translate</extracomment>
        <translation>Warnung: &apos;nitrogen&apos; scheint nicht installiert zu sein!</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="54"/>
        <source>Warning: Both &apos;feh&apos; and &apos;nitrogen&apos; don&apos;t seem to be installed!</source>
        <extracomment>&quot;feh&quot; and &quot;nitrogen&quot; are a fixed names (names of tools), please don't translate</extracomment>
        <translation>Warnung: &apos;feh and nitrogen&apos; scheinen nicht installiert zu sein!</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="66"/>
        <source>PhotoQt can use &apos;feh&apos; or &apos;nitrogen&apos; to change the background of the desktop.</source>
        <extracomment>&quot;feh&quot; and &quot;nitrogen&quot; are a fixed names (names of tools), please don't translate</extracomment>
        <translation>PhotoQt kann &apos;feh&apos; oder &apos;nitrogen&apos; benutzen um den Bildschirmhintergrund zu ändern.</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="69"/>
        <source>This is intended particularly for window managers that don&apos;t natively support wallpapers (like Blackbox, Fluxbox, or Openbox).</source>
        <extracomment>&quot;Blackbox&quot;, &quot;Fluxbox&quot; and &quot;Openbox&quot; are fixed names, please don't translate</extracomment>
        <translation>Dies ist insbesondere für Fenstermanager gedacht die dazu nicht selbst in der Lage sind (wie z.B. Blackbox, Fluxbox oder Openbox). </translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="85"/>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="92"/>
        <source>Use</source>
        <extracomment>Used as in &quot;Use 'feh'&quot; (feh is a tool)
----------
Used as in &quot;Use 'nitrogen'&quot; (nitrogen is a tool)</extracomment>
        <translation>Benutze</translation>
    </message>
</context>
<context>
    <name>OverlayColor</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="17"/>
        <source>Overlay Color</source>
        <extracomment>This refers to the background color of PhotoQt, behind the main image (the part not covered by the image itself)</extracomment>
        <translation>Überlagerungsfarbe</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="18"/>
        <source>Here you can adjust the background colour of PhotoQt (of the part not covered by an image). When using compositing or a background image, then you can also specify an alpha value, i.e. the transparency of the coloured overlay layer. When neither compositing is enabled nor a background image is set, then this colour will be the non-transparent background of PhotoQt.</source>
        <translation>Hier kann die Farbe des Hintergrunds angepasst werden (der Bereich der nicht von einem Bild bedeckt wird). Wenn Compositing oder ein Hintergrundbild verwendet wird, dann kann auch ein Alpha-Wert angegeben werden, d.h. die Transparenz der farbigen Überlagerung. Wenn weder Composite eingeschaltet ist noch ein Hintergrundbild gesetzt wurde, dann ist dies die nicht-transparente Hintergrundfarbe Photos.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="44"/>
        <source>Red:</source>
        <translation>Rot:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="66"/>
        <source>Green:</source>
        <translation>Grün:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="88"/>
        <source>Blue:</source>
        <translation>Blau:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="111"/>
        <source>Alpha:</source>
        <extracomment>This refers to the alpha value of a color (i.e., how opaque/transparent the colour is)</extracomment>
        <translation>Alpha:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="176"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
</context>
<context>
    <name>PixmapCache</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/PixmapCache.qml" line="18"/>
        <source>Pixmap Cache</source>
        <extracomment>The pixmap cache is used to cache loaded images so they can be loaded much quicker a second time</extracomment>
        <translation>Zwischenspeicher für Pixmaps</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/PixmapCache.qml" line="19"/>
        <source>Here you can adjust the size of the pixmap cache. This cache holds the loaded image elements that have been displayed. This doesn&apos;t help when first displaying an image, but can speed up its second display significantly. On the other hand, it does increase the memory in use, up to the limit set here. If you disable the cache altogether (value of 0), then each time an image is displayed, it is loaded fresh from the harddrive.</source>
        <translation>Hier kann die Größe des Zwischenspeichers für Pixmaps angepasst werden. Dieser Zwischenspeicher beinhaltet die geladenen Elemente die angezeigt wurden. Dies bringt keinen Vorteil wenn wenn die Bilder das erste Mal angezeigt werden, kann aber die folgenden Ladezeiten erheblich verringern. Gleichzeitig benötigt es jedoch mehr Speicher, bis zu dem Maximum das hier gesetzt ist. Falls der Zwischenspeicher deaktiviert wurde (setze es zu 0), dann wird jedes Mal wenn ein Bild angezeigt werden soll dies von der Festplatte frisch geladen.</translation>
    </message>
</context>
<context>
    <name>Plasma5</name>
    <message>
        <location filename="../../qml/wallpaper/modules/Plasma5.qml" line="23"/>
        <source>Sorry, Plasma 5 doesn&apos;t yet offer the feature to change the wallpaper except from their own system settings. Hopefully this will change soon, but until then there&apos;s nothing I can do about that.</source>
        <extracomment>&quot;Plasma 5&quot; is a fixed name, please don't translate</extracomment>
        <translation>Tut mir leid, Plasma 5 bietet (noch) nicht die Möglichkeit ein Hintergrundbild zu setzen, außer über deren eigene Systemeinstellungen. Hoffentlich wird sich das irgendwann ändern, doch bis dahin kann leider nichts machen.</translation>
    </message>
</context>
<context>
    <name>QuickInfo</name>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="101"/>
        <source>Filter:</source>
        <extracomment>Used as in 'Filter images'</extracomment>
        <translation>Filter:</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="159"/>
        <source>Show counter</source>
        <extracomment>The counter shows the position of the currently loaded image in the folder</extracomment>
        <translation>Zeige Zähler</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="163"/>
        <source>Show filepath</source>
        <translation>Zeige Dateipfad</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="167"/>
        <source>Show filename</source>
        <translation>Zeige Dateinamen</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="172"/>
        <source>Show closing &apos;x&apos;</source>
        <extracomment>The clsoing 'x' is the button in the top right corner of the screen for closing PhotoQt</extracomment>
        <translation>Zeige &apos;x&apos; zum Beenden</translation>
    </message>
</context>
<context>
    <name>Quickinfo</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="16"/>
        <source>Show Quickinfo (Text Labels)</source>
        <translation>Schnellinfo anzeigen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="17"/>
        <source>PhotoQt shows certain information about the current image and the folder in the top left corner of the screen. You can choose which information in particular to show there. This also includes the &apos;x&apos; for closing PhotoQt in the top right corner.</source>
        <translation>PhotoQt zeigt gewisse Informationen über das aktuelle Bild und den Ordner in der oberen linken Fensterecke an. Hier kann ausgewählt werden welche Information dort angezeigt werden soll. Dies beinhaltet ebenfalls das &apos;x&apos; für das Schließen von PhotoQt.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="30"/>
        <source>Counter</source>
        <extracomment>The counter shows the current image position in the folder</extracomment>
        <translation>Zähler</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="35"/>
        <source>Filepath</source>
        <translation>Dateipfad</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="40"/>
        <source>Filename</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="45"/>
        <source>Exit button (&apos;x&apos; in top right corner)</source>
        <translation>Schließknopf (&apos;x&apos; in der Ecke oben rechts)</translation>
    </message>
</context>
<context>
    <name>ReOpenFile</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ReOpenFile.qml" line="16"/>
        <source>Re-open last used image at startup</source>
        <translation>Zuletzt benutztes Bild beim Start wieder anzeigen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ReOpenFile.qml" line="17"/>
        <source>At startup, you can set PhotoQt to re-open the last used image and directory. This doesn&apos;t keep any zooming/scaling/mirroring from before. If you pass an image to PhotoQt on the command line, it will always favor the passed-on image.</source>
        <translation>Beim Start kann PhotoQt das zuletzt geladene Bild und Verzeichnis wieder laden. Dabei wird jedoch keine Vergrößerung/Drehung/Spigelung beibehalten. Falls ein Bild beim Start auf der Kommandozeile angegeben ist, dann wird PhotoQt immer das dortige bevorzugen.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ReOpenFile.qml" line="29"/>
        <source>Re-open last used image</source>
        <translation>Zuletzt benutztes Bild wieder anzeigen</translation>
    </message>
</context>
<context>
    <name>Rename</name>
    <message>
        <location filename="../../qml/filemanagement/Rename.qml" line="19"/>
        <source>Rename File</source>
        <translation>Datei umbenennen</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Rename.qml" line="78"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Rename.qml" line="88"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>RotateFlip</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/RotateFlip.qml" line="17"/>
        <source>Automatic Rotate/Flip</source>
        <translation>Automatisch Drehen/Spiegeln</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/RotateFlip.qml" line="18"/>
        <source>Some cameras can detect - while taking the photo - whether the camera was turned and might store this information in the image exif data. If PhotoQt finds this information, it can rotate the image accordingly or simply ignore that information.</source>
        <translation>Einige Fotoapparate können während der Aufnahme erkennen ob der Apparat gedreht wurde und können diese Information in den Metadaten des Bildes speichern. Wenn PhotoQt diese Information findet, dann kann es das Bild dementsprechend drehen oder diese Information, nach Wunsch, auch ignorieren.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/RotateFlip.qml" line="34"/>
        <source>Never rotate/flip images</source>
        <translation>Bilder niemals drehen/spiegeln</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/RotateFlip.qml" line="40"/>
        <source>Always rotate/flip images</source>
        <translation>Bilder immer drehen/spiegeln</translation>
    </message>
</context>
<context>
    <name>Scale</name>
    <message>
        <location filename="../../qml/other/Scale.qml" line="23"/>
        <source>Scale Image</source>
        <translation>Bild skalieren</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="40"/>
        <source>Current Size:</source>
        <extracomment>'Size&quot; here refers to the dimensions (numbers of pixel), expressed here as 'width x height', NOT the filesize</extracomment>
        <translation>Momentane Größe:</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="76"/>
        <source>Error! Something went wrong, unable to scale image...</source>
        <translation>Fehler! Etwas ist schiefgelaufen, das Bild konnte nicht skaliert werden...</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="107"/>
        <source>New width:</source>
        <extracomment>The width (number of pixels) of the image</extracomment>
        <translation>Neue Breite:</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="115"/>
        <source>New height:</source>
        <extracomment>The height (number of pixels) of the image</extracomment>
        <translation>Neue Höhe:</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="192"/>
        <source>Aspect Ratio</source>
        <extracomment>This is the ratio of the image = width/height</extracomment>
        <translation>Seitenverhältnis</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="230"/>
        <source>Quality</source>
        <extracomment>Refers to the quality of scaling an image</extracomment>
        <translation>Qualität</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="274"/>
        <source>Scale in place</source>
        <extracomment>Scale as in &quot;Scale image&quot;</extracomment>
        <translation>An Ort und Stelle skalieren</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="290"/>
        <source>Scale into new file</source>
        <extracomment>Scale as in &quot;Scale image&quot;</extracomment>
        <translation>Größe ändern in neuer Datei</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="293"/>
        <source>Save file as...</source>
        <translation>Speicher Datei unter...</translation>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="310"/>
        <source>Don&apos;t scale</source>
        <extracomment>Scale as in &quot;scale image&quot;</extracomment>
        <translation>Größe nicht ändern</translation>
    </message>
</context>
<context>
    <name>ScaleUnsupported</name>
    <message>
        <location filename="../../qml/other/ScaleUnsupported.qml" line="31"/>
        <source>Sorry, this fileformat cannot be scaled with PhotoQt yet!</source>
        <translation>Tut mir leid, dieses Dateiformat kann noch nicht mit PhotoQt skaliert werden!</translation>
    </message>
    <message>
        <location filename="../../qml/other/ScaleUnsupported.qml" line="42"/>
        <source>Okay, I understand</source>
        <translation>Okay, verstanden</translation>
    </message>
</context>
<context>
    <name>Set</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="85"/>
        <source>quit</source>
        <extracomment>Shortcuts: KEEP THIS STRING SHORT! It is displayed for external shortcuts as an option to quit PhotoQt after executing shortcut</extracomment>
        <translation>beenden</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="92"/>
        <source>Quit PhotoQt when executing shortcut</source>
        <translation>Nach Ausführen beenden</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="93"/>
        <source>Keep PhotoQt running when executing shortcut</source>
        <translation>Nach Ausführen PhotoQT laufen lassen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="148"/>
        <source>The command goes here</source>
        <extracomment>Shortcuts: This is the command/executable to be executed (external shortcut)</extracomment>
        <translation>Der Befehl kommt hierhin</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="201"/>
        <source>Delete shortcut</source>
        <translation>Kurzbefehl löschen</translation>
    </message>
</context>
<context>
    <name>SettingInfoOverlay</name>
    <message>
        <location filename="../../qml/settingsmanager/SettingInfoOverlay.qml" line="116"/>
        <source>Click to close</source>
        <translation>Klicke zum Beenden</translation>
    </message>
</context>
<context>
    <name>SettingsManager</name>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="63"/>
        <source>Look and Feel</source>
        <extracomment>The look of PhotoQt and how it feels and behaves</extracomment>
        <translation>Aussehen und Verhalten</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="86"/>
        <source>Thumbnails</source>
        <translation>Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="119"/>
        <source>Metadata</source>
        <translation>Metadaten</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="139"/>
        <source>Fileformats</source>
        <translation>Dateiformate</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="160"/>
        <source>Other Settings</source>
        <translation>Andere Einstellungen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="180"/>
        <source>Shortcuts</source>
        <translation>Kurzbefehle</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="237"/>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="349"/>
        <source>Restore Default Settings</source>
        <translation>Standardeinstellungen wiederherstellen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="263"/>
        <source>Exit and Discard Changes</source>
        <extracomment>Changes here refer to changes in the settings manager</extracomment>
        <translation>Schließen und Änderungen verwerfen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="282"/>
        <source>Save Changes and Exit</source>
        <extracomment>Changes here refer to changes in the settings manager</extracomment>
        <translation>Änderungen speichern und schließen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="319"/>
        <source>Do you really want to clean up the database?</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation>Datenbank wirklich aufräumen?</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="320"/>
        <source>This removes all obsolete thumbnails, thus possibly making PhotoQt a little faster.</source>
        <translation>Dies entfernt alle veralteten Vorschaubilder und bewirkt eventuell ein schnelleres Laden eines Verzeichnisses.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="321"/>
        <source>This process might take a little while.</source>
        <translation>Dieser Vorgang kann etwas Zeit in Anspruch nehmen.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="323"/>
        <source>Yes, clean is good</source>
        <extracomment>Along the lines of &quot;Yes, clean the database for thumbnails caching&quot;</extracomment>
        <translation>Jup, sauber ist gut</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="325"/>
        <source>No, don&apos;t have time for that</source>
        <extracomment>Along the lines of &quot;No, cleaning the database for thumbnails caching takes too long, don't do it&quot;</extracomment>
        <translation>Ne, hab dafür jetzt gerade keine Zeit</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="335"/>
        <source>Do you really want to ERASE the entire database?</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation>Gesamte Datenbank wirklich LÖSCHEN?</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="352"/>
        <source>Yes, go ahead</source>
        <translation>Ja, los geht&apos;s</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="368"/>
        <source>No, don&apos;t</source>
        <translation>Nein, nicht</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="338"/>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="351"/>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="366"/>
        <source>This step cannot be reversed!</source>
        <translation>Dieser Schritt kann nicht rückgängig gemacht werden!</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="340"/>
        <source>Yes, get rid of it all</source>
        <extracomment>Along the lines of &quot;Yes, empty the database for thumbnails caching&quot;</extracomment>
        <translation>Ja, alles loswerden</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="308"/>
        <source>Close settings manager</source>
        <translation>Einstellungen schließen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="317"/>
        <source>Clean Database!</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation>Datenbank aufräumen!</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="333"/>
        <source>Erase Database?</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation>Datenbank löschen?</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="337"/>
        <source>This removes every single item from the database! This step should never really be necessary. Afterwards every thumbnail has to be re-created.</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation>Dies entfernt den gesamten Inhalt der Datenbank! Dieser Schritt ist normalerweise nicht notwendig. Danach muss jedes Vorschaubild neu erstellt werden.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="342"/>
        <source>No, I want to keep it</source>
        <extracomment>Along the lines of &quot;No, don't empty the database for thumbnails caching, I want to keep it&quot;</extracomment>
        <translation>Nein, ich will alles behalten</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="350"/>
        <source>Are you sure you want to revert back to the default settings?</source>
        <translation>Bist du dir sicher, dass du die Standardeinstellungen wiederherstellen möchtest?</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="354"/>
        <source>No, thanks</source>
        <extracomment>Used in settings manager when asking for confirmation for restoring default settings (written on button)</extracomment>
        <translation>Nein, danke</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="364"/>
        <source>Set Default Shortcuts</source>
        <translation>Kurzbefehle auf Standard zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="365"/>
        <source>Are you sure you want to reset the shortcuts to the default set?</source>
        <translation>Sicher, dass Kurzbefehle auf Standardeinstellung gesetzt werden sollen?</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="367"/>
        <source>Yes, please</source>
        <translation>Ja, bitte</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="396"/>
        <source>Go to the next tab</source>
        <extracomment>The tab refers to the tabs in the settings manager</extracomment>
        <translation>Gehe zum nächsten Tab</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="398"/>
        <source>Go to the previous tab</source>
        <extracomment>The tab refers to the tabs in the settings manager</extracomment>
        <translation>Gehe zum vorherigen Tab</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="400"/>
        <source>Switch to tab 1 to 5</source>
        <extracomment>The tab refers to the tabs in the settings manager</extracomment>
        <translation>Gehe zu Tab 1-5</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="401"/>
        <source>Save settings</source>
        <translation>Einstellungen speichern</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="402"/>
        <source>Discard settings</source>
        <translation>Einstellungen verwerfen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabThumbnails.qml" line="53"/>
        <location filename="../../qml/settingsmanager/tabs/TabOther.qml" line="53"/>
        <location filename="../../qml/settingsmanager/tabs/TabMetadata.qml" line="53"/>
        <location filename="../../qml/settingsmanager/tabs/TabLookAndFeel.qml" line="54"/>
        <location filename="../../qml/settingsmanager/tabs/TabFileformats.qml" line="53"/>
        <source>Move your mouse cursor over (or click on) the different settings titles to see more information.</source>
        <translation>Bewege deinen Mauszeiger über die Titel der verschiedenen Einstellungen (oder klicke darauf) um mehr Informationen zu sehen.</translation>
    </message>
</context>
<context>
    <name>ShortcutNotifier</name>
    <message>
        <location filename="../../qml/elements/ShortcutNotifier.qml" line="12"/>
        <source>Shortcuts</source>
        <extracomment>Keep short!</extracomment>
        <translation>Kurzbefehle</translation>
    </message>
    <message>
        <location filename="../../qml/elements/ShortcutNotifier.qml" line="13"/>
        <source>You can use the following shortcuts for navigation</source>
        <translation>Die folgenden Kurzbefehle können zur Navigation verwendet werden</translation>
    </message>
    <message>
        <location filename="../../qml/elements/ShortcutNotifier.qml" line="19"/>
        <source>Got it!</source>
        <extracomment>In the sense of 'I understand it'</extracomment>
        <translation>Verstanden!</translation>
    </message>
    <message>
        <location filename="../../qml/elements/ShortcutNotifier.qml" line="22"/>
        <source>Don&apos;t show again</source>
        <extracomment>In the sense of 'Don't show me that confirmation element again'</extracomment>
        <translation>Nicht erneut anzeigen</translation>
    </message>
</context>
<context>
    <name>ShortcutsContainer</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/ShortcutsContainer.qml" line="77"/>
        <source>External</source>
        <extracomment>One of the shortcuts categories</extracomment>
        <translation>Extern</translation>
    </message>
</context>
<context>
    <name>SlideshowBar</name>
    <message>
        <location filename="../../qml/slideshow/SlideshowBar.qml" line="49"/>
        <source>Play Slideshow</source>
        <extracomment>Stays alone like that, not part of a full sentence. Written on button to allow user to play a currently paused slideshow.</extracomment>
        <translation>Diaschau fortsetzen</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowBar.qml" line="51"/>
        <source>Pause Slideshow</source>
        <extracomment>Stays alone like that, not part of a full sentence. Written on button to allow user to pause a currently playing slideshow.</extracomment>
        <translation>Diaschau anhalten</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowBar.qml" line="66"/>
        <source>Music Volume:</source>
        <translation>Musiklautstärke:</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowBar.qml" line="94"/>
        <source>Quit Slideshow</source>
        <translation>Diaschau beenden</translation>
    </message>
</context>
<context>
    <name>SlideshowSettings</name>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="10"/>
        <source>Slideshow Setup</source>
        <translation>Einrichten der Diaschau</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="25"/>
        <source>There are several settings that can be adjusted for a slideshow, like the time between the image, if and how long the transition between the images should be, and also a music file can be specified that will be played in the background.</source>
        <translation>Es gibt ein paar Einstellungen die angepasst werden können, z.B. die Zeit zwischen den Bildern, ob und wie lang ein weicher Übergang stattfinden soll, und es kann auch eine Musikdatei angegeben werden die dann im Hintergrund abgespielt wird.</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="32"/>
        <source>Once you have set the desired options, you can also start a slideshow the next time via &apos;Quickstart&apos;, i.e. skipping this settings window.</source>
        <translation>Wenn einmal die gewünschten Einstellungen gesetzt sind, dann kann eine Diaschau das nächste Mal auch via &apos;Schnellstart&apos; gestartet werden, d.h. das Einstellungsfenster wird dann übersprungen.</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="43"/>
        <source>Time in between</source>
        <extracomment>This refers to the time the slideshow waits before loading the next image</extracomment>
        <translation>Zwischenzeit</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="50"/>
        <source>Adjust the time between the images, i.e., how long the slideshow will wait before loading the next image.</source>
        <translation>Hier kann die Zeit zwischen den Bildern angepasst werden, d.h. wie lange die Diaschau wartet bevor das nächste Bild geladen wird.</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="91"/>
        <source>Smooth Transition</source>
        <extracomment>This refers to the transition between two images, how quickly they fade into each other (if at all)</extracomment>
        <translation>Weicher Übergang</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="98"/>
        <source>Here you can set whether you want the images to fade into each other and how fast they are to do that.</source>
        <translation>Hier kann eingestellt werden ob die Bilder beim Wechsel ineinander übergehen sollen und wie schnell das geschehen soll.</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="113"/>
        <source>No Transition</source>
        <extracomment>This refers to the fading between images. No transition means that the new images simply replaces the old image instantly</extracomment>
        <translation>Kein Übergang</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="130"/>
        <source>Long Transition</source>
        <extracomment>This refers to the fading between images. A long transition means that two images fade very slowly into each other</extracomment>
        <translation>Langer Übergang</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="143"/>
        <source>Shuffle and Loop</source>
        <extracomment>Shuffle means putting the list of all the files in the current folder into random order. Loop means that the slideshow will start again from the bginning when it reaches the last image.</extracomment>
        <translation>Mischen und Schleife</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="150"/>
        <source>If you want PhotoQt to loop over all images (i.e., once it shows the last image it starts from the beginning), or if you want PhotoQt to load your images in random order, you can check either or both boxes below. Note in the case of shuffling that no image will be shown twice before every image has been shown once.</source>
        <translation>Falls gewünscht wird, dass PhotoQt die Bilder in einer Schleife durchläuft (d.h. nach dem letzten Bild fängt es wieder von vorne an), oder dass PhotoQt alle Bilder in einer zufälligen Reihenfolge lädt, dann kann eines oder beide der folgenden Kästchen angekreuzt werden. Beachte, dass, wenn die Bilder in zufälliger Reihenfolge angezeigt werden, keines zweimal gezeigt wird bevor alle einmal geladen wurden.</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="157"/>
        <source>Loop over images</source>
        <extracomment>This means that once the last image is reaches PhotoQt will start again from the first one</extracomment>
        <translation>Bilder in Schleife durchlaufen</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="164"/>
        <source>Shuffle images</source>
        <extracomment>This means to put the list of files into random order.</extracomment>
        <translation>Bilder zufällig mischen</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="177"/>
        <source>Quickinfo</source>
        <extracomment>The quickinfo refers to the labels (like position in the folder, filename, closing 'x') that are normally shown on the main image</extracomment>
        <translation>Schnellinfo</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="185"/>
        <source>Depending on your setup, PhotoQt displays some information at the top edge, like position in current directory or file path/name. Here you can disable them temporarily for the slideshow.</source>
        <translation>Je nach Einstellungen zeigt PhotoQt an der oberen Bildschirmkante verschiedene Schnellinformationen an, wie z.B. die momentane Position im Verzeichnis oder den Dateinamen/-pfad. Hier können diese für die Dauer der Diaschau versteckt werden.</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="191"/>
        <source>Hide Quickinfo</source>
        <extracomment>The quickinfo refers to the labels (like position in the folder, filename, closing 'x') that are normally shown on the main image</extracomment>
        <translation>Schnellinfo verstecken</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="203"/>
        <source>Background Music</source>
        <translation>Hintergrundmusik</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="210"/>
        <source>Some might like to listen to some music while the slideshow is running. Here you can select a music file you want to be played in the background.</source>
        <translation>Manch einer hört während einer Diaschau gerne etwas Musik im Hintergrund. Hier kann eine Musikdatei dafür ausgewählt werden.</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="217"/>
        <source>Enable Music</source>
        <translation>Musik einschalten</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="247"/>
        <source>Click here to select a music file...</source>
        <translation>Klicke hier um eine Musikdatei auszuwählen...</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="270"/>
        <source>Okay, lets start</source>
        <extracomment>In the sense of 'ok, save the slideshow settings and lets start with the slideshow'</extracomment>
        <translation>Okay, los geht&apos;s</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="275"/>
        <source>Wait, maybe later</source>
        <extracomment>In the sense of, 'no, don't save the slideshow settings and don't start a slideshow'</extracomment>
        <translation>Warte, vielleicht später</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="280"/>
        <source>Save changes, but don&apos;t start just yet</source>
        <extracomment>In the sense of 'ok, save the slideshow settings, but do not start a slideshow'</extracomment>
        <translation>Änderungen speichern, aber noch nicht anfangen</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="307"/>
        <source>Select music file...</source>
        <translation>Musikdatei auswählen …</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="308"/>
        <source>Music Files</source>
        <translation>Musikdateien</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="309"/>
        <source>All Files</source>
        <translation>Alle Dateien</translation>
    </message>
</context>
<context>
    <name>SortBy</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="17"/>
        <source>Sort Images</source>
        <translation>Bilder sortieren</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="19"/>
        <source>Images in the current folder can be sorted in varios ways. They can be sorted by filename, natural name (e.g., file10.jpg comes after file9.jpg and not after file1.jpg), filesize, and date, all of that both ascending or descending.</source>
        <translation>Die Bilder in einem Ordner können auf verschiedene Weisen sortiert werden, entweder nach Dateinamen, natürlichem Namen (z.B., datei10.jpg kommt nach datei9.jpg und nicht nach datei1.jpg), Dateigröße, oder nach Datum. All dies kann entweder in aufsteigender oder absteigender Reihenfolge geschehen.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="34"/>
        <source>Sort by:</source>
        <extracomment>As in &quot;Sort the images by some criteria&quot;</extracomment>
        <translation>Sortieren nach:</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="43"/>
        <source>Name</source>
        <extracomment>Refers to the filename</extracomment>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="45"/>
        <source>Natural Name</source>
        <extracomment>Sorting by natural name means file10.jpg comes after file9.jpg and not after file1.jpg</extracomment>
        <translation>Natürlicher Name</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="47"/>
        <source>Date</source>
        <extracomment>The date the file was created</extracomment>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="48"/>
        <source>Filesize</source>
        <translation>Dateigröße</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="57"/>
        <source>Ascending</source>
        <translation>Aufsteigend</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="65"/>
        <source>Descending</source>
        <translation>Absteigend</translation>
    </message>
</context>
<context>
    <name>Spacing</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Spacing.qml" line="18"/>
        <source>Spacing Between Thumbnails</source>
        <translation>Abstand zwischen Vorschaubildern</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Spacing.qml" line="19"/>
        <source>The thumbnails are shown in a row at the lower or upper edge (depending on your setup). They are lined up side by side. Per default, there&apos;s no empty space between them.</source>
        <translation>Die Vorschaubilder werden an der unteren/oberen Kante (je nach Einstellung) aufgereiht, Seite an Seite. Standardmäßig befindet sich kein Abstand zwischen denen.</translation>
    </message>
</context>
<context>
    <name>Startup</name>
    <message>
        <location filename="../../qml/other/Startup.qml" line="85"/>
        <source>Welcome to PhotoQt</source>
        <translation>Willkommen bei PhotoQt</translation>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="100"/>
        <source>PhotoQt was successfully installed!</source>
        <translation>PhotoQt wurde erfolgreich installiert!</translation>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="101"/>
        <source>An image viewer packed with features and adjustable in every detail awaits you... Go, enjoy :-)</source>
        <translation>Ein Bildbetrachter vollgepackt mit vielen Funktionen und anpassbar bis ins kleinste Detail erwartet dich... Los, viel Spaß :-)</translation>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="102"/>
        <source>PhotoQt was successfully updated!</source>
        <translation>PhotoQt wurde erfolgreich aktualisiert!</translation>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="103"/>
        <source>Many new features and bug fixes await you... Go, enjoy :-)</source>
        <translation>Viele neue Funktionen und Fehlerkorrekturen erwarten dich... Los, viel Spaß :-)</translation>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="113"/>
        <source>Lets get started!</source>
        <translation>Los gehts!</translation>
    </message>
</context>
<context>
    <name>Strings</name>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="8"/>
        <source>Alt</source>
        <extracomment>Refers to a keyboard modifier</extracomment>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="10"/>
        <source>Ctrl</source>
        <extracomment>Refers to a keyboard modifier</extracomment>
        <translation>Strg</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="12"/>
        <source>Shift</source>
        <extracomment>Refers to a keyboard modifier</extracomment>
        <translation>Umschalt</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="14"/>
        <source>Page Up</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Bild auf</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="16"/>
        <source>Page Down</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Bild ab</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="18"/>
        <source>Meta</source>
        <extracomment>Refers to the key that usually has the 'Windows' symbol on it</extracomment>
        <translation>Meta</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="20"/>
        <source>Keypad</source>
        <extracomment>Refers to the key that triggers the number block on keyboards</extracomment>
        <translation>Ziffernblock</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="22"/>
        <source>Escape</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Escape</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="24"/>
        <source>Right</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="26"/>
        <source>Left</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="28"/>
        <source>Up</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation>Aufwärts</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="30"/>
        <source>Down</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation>Abwärts</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="32"/>
        <source>Space</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Leertaste</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="34"/>
        <source>Delete</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="36"/>
        <source>Backspace</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Rücktaste</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="38"/>
        <source>Home</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Pos1</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="40"/>
        <source>End</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="42"/>
        <source>Insert</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="44"/>
        <source>Tab</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation>Tabulator</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="46"/>
        <source>Return</source>
        <extracomment>'Return' refers to the enter key of the number block - please try to make the translations of 'Return' and 'Enter' (the main button) different if possible!</extracomment>
        <translation>Eingabe</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="48"/>
        <source>Enter</source>
        <extracomment>'Enter' refers to the main enter key - please try to make the translations of 'Return' (in the number block) and 'Enter' different if possible!</extracomment>
        <translation>Enter</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="54"/>
        <source>Left Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation>Linksklick</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="56"/>
        <source>Right Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation>Rechtsklick</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="58"/>
        <source>Middle Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation>Mittelklick</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="60"/>
        <source>Wheel Up</source>
        <extracomment>Refers to the mouse wheel</extracomment>
        <translation>Mausrad auf</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="62"/>
        <source>Wheel Down</source>
        <extracomment>Refers to the mouse wheel</extracomment>
        <translation>Mausrad ab</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="64"/>
        <source>East</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation>Osten</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="66"/>
        <source>South</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation>Süden</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="68"/>
        <source>West</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation>Westen</translation>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="70"/>
        <source>North</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation>Norden</translation>
    </message>
</context>
<context>
    <name>TabFileformats</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabFileformats.qml" line="43"/>
        <source>Fileformats</source>
        <translation>Dateiformate</translation>
    </message>
</context>
<context>
    <name>TabLookAndFeel</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabLookAndFeel.qml" line="44"/>
        <source>Look and Feel</source>
        <extracomment>The look of PhotoQt and how it feels and behaves</extracomment>
        <translation>Aussehen und Verhalten</translation>
    </message>
</context>
<context>
    <name>TabMetadata</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabMetadata.qml" line="43"/>
        <source>Image Metadata</source>
        <translation>Bild Metadaten</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabMetadata.qml" line="65"/>
        <source>PhotoQt can display different information of and about each image. The element for this information is hidden on the left side of the screen and fades in when the mouse cursor gets close to the left screen edge and/or when the set shortcut is triggered. On demand, the triggering by mouse movement can be disabled by checking the box below.</source>
        <extracomment>Introduction text of metadata tab in settings manager</extracomment>
        <translation>PhotoQt kann verschiedene Informationen über ein Bild anzeigen. Das Element dafür versteckt sich auf der linken Seite des Bildschirms, und wird eingeblendet wenn der Mauszeiger an diese Kante bewegt wird und/oder wenn der Kurzbefehl gedrückt wurde. Nach Bedarf kann das Auslösen per Mauszeiger deaktiviert werden.</translation>
    </message>
</context>
<context>
    <name>TabOther</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabOther.qml" line="43"/>
        <source>Other Settings</source>
        <translation>Andere Einstellungen</translation>
    </message>
</context>
<context>
    <name>TabShortcuts</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="43"/>
        <source>Shortcuts</source>
        <translation>Kurzbefehle</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="75"/>
        <source>Pressing the left button of the mouse and moving it around can be used for moving an image around. If you put it to use for this purpose then any shortcut involving the left mouse button will have no effect! Note that it is not recommended to disable this if you do not have any other means to move an image around (e.g., touchscreen)!</source>
        <translation>Die linke Maustaste kann zum Bewegen eines Bildes benutzt werden. Wenn dies so eingestellt ist, dann wird jeder Kurzbefehl der die linke Maustaste beinhaltet wirkungslos! Beachte, dass es nicht empfohlen wird dies zu deaktivieren wenn es keinen andere Weg gibt das Bild zu bewegen (z.B. mittels Touchscreen)!</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="87"/>
        <source>Mouse: Left button click-and-move</source>
        <translation>Maus: Linke Taste drücken und bewegen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="52"/>
        <source>Here you can adjust the shortcuts, add new or remove existing ones, or change a key/mouse combination. The shortcuts are grouped into 4 different categories for internal commands plus a category for external commands. The boxes on the right side contain all the possible commands. To add a shortcut for one of the available functions simply click on it. This will automatically open another element where you can set the desired shortcut.</source>
        <translation>Hier können die Kurzbefehle angepasst werden: Neue hinzufügen, alte entfernen, oder eine Tasten-/Maus-Kombination ändern. Die Kurzbefehle sind in 4 Kategorien für interne Befehle, plus einer Kategorie für externe Befehle unterteilt. Die Rechtecke auf der rechten Seite enthalten alle verfügbaren Kurzbefehle. Ein Kurzbefehl kann durch einen einfachen Klick auf das gewünschte Rechteck hinzugefügt werden. Dies wird automatisch ein weiteres Fenster öffnen in dem die gewünschte Kombination eingestellt werden kann.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="97"/>
        <source>Set default shortcuts</source>
        <translation>Kurzbefehle auf Standard zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="106"/>
        <source>Navigation</source>
        <extracomment>A shortcuts category: navigating images</extracomment>
        <translation>Navigation</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="107"/>
        <source>Open New File</source>
        <translation>Neue Datei öffnen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="108"/>
        <source>Filter Images in Folder</source>
        <translation>Setze Dateifilter</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="109"/>
        <source>Next Image</source>
        <translation>Nächstes Bild</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="110"/>
        <source>Previous Image</source>
        <translation>Vorheriges Bild</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="111"/>
        <source>Go to first Image</source>
        <translation>Gehe zu erstem Bild</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="112"/>
        <source>Go to last Image</source>
        <translation>Gehe zu letztem Bild</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="113"/>
        <source>Hide to System Tray (if enabled)</source>
        <translation>In Systemleiste verbergen (falls aktiviert)</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="114"/>
        <source>Quit PhotoQt</source>
        <translation>PhotoQt beenden</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="122"/>
        <source>Image</source>
        <extracomment>A shortcuts category: image manipulation</extracomment>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="123"/>
        <source>Zoom In</source>
        <translation>Vergrößern</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="124"/>
        <source>Zoom Out</source>
        <translation>Verkleinern</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="125"/>
        <source>Zoom to Actual Size</source>
        <translation>Tatsächliche Größe</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="126"/>
        <source>Reset Zoom</source>
        <translation>Vergrößerung zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="127"/>
        <source>Rotate Right</source>
        <translation>Rechts drehen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="128"/>
        <source>Rotate Left</source>
        <translation>Links drehen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="129"/>
        <source>Reset Rotation</source>
        <translation>Drehung zurücksetzen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="130"/>
        <source>Flip Horizontally</source>
        <translation>Horizontal spiegeln</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="131"/>
        <source>Flip Vertically</source>
        <translation>Senkrecht spiegeln</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="132"/>
        <source>Scale Image</source>
        <translation>Bild skalieren</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="133"/>
        <source>Play/Pause image animation</source>
        <translation>Bild-Animation abspielen/anhalten</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="141"/>
        <source>File</source>
        <extracomment>A shortcuts category: file management</extracomment>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="142"/>
        <source>Rename File</source>
        <translation>Datei umbenennen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="143"/>
        <source>Delete File</source>
        <translation>Datei löschen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="144"/>
        <source>Delete File (without confirmation)</source>
        <translation>Datei löschen (ohne Bestätigung)</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="145"/>
        <source>Copy File to a New Location</source>
        <translation>Datei kopieren</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="146"/>
        <source>Move File to a New Location</source>
        <translation>Datei verschieben</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="147"/>
        <source>Copy Image to Clipboard</source>
        <translation>Kopiere Bild in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="155"/>
        <source>Other</source>
        <extracomment>A shortcuts category: other functions</extracomment>
        <translation>Andere</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="156"/>
        <source>Hide/Show Exif Info</source>
        <translation>Exif-Info anzeigen/verbergen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="162"/>
        <source>Show Histogram</source>
        <translation>Zeige Histogramm</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="163"/>
        <source>Upload to imgur.com (anonymously)</source>
        <translation>Hochladen auf imgur.com (anonym)</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="164"/>
        <source>Upload to imgur.com user account</source>
        <translation>Hochladen auf imgur.com (Benutzerkonto)</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="172"/>
        <source>External</source>
        <extracomment>A shortcuts category: external commands</extracomment>
        <translation>Extern</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="245"/>
        <source/>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="157"/>
        <source>Show Settings</source>
        <translation>Einstellungen anzeigen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="158"/>
        <source>Start Slideshow</source>
        <translation>Starte Diaschau</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="159"/>
        <source>Start Slideshow (Quickstart)</source>
        <translation>Starte Diaschau (Schnellstart)</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="160"/>
        <source>About PhotoQt</source>
        <translation>Über PhotoQt</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="161"/>
        <source>Set as Wallpaper</source>
        <translation>Als Hintergrundbild einstellen</translation>
    </message>
</context>
<context>
    <name>TabThumbnails</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabThumbnails.qml" line="43"/>
        <source>Thumbnails</source>
        <translation>Vorschaubilder</translation>
    </message>
</context>
<context>
    <name>ThumbnailSize</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/ThumbnailSize.qml" line="18"/>
        <source>Thumbnail Size</source>
        <translation>Größe der Vorschaubilder</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/ThumbnailSize.qml" line="19"/>
        <source>Here you can adjust the thumbnail size. You can set it to any size between 20 and 256 pixel. Per default it is set to 80 pixel, but the optimal size depends on the screen resolution.</source>
        <translation>Hier kann die Größe der Vorschaubilder angepasst werden. Sie kann beliebig zwischen 20 und 256 Pixeln gewählt werden. Als Standard sind 80 Pixel gesetzt, jedoch hängt der optimale Wert von der Bildschirmauflösung ab.</translation>
    </message>
</context>
<context>
    <name>TopOrBottom</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/TopOrBottom.qml" line="18"/>
        <source>Top or Bottom</source>
        <extracomment>Refers to the top and bottom screen edges</extracomment>
        <translation>Oben oder Unten</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/TopOrBottom.qml" line="19"/>
        <source>Per default the bar with the thumbnails is shown at the lower screen edge. However, some might find it nice and handy to have the thumbnail bar at the upper edge.</source>
        <translation>Standardmäßig werden die Vorschaubilder am unteren Bildschirmrand angezeigt. Jedoch bevorzugt manch einer vielleicht sie am oberen Rand zu haben.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/TopOrBottom.qml" line="36"/>
        <source>Show at lower edge</source>
        <extracomment>Edge refers to a screen edge</extracomment>
        <translation>An unterer Kante anzeigen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/TopOrBottom.qml" line="44"/>
        <source>Show at upper edge</source>
        <extracomment>Edge refers to a screen edge</extracomment>
        <translation>An oberer Kante anzeigen</translation>
    </message>
</context>
<context>
    <name>Transition</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Transition.qml" line="19"/>
        <source>Smooth Transition</source>
        <extracomment>The transition refers to images fading into one another when switching between them</extracomment>
        <translation>Weicher Übergang</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Transition.qml" line="20"/>
        <source>Switching between images can be done smoothly, the new image can be set to fade into the old image. &apos;No transition&apos; means, that the previous image is simply replaced instantly by the new image.</source>
        <translation>Der Übergang zwischen zwei Bildern kann weich dargestellt werden, d.h. das neue Bild wird über das alte Bild eingeblendet. &apos;Kein Übergang&apos; bedeutet, dass das alte Bild einfach mit dem neuem Bild ersetzt wird.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Transition.qml" line="34"/>
        <source>No Transition</source>
        <extracomment>No transition means that images are simply replaced when switching between them, no cross-fading</extracomment>
        <translation>Kein Übergang</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Transition.qml" line="57"/>
        <source>Long Transition</source>
        <extracomment>A very long transition between images, they slowly fade into each other</extracomment>
        <translation>Langer Übergang</translation>
    </message>
</context>
<context>
    <name>TransparencyMarker</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TransparencyMarker.qml" line="17"/>
        <source>Transparency Marker</source>
        <extracomment>Refers to looping through the folder, i.e., from the last image go back to the first one (and vice versa)</extracomment>
        <translation>Transparenzmarker</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TransparencyMarker.qml" line="18"/>
        <source>Transparency in image viewers is often signalled by displaying a pattern of light and dark grey squares. PhotoQt can do the same, by default it will, however, show transparent areas as transparent.</source>
        <translation>Transparenz wird in Bildbetrachtern oftmals mit Hilfe eines Schachbrettmusters mit hell- und dunkelgrauen Quadraten verdeutlicht. PhotoQt kann dies genauso handhaben, zeigt aber standardmäßig transparente Bereiche als transparent an.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TransparencyMarker.qml" line="27"/>
        <source>Show Transparency Marker</source>
        <translation>Zeige Transparenzmarker</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="17"/>
        <source>Hide to Tray Icon</source>
        <translation>In Systemleiste verbergen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="18"/>
        <source>PhotoQt can make use of a tray icon in the system tray. It can also hide to the system tray when closing it instead of quitting. It is also possible to start PhotoQt already minimised to the tray (e.g. at system startup) when called with &quot;--start-in-tray&quot;.</source>
        <translation>PhotoQt kann von der Systemablage Gebrauch machen und dort ein Ablagesymbol erstellen. Nach Wunsch kann es auch beim Schließen sich nur dorthin minimieren und im Hintergrund weiterlaufen. Es ist ebenfalls möglich PhotoQt direkt minimiert in der Systemablage zu starten (z.B. beim Hochfahren des Computers) wenn es mit der Option &quot;--start-in-tray&quot; ausgeführt wird.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="33"/>
        <source>No tray icon</source>
        <extracomment>The tray icon is the icon in the system tray</extracomment>
        <translation>Kein Systemleistensymbol</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="40"/>
        <source>Hide to tray icon</source>
        <extracomment>The tray icon is the icon in the system tray</extracomment>
        <translation>In Systemleiste verbergen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="46"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <extracomment>The tray icon is the icon in the system tray</extracomment>
        <translation>Systemleistensymbol anzeigen, aber nicht in diesem verbergen</translation>
    </message>
</context>
<context>
    <name>TweaksFileType</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksFileType.qml" line="24"/>
        <source>All supported images</source>
        <translation>Alle unterstützten Bilder</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksFileType.qml" line="26"/>
        <location filename="../../qml/openfile/tweaks/TweaksFileType.qml" line="28"/>
        <location filename="../../qml/openfile/tweaks/TweaksFileType.qml" line="30"/>
        <source>images</source>
        <extracomment>Used as in 'Qt images'
----------
Used as in 'GraphicsMagick images'
----------
Used as in 'LibRaw images'</extracomment>
        <translation>Bilder</translation>
    </message>
</context>
<context>
    <name>TweaksPreview</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksPreview.qml" line="46"/>
        <source>En-/Disable hover preview</source>
        <extracomment>The hover preview shows the image behind the files in the element for opening files</extracomment>
        <translation>Große Vorschau beim Überfahren mit der Maus ein-/ausschalten</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksPreview.qml" line="85"/>
        <source>Use HIGH QUALITY preview</source>
        <extracomment>The preview shows the image behind the files in the element for opening files, use high quality preview</extracomment>
        <translation>Vorschau in HOHER QUALITÄT</translation>
    </message>
</context>
<context>
    <name>TweaksRememberLocation</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksRememberLocation.qml" line="45"/>
        <source>Remember current folder between sessions</source>
        <translation>Behalte aktuellen Order zwischen Sitzungen</translation>
    </message>
</context>
<context>
    <name>TweaksThumbnails</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksThumbnails.qml" line="46"/>
        <source>En-/Disable image thumbnails</source>
        <extracomment>The thumbnails in the element for opening files</extracomment>
        <translation>Vorschaubilder ein-/ausschalten</translation>
    </message>
</context>
<context>
    <name>TweaksUserPlaces</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksUserPlaces.qml" line="46"/>
        <source>Show/Hide UserPlaces</source>
        <extracomment>The thumbnails in the element for opening files</extracomment>
        <translation>Zeige/Verstecke Benutzerorte</translation>
    </message>
</context>
<context>
    <name>TweaksViewMode</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksViewMode.qml" line="43"/>
        <source>Show files as list</source>
        <translation>Dateien in einer Liste anzeigen</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksViewMode.qml" line="77"/>
        <source>Show files as grid</source>
        <translation>Dateien in einem Gitter anzeigen</translation>
    </message>
</context>
<context>
    <name>TweaksZoom</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksZoom.qml" line="19"/>
        <source>Zoom:</source>
        <extracomment>As in 'Zoom the files shown'</extracomment>
        <translation>Vergrößerung:</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksZoom.qml" line="34"/>
        <source>Move slider to adjust the size of files</source>
        <translation>Bewege den Schieberegler um die Größe der Dateien anzupassen</translation>
    </message>
</context>
<context>
    <name>UserPlaces</name>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="735"/>
        <source>Show standard locations</source>
        <extracomment>The standard/common folders (like Home, Desktop, ...)</extracomment>
        <translation>Zeige Standardorte</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="739"/>
        <source>Show user locations</source>
        <extracomment>The user set folders (or favorites) in the element for opening files</extracomment>
        <translation>Zeige Benutzerorte</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="743"/>
        <source>Show devices</source>
        <extracomment>The storage devices (like USB keys)</extracomment>
        <translation>Geräte anzeigen</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="111"/>
        <source>Home</source>
        <extracomment>This is used as name of the HOME folder</extracomment>
        <translation>Persönlicher Ordner</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="75"/>
        <source>No categories enabled! They can be shown on right click!</source>
        <translation>Es sind keine Kategorien aktiviert! Dies kann mit einem Rechtsklick angepasst werden!</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="115"/>
        <source>Desktop</source>
        <extracomment>This is used as name of the DESKTOP folder</extracomment>
        <translation>Schreibtisch</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="119"/>
        <source>Pictures</source>
        <extracomment>This is used as name of the PICTURES folder</extracomment>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="123"/>
        <source>Downloads</source>
        <extracomment>This is used as name of the DOWNLOADS folder</extracomment>
        <translation>Downloads</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="205"/>
        <source>Standard</source>
        <extracomment>This is the category title of standard/common folders (like Home, Desktop, ...) in the element for opening files</extracomment>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="285"/>
        <source>No user places set yet. You can drag and drop folders here to bookmark them.</source>
        <translation>Es gibt noch keine Benutzerorte. Neue können durch das Ziehen von Ordnern hierhin erstellt werden.</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="475"/>
        <source>Places</source>
        <extracomment>This is the category title of user set folders (or favorites) in the element for opening files</extracomment>
        <translation>Orte</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="527"/>
        <source>Remove entry</source>
        <extracomment>Remove an entry from the list of user places (or favorites) in the element for opening files</extracomment>
        <translation>Eintrag entfernen</translation>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="699"/>
        <source>Storage devices</source>
        <extracomment>This is the category title of storage devices to open (like USB keys) in the element for opening files</extracomment>
        <translation>Speichergeräte</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="11"/>
        <source>Set as Wallpaper</source>
        <translation>Als Hintergrundbild verwenden</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="29"/>
        <source>Window Manager</source>
        <translation>Fenstermanager</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="37"/>
        <source>PhotoQt tries to detect your window manager according to the environment variables set by your system. If it still got it wrong, you can change the window manager manually.</source>
        <translation>PhotoQt versucht deinen Fenstermanager anhand der Umgebungsvariablen deines Systems zu ermitteln. Wenn es jedoch trotzdem den Falschen ermittelt hat, kann er hier manuell umgestellt werden.</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="46"/>
        <source>Other</source>
        <extracomment>'Other' as in 'Other window managers'</extracomment>
        <translation>Andere</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="167"/>
        <source>Okay, do it!</source>
        <extracomment>Along the lines of: Set image as wallpaper</extracomment>
        <translation>Okay, mach es!</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="173"/>
        <source>Nooo, don&apos;t!</source>
        <extracomment>Along the lines of: Don't set image as wallpaper</extracomment>
        <translation>Neeiiin, nicht!</translation>
    </message>
</context>
<context>
    <name>WindowMode</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/WindowMode.qml" line="16"/>
        <source>Window Mode</source>
        <translation>Fenstermodus</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/WindowMode.qml" line="17"/>
        <source>PhotoQt can be used both in fullscreen mode or as a normal window. It was designed with a fullscreen/maximised application in mind, thus it will look best when used that way, but will work just as well any other way.</source>
        <translation>PhotoQt kann sowohl als Vollbild als auch als normales Fenster dargestellt werden. Das Design passt am besten wenn es entweder in Vollbild oder maximiert dargestellt wird, aber es funktioniert problemlos egal wie es benutzt wird.</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/WindowMode.qml" line="29"/>
        <source>Run PhotoQt in Window Mode</source>
        <translation>PhotoQt in Fenstermodus setzen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/WindowMode.qml" line="35"/>
        <source>Show Window Decoration</source>
        <translation>Fensterdekoration anzeigen</translation>
    </message>
</context>
<context>
    <name>XFCE4</name>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="32"/>
        <source>Warning: &apos;xfconf-query&apos; doesn&apos;t seem to be available! Are you sure XFCE4 is installed?</source>
        <extracomment>&quot;xfconf-query&quot; and &quot;XFCE4&quot; are fixed names, please don't translate</extracomment>
        <translation>Warnung: &apos;xfconf-query&apos; scheint nicht verfügbar zu sein! Sicher, dass XFCE4 installiert ist?</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="45"/>
        <source>The wallpaper can be set to any of the available monitors (one or any combination).</source>
        <translation>Dieses Hintergrundbild kann auf einem beliebigen Bildschirm gesetzt werden (und auch jeder Kombination).</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="63"/>
        <source>Screen</source>
        <extracomment>Used as in 'Screen #4'</extracomment>
        <translation>Bildschirm</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="99"/>
        <source>There are several picture options that can be set for the wallpaper image.</source>
        <extracomment>'picture options' refers to options like stretching the image to fill the background, or tile the image, center it, etc.</extracomment>
        <translation>Es gibt verschiedene Optionen, die für dieses Hintergrundbild gesetzt werden können.</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="115"/>
        <source>Automatic</source>
        <extracomment>&quot;Automatic&quot; means automatically choose how to set the image as wallpaper</extracomment>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="122"/>
        <source>Centered</source>
        <extracomment>&quot;Centered&quot; means set the image centered as wallpaper</extracomment>
        <translation>Zentriert</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="129"/>
        <source>Tiled</source>
        <extracomment>&quot;Tiled&quot; means repeat the wallpaper image until the full screen is covered</extracomment>
        <translation>Gekachelt</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="136"/>
        <source>Stretched</source>
        <extracomment>&quot;Stretched&quot; means make the wallpaper image fill the screen without regard to its aspect ratio</extracomment>
        <translation>Gestreckt</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="143"/>
        <source>Scaled</source>
        <extracomment>&quot;Scaled&quot; means that the wallpaper image is scaled to properly fill the screen</extracomment>
        <translation>Skaliert</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="150"/>
        <source>Zoomed</source>
        <extracomment>&quot;Zoomed&quot; means that the wallpaper image is zoomed to fill the screen</extracomment>
        <translation>Vergrößert</translation>
    </message>
</context>
<context>
    <name>handlestuff</name>
    <message>
        <location filename="../../qml/openfile/handlestuff.js" line="193"/>
        <source>device</source>
        <extracomment>This is used for the name of a storage device (e.g., USB), as in '5 GB device'</extracomment>
        <translation>Gerät</translation>
    </message>
</context>
<context>
    <name>imageprovider</name>
    <message>
        <location filename="../../cplusplus/imageprovider/imageproviderthumbnail.cpp" line="41"/>
        <location filename="../../cplusplus/imageprovider/imageproviderfull.cpp" line="41"/>
        <source>File failed to load, it doesn&apos;t exist!</source>
        <translation>Laden der Datei fehlgeschlagen, sie existiert nicht!</translation>
    </message>
</context>
<context>
    <name>metadata</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="71"/>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="110"/>
        <location filename="../../qml/mainview/MetaData.qml" line="363"/>
        <source>Filename</source>
        <extracomment>Keep string short!</extracomment>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="73"/>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="112"/>
        <location filename="../../qml/mainview/MetaData.qml" line="368"/>
        <source>Filesize</source>
        <extracomment>Keep string short!</extracomment>
        <translation>Dateigröße</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="75"/>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="114"/>
        <location filename="../../qml/mainview/MetaData.qml" line="373"/>
        <source>Image</source>
        <extracomment>Used as in &quot;Image 3/16&quot;. The numbers (position of image in folder) are added on automatically. Keep string short!</extracomment>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="77"/>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="116"/>
        <location filename="../../qml/mainview/MetaData.qml" line="379"/>
        <location filename="../../qml/mainview/MetaData.qml" line="383"/>
        <source>Dimensions</source>
        <extracomment>The dimensions of the loaded image. Keep string short!</extracomment>
        <translation>Bildgröße</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="79"/>
        <location filename="../../qml/mainview/MetaData.qml" line="390"/>
        <source>Make</source>
        <extracomment>Exif image metadata: the make of the camera used to take the photo. Keep string short!</extracomment>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="81"/>
        <location filename="../../qml/mainview/MetaData.qml" line="392"/>
        <source>Model</source>
        <extracomment>Exif image metadata: the model of the camera used to take the photo. Keep string short!</extracomment>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="83"/>
        <location filename="../../qml/mainview/MetaData.qml" line="394"/>
        <source>Software</source>
        <extracomment>Exif image metadata: the software used to create the photo. Keep string short!</extracomment>
        <translation>Software</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="85"/>
        <location filename="../../qml/mainview/MetaData.qml" line="397"/>
        <source>Time Photo was Taken</source>
        <extracomment>Exif image metadata: when the photo was taken. Keep string short!</extracomment>
        <translation>Aufnahmezeit</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="87"/>
        <location filename="../../qml/mainview/MetaData.qml" line="399"/>
        <source>Exposure Time</source>
        <extracomment>Exif image metadata: how long the sensor was exposed to the light. Keep string short!</extracomment>
        <translation>Belichtungszeit</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="89"/>
        <location filename="../../qml/mainview/MetaData.qml" line="401"/>
        <source>Flash</source>
        <extracomment>Exif image metadata: the flash setting when the photo was taken. Keep string short!</extracomment>
        <translation>Blitz</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="92"/>
        <location filename="../../qml/mainview/MetaData.qml" line="404"/>
        <source>Scene Type</source>
        <extracomment>Exif image metadata: the specific scene type the camera used for the photo. Keep string short!</extracomment>
        <translation>Szenentyp</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="94"/>
        <location filename="../../qml/mainview/MetaData.qml" line="406"/>
        <source>Focal Length</source>
        <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/Focal_length . Keep string short!</extracomment>
        <translation>Brennweite</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="96"/>
        <source>F-Number</source>
        <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/F-number . Keep string short!</extracomment>
        <translation>Blende</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="98"/>
        <location filename="../../qml/mainview/MetaData.qml" line="410"/>
        <source>Light Source</source>
        <extracomment>Exif image metadata: What type of light the camera detected. Keep string short!</extracomment>
        <translation>Lichtquelle</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="100"/>
        <location filename="../../qml/mainview/MetaData.qml" line="413"/>
        <source>Keywords</source>
        <extracomment>IPTC image metadata: A description of the image by the user/software. Keep string short!</extracomment>
        <translation>Schlüsselwörter</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="102"/>
        <location filename="../../qml/mainview/MetaData.qml" line="415"/>
        <source>Location</source>
        <extracomment>IPTC image metadata: The CITY the imge was taken in. Keep string short!</extracomment>
        <translation>Ort</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="104"/>
        <location filename="../../qml/mainview/MetaData.qml" line="417"/>
        <source>Copyright</source>
        <extracomment>IPTC image metadata. Keep string short!</extracomment>
        <translation>Urheberrecht</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="106"/>
        <location filename="../../qml/mainview/MetaData.qml" line="420"/>
        <source>GPS Position</source>
        <extracomment>Exif image metadata. Keep string short!</extracomment>
        <translation>GPS-Position</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="408"/>
        <source>F Number</source>
        <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/F-number . Keep string short!</extracomment>
        <translation>Blende</translation>
    </message>
</context>
</TS>