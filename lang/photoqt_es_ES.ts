<?xml version="1.0" ?><!DOCTYPE TS><TS language="es_ES" version="2.1">
<context>
    <name>About</name>
    <message>
        <location filename="../../qml/other/About.qml" line="10"/>
        <source>About PhotoQt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="36"/>
        <source>PhotoQt is a simple image viewer, designed to be good looking, highly configurable, yet easy to use and fast.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="38"/>
        <source>Another image viewer?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="39"/>
        <source>There are many good image viewers out there. But PhotoQt is a little different than all of them. Its interface is kept very simple, yet there is an abundance of settings to turn PhotoQt from AN image viewer into YOUR image viewer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="40"/>
        <source>Occasionally someone comes along because they think PhotoQt is &apos;like Picasa&apos;. However, if you take a good look at it then you see that they are in fact quite different. I myself have never used Picasa, and don&apos;t have any intention to copy Picasa. With PhotoQt I want to do my own thing, and to do that as good as I can.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="42"/>
        <source>So then, who are you?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="47"/>
        <source>If you find a bug or if you have a question or suggestion, please tell me. I&apos;m open to any feedback I get!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="99"/>
        <source>If you want to support PhotoQt with a donation, you can do so via PayPal here:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="123"/>
        <source>Okay, take me back</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="45"/>
        <source>Don&apos;t forget to check out the website:</source>
        <translation>No olvides pasar por el sitio web:</translation>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="43"/>
        <source>I am Lukas Spies, the sole developer of PhotoQt. Born and raised in the southwest of Germany, I left my home country for university shortly after finishing school. Since then I have live for some years in Ireland, Canada, USA, and France, studying and doing research in Mathematics and Scientific Computing. I started playing around with programming since I was about 15 years old. So most of my programming knowledge is self-taught through books and websites. The past few years of my studies I also did a lot of programming as part of my research. Through all of that I gained a good bit of experience in programming using different programming languages. This becomes especially apparent when looking at how PhotoQt has changed since it started at the end of 2011.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="82"/>
        <source>Thanks to everybody who contributed to PhotoQt and/or translated PhotoQt to another language! You guys rock!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/About.qml" line="99"/>
        <source>You want to join the team and do something, e.g. translating PhotoQt to another language? Drop me and email (%1), and for translations, check the project page on Transifex:</source>
        <extracomment>Don't forget to add the %1 in your translation, it is a placeholder for the email address!!</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Animation</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="18"/>
        <source>Animation and Window Geometry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="19"/>
        <source>Save and restore of Window Geometry: On quitting PhotoQt, it stores the size and position of the window and can restore it the next time started.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="19"/>
        <source>Keep PhotoQt above all other windows at all time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="19"/>
        <source>There are three things that can be adjusted here:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="19"/>
        <source>Animations of elements and items (like fade-in, etc.)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="33"/>
        <source>Enable Animations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="41"/>
        <source>Save and restore window geometry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Animation.qml" line="49"/>
        <source>Keep above other windows</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Available</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Available.qml" line="104"/>
        <source>Click to add shortcut</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Background</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="18"/>
        <source>Background</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="19"/>
        <source>The background of PhotoQt is the part, that is not covered by an image. It can be made either real (half-)transparent (using a compositor), or faked transparent (instead of the actual desktop a screenshot of it is shown), or a custom background image can be set, or none of the above. Please note: Fake transparency currently only really works when PhotoQt is run in fullscreen/maximised!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="37"/>
        <source>(Half-)Transparent background</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="43"/>
        <source>Faked transparency</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="48"/>
        <source>Custom background image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="53"/>
        <source>Monochrome, non-transparent background</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="99"/>
        <source>No image selected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="124"/>
        <source>Scale to fit</source>
        <extracomment>Refers to a background image, scale it to fit</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="131"/>
        <source>Scale and Crop to fit</source>
        <extracomment>Refers to a background image, crop and scale it to fit perfectly</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="137"/>
        <source>Stretch to fit</source>
        <extracomment>Refers to a background image, stretch it to fit perfectly</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="143"/>
        <source>Center image</source>
        <extracomment>Refers to a background image, center it</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Background.qml" line="149"/>
        <source>Tile image</source>
        <extracomment>Refers to a background image, tile it to fill everything</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/Background.qml" line="67"/>
        <source>Open a file to begin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/Background.qml" line="83"/>
        <source>Folder is now empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/Background.qml" line="99"/>
        <source>No image matches selected filter</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BorderAroundImage</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/BorderAroundImage.qml" line="18"/>
        <source>Margin Around Image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/BorderAroundImage.qml" line="19"/>
        <source>Whenever you load an image, the image is per default not shown completely in fullscreen, i.e. it&apos;s not stretching from screen edge to screen edge. Instead there is a small margin around the image of a couple pixels. Here you can adjust the width of this margin (set to 0 to disable it).</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BreadCrumbs</name>
    <message>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="60"/>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="68"/>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="109"/>
        <source>Go backwards in history</source>
        <extracomment>The history is the list of visited folders in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="69"/>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="101"/>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="110"/>
        <source>Go forwards in history</source>
        <extracomment>The history is the list of visited folders in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="141"/>
        <source>Close element</source>
        <extracomment>The element in this case is the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/BreadCrumbs.qml" line="228"/>
        <source>Go directly to subfolder of</source>
        <extracomment>Used as in &quot;Go directly to subfolder of '/path/to/somewhere'&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Cache</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="17"/>
        <source>Thumbnail Cache</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="22"/>
        <source>2) Database Caching</source>
        <extracomment>This refers to a type of cache for the thumbnails</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="24"/>
        <source>Both ways have their advantages and disadvantages:</source>
        <extracomment>The two ways are the two types of thumbnail caching (files and database)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="18"/>
        <source>Thumbnails can be cached in two different ways:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="20"/>
        <source>1) File Caching (following the freedesktop.org standard)</source>
        <extracomment>This refers to a type of cache for the thumbnails</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="26"/>
        <source>File Caching is done according to the freedesktop.org standard and thus different applications can share the same thumbnail for the same image file.</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="28"/>
        <source>Database Caching doesn&apos;t have the advantage of sharing thumbnails with other applications (and thus every thumbnails has to be newly created for PhotoQt), but it allows PhotoQt to have more control over existing thumbnails and works better on systems like Windows.</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="30"/>
        <source>PhotoQt works with either option, though the first way is set as default and strongly recommended.</source>
        <extracomment>The options talked about are the two ways to cache thumbnails (files and database)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="32"/>
        <source>Although everybody is encouraged to use at least one of the two options, caching can be completely disabled altogether. However, this means that each thumbnail has to be recreated everytime it is needed.</source>
        <extracomment>Talking about thumbnail caching with its two possible options, files and database caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="49"/>
        <source>Enable Cache</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="78"/>
        <source>File Caching</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="85"/>
        <source>Database Caching</source>
        <extracomment>The caching here refers to thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="115"/>
        <source>Database filesize:</source>
        <extracomment>The database refers to the database used for caching thumbnail images</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="145"/>
        <source>Entries in database:</source>
        <extracomment>The database refers to the database used for caching thumbnail images (the entries)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="173"/>
        <source>CLEAN UP</source>
        <extracomment>Refers to cleaning up the database for thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Cache.qml" line="187"/>
        <source>ERASE</source>
        <extracomment>Refers to emptying the database for thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CenterOn</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/CenterOn.qml" line="18"/>
        <source>Keep in Center</source>
        <extracomment>the center is the center of the screen edge. The thing talked about are the thumbnails.</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/CenterOn.qml" line="19"/>
        <source>If this option is set, then the current thumbnail (i.e., the thumbnail of the currently displayed image) will always be kept in the center of the thumbnail bar (if possible). If this option is not set, then the active thumbnail will simply be kept visible, but not necessarily in the center.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/CenterOn.qml" line="29"/>
        <source>Center on Current Thumbnail</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CloseOnClick</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/CloseOnClick.qml" line="17"/>
        <source>Click on Empty Area</source>
        <extracomment>The empty area is the area around the main image</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/CloseOnClick.qml" line="18"/>
        <source>This option makes PhotoQt behave a bit like the JavaScript image viewers you find on many websites. A click outside of the image on the empty background will close the application. This way PhotoQt will feel even more like a &apos;floating layer&apos;, however, this can easily be triggered accidentally. Note that if you use a mouse click for a shortcut already, then this option wont have any effect!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/CloseOnClick.qml" line="28"/>
        <source>Close on click in empty area</source>
        <extracomment>The empty area is the area around the main image</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ClosingX</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="19"/>
        <source>Exit button (&apos;x&apos; in top right corner)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="20"/>
        <source>There are two looks for the exit button: a normal &apos;x&apos; or a plain text&apos;x&apos;. The normal &apos;x&apos; fits in better with the overall design of PhotoQt, but the plain text &apos;x&apos; is smaller and more discreet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="36"/>
        <source>Normal</source>
        <extracomment>This is a type of exit button ('x' in top right screen corner)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="42"/>
        <source>Plain</source>
        <extracomment>This is a type of exit button ('x' in top right screen corner), showing a simple text 'x'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="59"/>
        <source>Small Size</source>
        <extracomment>The size of the exit button ('x' in top right screen corner)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ClosingX.qml" line="83"/>
        <source>Large Size</source>
        <extracomment>The size of the exit button ('x' in top right screen corner)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/ClosingX.qml" line="61"/>
        <source>Show counter</source>
        <extracomment>The counter shows the position of the currently loaded image in the folder</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/ClosingX.qml" line="65"/>
        <source>Show filepath</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/ClosingX.qml" line="69"/>
        <source>Show filename</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/ClosingX.qml" line="74"/>
        <source>Show closing &apos;x&apos;</source>
        <extracomment>The clsoing 'x' is the button in the top right corner of the screen for closing PhotoQt</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Copy</name>
    <message>
        <location filename="../../qml/filemanagement/Copy.qml" line="17"/>
        <source>Use the file dialog to select a destination location.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Copy.qml" line="36"/>
        <source>Copy Image to...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CustomConfirm</name>
    <message>
        <location filename="../../qml/elements/CustomConfirm.qml" line="130"/>
        <source>Don&apos;t ask again</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CustomEntries</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="20"/>
        <source>Custom Entries in Main Menu</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="21"/>
        <source>Here you can adjust the custom entries in the main menu. You can simply drag and drop the entries, edit them, add a new one and remove an existing one.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="76"/>
        <source>Executable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="94"/>
        <source>Menu Text</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="272"/>
        <source>Click here to drag entry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="388"/>
        <source>quit</source>
        <extracomment>KEEP THIS STRING SHORT! It is displayed for external applications of main menu as an option to quit PhotoQt after executing it</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="484"/>
        <source>Add new entry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/CustomEntries.qml" line="501"/>
        <source>Set default entries</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CustomFileSelect</name>
    <message>
        <location filename="../../qml/elements/CustomFileSelect.qml" line="52"/>
        <source>Click here to select a configuration file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomFileSelect.qml" line="82"/>
        <source>Select PhotoQt config file...</source>
        <extracomment>PhotoQt config file = configuration file</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomFileSelect.qml" line="84"/>
        <source>PhotoQt Config Files</source>
        <extracomment>PhotoQt config file = configuration file</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomFileSelect.qml" line="85"/>
        <source>All Files</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CustomLineEdit</name>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="89"/>
        <source>Undo</source>
        <extracomment>As in 'Undo latest change'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="92"/>
        <source>Redo</source>
        <extracomment>As in 'Redo latest change'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="97"/>
        <source>Cut selection</source>
        <extracomment>selection = selected text</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="100"/>
        <source>Copy selection to clipboard</source>
        <extracomment>selection = selected text</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="102"/>
        <source>Paste clipboard content</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="105"/>
        <source>Delete content</source>
        <extracomment>content refers to text content in a line edit</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="110"/>
        <source>Select all</source>
        <extracomment>Refering to all text</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/CustomLineEdit.qml" line="113"/>
        <source>Select all and copy</source>
        <extracomment>In the sense of 'Selecting all text and copying it to clipboard'</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Delete</name>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="20"/>
        <source>Delete File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="42"/>
        <source>Do you really want to delete this file?</source>
        <translation>¿Realmente deseas eliminar este fichero?</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="73"/>
        <source>Move to Trash</source>
        <extracomment>In the sense of 'move the current image into the trash'</extracomment>
        <translation>Mover a la papelera</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="75"/>
        <source>Delete</source>
        <extracomment>As in 'Delete the current image'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="84"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="106"/>
        <source>Delete permanently</source>
        <extracomment>In the sense of 'Delete the current image permanently'</extracomment>
        <translation>Borrar permanentemente</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="124"/>
        <source>Enter = Move to Trash, Shift+Enter = Delete permanently, Escape = Cancel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Delete.qml" line="125"/>
        <source>Enter = Delete, Escape = Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>DetectShortcut</name>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="139"/>
        <source>Shortcut also already set for the following:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="189"/>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="218"/>
        <source>Perform any mouse action or press any key combination.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="219"/>
        <source>When your satisfied, click the button to the right.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/DetectShortcut.qml" line="242"/>
        <source>Ok, set shortcut</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Disable</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Disable.qml" line="17"/>
        <source>Disable thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Disable.qml" line="18"/>
        <source>If you just don&apos;t need or don&apos;t want any thumbnails whatsoever, then you can disable them here completely. This will increase the speed of PhotoQt, but will make navigating with the mouse harder.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Disable.qml" line="29"/>
        <source>Disable Thumbnails altogether</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Enlightenment</name>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="32"/>
        <source>Warning: It seems that the &apos;msgbus&apos; (DBUS) module is not activated! It can be activated in the settings console:</source>
        <extracomment>&quot;msgbus&quot; and &quot;DBUS&quot; are fixed names, please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="44"/>
        <source>Warning: &apos;enlightenment_remote&apos; doesn&apos;t seem to be available! Are you sure Enlightenment is installed?</source>
        <extracomment>&quot;enlightenment_remote&quot; and &quot;Enlightenment&quot; are fixed names, please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="55"/>
        <source>The wallpaper can be set to any of the available monitors (one or any combination).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="72"/>
        <source>Screen</source>
        <extracomment>Used as in 'Screen #4', the screen is not referring to multiple desktops/workspaces, but actual (physical) screens</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="107"/>
        <source>You can set the wallpaper to any sub-selection of workspaces</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="128"/>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="131"/>
        <location filename="../../qml/wallpaper/modules/Enlightenment.qml" line="133"/>
        <source>Workspace</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ExportImport</name>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="65"/>
        <source>Export/Import settings and shortcuts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="75"/>
        <source>Here you can export all settings and shortcuts into a single packed file and, e.g., import it in another installation of PhotoQt.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="93"/>
        <source>Export everything to file</source>
        <extracomment>Everything refers to all settings and shortcuts</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="129"/>
        <source>Import settings and shortcuts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="153"/>
        <source>PhotoQt will attempt to automatically restart after a successful import!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="167"/>
        <source>I don&apos;t want to do this</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="180"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="184"/>
        <source>Exporting the configuration file failed with the following error message:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="185"/>
        <source>Importing the configuration file failed with the following error message:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/ExportImport.qml" line="186"/>
        <source>Oh, okay</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FileTypesExtras</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="19"/>
        <source>File Formats</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="21"/>
        <source>Extras</source>
        <extracomment>These are extra (special) file formats</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="22"/>
        <source>The following filetypes are supported by means of other third party tools. You first need to install them before you can use them.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="23"/>
        <source>Please note that if an image format is also provided by GraphicsMagick/Qt, then PhotoQt first chooses the external tool (if enabled).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="81"/>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesExtras.qml" line="83"/>
        <source>Makes use of</source>
        <extracomment>Used as in 'Makes use of tool abc'</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FileTypesGM</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGM.qml" line="19"/>
        <source>File Formats</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGM.qml" line="21"/>
        <source>disabled</source>
        <extracomment>Used as in 'disabled category'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGM.qml" line="23"/>
        <source>PhotoQt makes use of GraphicsMagick for support of many different image formats. The list below are all those formats, that were successfully displayed using test images. There are a few formats, that were not tested in PhotoQt (due to lack of a test image). You can find those in the &apos;Untested&apos; category below.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGM.qml" line="24"/>
        <source>PhotoQt was built without GraphicsMagick support!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FileTypesGMGhostscript</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGMGhostscript.qml" line="19"/>
        <source>File Formats</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGMGhostscript.qml" line="21"/>
        <source>disabled</source>
        <extracomment>Used as in 'disabled category'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGMGhostscript.qml" line="23"/>
        <source>The following file types are supported by GraphicsMagick, and they have been tested and should work. However, they require Ghostscript to be installed on the system.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesGMGhostscript.qml" line="24"/>
        <source>PhotoQt was built without GraphicsMagick support!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FileTypesQt</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesQt.qml" line="19"/>
        <source>File Formats</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesQt.qml" line="20"/>
        <source>These are the file types natively supported by Qt. Make sure, that you&apos;ll have the required libraries installed (e.g., qt5-imageformats), otherwise some of them might not work on your system.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FileTypesRaw</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesRaw.qml" line="19"/>
        <source>File Formats</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesRaw.qml" line="21"/>
        <source>disabled</source>
        <extracomment>As in 'disabled category'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesRaw.qml" line="23"/>
        <source>PhotoQt can open and display most raw image formats. Here you can adjust the list of fileformats known to PhotoQt.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesRaw.qml" line="24"/>
        <source>PhotoQt was built without LibRaw support!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FileTypesUntested</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="19"/>
        <source>File Formats</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="21"/>
        <source>Untested</source>
        <extracomment>As in 'Untested file formats'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="23"/>
        <source>disabled</source>
        <extracomment>As in 'disabled category'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="25"/>
        <source>These are some file types that are supported by GraphicMagick, but have not been tested in PhotoQt. They might work, but no guarantee can be given!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/fileformats/FileTypesUntested.qml" line="26"/>
        <source>PhotoQt was built without GraphicsMagick support!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FilenameOnly</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/FilenameOnly.qml" line="19"/>
        <source>Filename Thumbnail</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/FilenameOnly.qml" line="20"/>
        <source>If you don&apos;t want PhotoQt to always load the actual image thumbnail in the background, but you still want to have something for better navigating, then you can set a filename-only thumbnail, i.e. PhotoQt wont load any thumbnail images but simply puts the file name into the box. You can also adjust the font size of this text.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/FilenameOnly.qml" line="34"/>
        <source>Use filename-only thumbnail</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/FilenameOnly.qml" line="46"/>
        <source>Fontsize</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FilesView</name>
    <message>
        <location filename="../../qml/openfile/FilesView.qml" line="96"/>
        <source>This protocol is currently not supported</source>
        <extracomment>Protocol refers to a file protocol (e.g., for network folders)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/FilesView.qml" line="98"/>
        <source>No image files found</source>
        <extracomment>Can also be expressed as 'zero subfolders' or '0 subfolders'. It is also possible to drop the 'sub' leaving 'folders' if that works better</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/FilesView.qml" line="353"/>
        <source>Name</source>
        <extracomment>Refers to the filename. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/FilesView.qml" line="355"/>
        <source>Size</source>
        <extracomment>Refers to the filesize. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Filter</name>
    <message>
        <location filename="../../qml/other/Filter.qml" line="21"/>
        <source>Filter images in current directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="36"/>
        <source>Enter here the term you want to filter the images by. Separate multiple terms by a space.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="43"/>
        <source>If you want to limit a term to file extensions, prepend a dot &apos;.&apos; to the term.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="92"/>
        <source>Filter</source>
        <extracomment>As in 'Go ahead and filter images'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="99"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../../qml/other/Filter.qml" line="113"/>
        <source>Remove Filter</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FitInWindow</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/FitInWindow.qml" line="17"/>
        <source>Fit in Window</source>
        <extracomment>Along the lines of 'zoom small images until they fill the window'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/FitInWindow.qml" line="18"/>
        <source>If the image dimensions are smaller than the screen dimensions, PhotoQt can automatically zoom those images to make them fit into the window.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/FitInWindow.qml" line="27"/>
        <source>Fit Smaller Images in Window</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Folders</name>
    <message>
        <location filename="../../qml/openfile/Folders.qml" line="73"/>
        <source>This protocol is currently not supported</source>
        <extracomment>Protocol refers to a file protocol (e.g., for network folders)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/Folders.qml" line="75"/>
        <source>No subfolders</source>
        <extracomment>Can also be expressed as 'zero subfolders' or '0 subfolders'. It is also possible to drop the 'sub' leaving 'folders' if that works better</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/Folders.qml" line="212"/>
        <source>image</source>
        <extracomment>Used as in '(1 image)'. This string is always used for the singular, exactly one image</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/Folders.qml" line="214"/>
        <source>images</source>
        <extracomment>Used as in '(11 images)'. This string is always used for multiple images (at least 2)</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FontSize</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/FontSize.qml" line="17"/>
        <source>Font Size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/FontSize.qml" line="18"/>
        <source>The fontsize of the metadata element can be adjusted independently of the rest of the application.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GetAndDoStuffContext</name>
    <message>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="18"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="20"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="22"/>
        <source>Edit with</source>
        <extracomment>Used as in 'Edit with tool abc'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="24"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="26"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="28"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="30"/>
        <location filename="../../cplusplus/scripts/getanddostuff/context.cpp" line="32"/>
        <source>Open in</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GetAndDoStuffFile</name>
    <message>
        <location filename="../../cplusplus/scripts/getanddostuff/file.cpp" line="8"/>
        <source>Please select image file</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GetMetaData</name>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="391"/>
        <source>Unknown</source>
        <translation>Desconocida</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="394"/>
        <source>Daylight</source>
        <translation>Luz del día</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="397"/>
        <source>Fluorescent</source>
        <translation>Fluorescente</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="400"/>
        <source>Tungsten (incandescent light)</source>
        <translation>Tungsteno (luz incandescente)</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="403"/>
        <source>Flash</source>
        <translation>Flash</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="406"/>
        <source>Fine weather</source>
        <translation>Clima Normal</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="409"/>
        <source>Cloudy Weather</source>
        <translation>Día Nublado</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="412"/>
        <source>Shade</source>
        <translation>Sombra</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="415"/>
        <source>Daylight fluorescent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="418"/>
        <source>Day white fluorescent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="421"/>
        <source>Cool white fluorescent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="424"/>
        <source>White fluorescent</source>
        <translation>Fluorescente blanco</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="427"/>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="430"/>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="433"/>
        <source>Standard light</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="446"/>
        <source>Other light source</source>
        <translation>Otra fuente de luz</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="449"/>
        <source>Invalid light source</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="458"/>
        <source>yes</source>
        <extracomment>This string identifies that flash was fired, stored in image metadata</extracomment>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="460"/>
        <source>no</source>
        <extracomment>This string identifies that flash wasn't fired, stored in image metadata</extracomment>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="462"/>
        <source>No flash function</source>
        <extracomment>This string refers to the absense of a flash, stored in image metadata</extracomment>
        <translation>Sin función flash</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="464"/>
        <source>strobe return light not detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="466"/>
        <source>strobe return light detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="468"/>
        <source>compulsory flash mode</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="470"/>
        <source>auto mode</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>modo automático</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="472"/>
        <source>red-eye reduction mode</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation>modo reducción de ojos rojos</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="474"/>
        <source>return light detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="476"/>
        <source>return light not detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="524"/>
        <source>Invalid flash</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="534"/>
        <source>Standard</source>
        <translation>Estándar</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="537"/>
        <source>Landscape</source>
        <translation>Paisaje</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="540"/>
        <source>Portrait</source>
        <translation>Retrato</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="543"/>
        <source>Night Scene</source>
        <translation>Escena nocturna</translation>
    </message>
    <message>
        <location filename="../../cplusplus/scripts/getmetadata.cpp" line="546"/>
        <source>Invalid Scene Type</source>
        <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GnomeUnity</name>
    <message>
        <location filename="../../qml/wallpaper/modules/GnomeUnity.qml" line="30"/>
        <source>Warning: &apos;gsettings&apos; doesn&apos;t seem to be available! Are you sure Gnome/Unity is installed?</source>
        <extracomment>&quot;gsettings&quot;, &quot;Gnome&quot; and &quot;Unity&quot; are fixed names, please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/GnomeUnity.qml" line="41"/>
        <source>There are several picture options that can be set for the wallpaper image.</source>
        <extracomment>'picture options' refers to options like stretching the image to fill the background, or tile the image, center it, etc.</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Histogram</name>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="140"/>
        <source>Histogram</source>
        <extracomment>A histogram visualises the color distribution in an image</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="174"/>
        <source>Loading...</source>
        <extracomment>As in 'Loading the histogram for the current image'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="207"/>
        <source>Click-and-drag to move. Right click to switch version.</source>
        <extracomment>Used for the histogram. The version refers to the type of histogram that is available (colored and greyscale)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="318"/>
        <source>Click to switch between coloured and greyscale histogram. You can also switch by doing a right-click onto the histogram.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/Histogram.qml" line="362"/>
        <source>Click to hide histogram. It can always be shown again from the main menu.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>HotEdge</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/HotEdge.qml" line="19"/>
        <source>Size of &apos;Hot Edge&apos;</source>
        <extracomment>The hot edge refers to the left and right screen edge. When the mouse cursor enters the hot edge area, then the main menu/metadata element is shown</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/HotEdge.qml" line="20"/>
        <source>Here you can adjust the sensitivity of the metadata and main menu elements. The main menu opens when your mouse cursor gets close to the right screen edge, the metadata element when you go to the left screen edge. This setting controls how close to the screen edge you have to get before they are shown.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/HotEdge.qml" line="34"/>
        <source>Small</source>
        <extracomment>This refers to the size of the hot edge, you have to get very close to the screen edge to trigger the main menu or metadata element</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/HotEdge.qml" line="57"/>
        <source>Large</source>
        <extracomment>This refers to the size of the hot edge, you don't have to get very close to the screen edge to trigger the main menu or metadata element</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Imgur</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="19"/>
        <source>Here you can connect PhotoQt to your imgur.com account for uploading images directly to it. Alternatively, you can always upload images anonymously to imgur.com without any user account. In either case, PhotoQt will return the image URL to you.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="46"/>
        <source>Authenticated with account</source>
        <extracomment>Account refers to an imgur.com user account</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="57"/>
        <source>not authenticated</source>
        <extracomment>As in &quot;not authenticated with imgur.com user account&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="66"/>
        <source>authenticated on</source>
        <extracomment>As in &quot;authenticated with imgur.com user account on 1991-07-23, 13:31&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="86"/>
        <source>Connect to Account</source>
        <extracomment>Account refers to imgur.com user account</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="88"/>
        <source>Connect to New Account</source>
        <extracomment>Account refers to imgur.com user account</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="94"/>
        <source>Forget Account</source>
        <extracomment>Account refers to imgur.com user account</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="142"/>
        <source>Not connected to internet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="147"/>
        <source>Go to this URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="154"/>
        <source>loading...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="157"/>
        <source>open link</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="163"/>
        <source>Paste PIN here</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="170"/>
        <source>Connect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Imgur.qml" line="178"/>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ImgurFeedback</name>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="57"/>
        <source>Uploading image to imgur.com anonymously</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="58"/>
        <source>Uploading image to imgur.com account:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="88"/>
        <source>Cancel upload</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="128"/>
        <source>Obtaining image url</source>
        <extracomment>The image url is the url returned from imgur.com after uploading</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="162"/>
        <source>Please wait!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="175"/>
        <source>This seems to take a long time... If it doesn&apos;t work, then there might be a problem with the imgur.com servers at the moment. In that case the only solution is to try again at some later point!</source>
        <extracomment>Refers to uploading an image to imgur.com</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="197"/>
        <source>I don&apos;t want to know it!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="241"/>
        <source>An Error occured while uploading image!</source>
        <extracomment>Refers to uploading an image to imgur.com</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="242"/>
        <source>Error code:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="249"/>
        <location filename="../../qml/other/ImgurFeedback.qml" line="295"/>
        <source>Oh, ok, got it!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="288"/>
        <source>You don&apos;t seem to be able to be connected to the internet... Unable to upload!</source>
        <extracomment>Refers to uploading an image to imgur.com</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="334"/>
        <source>Image successfully uploaded!</source>
        <extracomment>Refers to uploading an image to imgur.com</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="355"/>
        <source>URL for accessing image</source>
        <extracomment>The url is returned from imgur.com after uploading</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="367"/>
        <location filename="../../qml/other/ImgurFeedback.qml" line="403"/>
        <source>visit link</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="391"/>
        <source>URL for deleting image</source>
        <extracomment>The url is returned from imgur.com after uploading</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ImgurFeedback.qml" line="413"/>
        <source>Got it!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Interpolation</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Interpolation.qml" line="19"/>
        <source>Interpolation</source>
        <extracomment>The type of interpolation to use for small images</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Interpolation.qml" line="20"/>
        <source>There are many different interpolation algorithms out there. Depending on the choice of interpolation algorithm, the image (when zoomed in) will look slightly differently. PhotoQt uses mipmaps to get the best quality for images. However, for very small images, that might lead to too much blurring causing them to look rather ugly. For those images, the &apos;Nearest Neighbour&apos; algorithm tends to be a better choice. The threshold defines for which images to use which algorithm.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Interpolation.qml" line="35"/>
        <source>Threshold:</source>
        <extracomment>When to trigger an action, below which threshold</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Interpolation.qml" line="68"/>
        <source>Use &apos;Nearest Neighbour&apos; algorithm for upscaling</source>
        <extracomment>'Nearest Neighbour' is the name of a specific algorithm</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KDE4</name>
    <message>
        <location filename="../../qml/wallpaper/modules/KDE4.qml" line="23"/>
        <source>Sorry, KDE4 doesn&apos;t offer the feature to change the wallpaper except from their own system settings. Unfortunately there&apos;s nothing I can do about that.</source>
        <extracomment>&quot;KDE4&quot; is a fixed name, please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Keep</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Keep.qml" line="17"/>
        <source>Keep between images</source>
        <extracomment>Refers to keeping zoom/rotation/flip/position when switching images</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Keep.qml" line="18"/>
        <source>By default, PhotoQt resets the zoom, rotation, flipping/mirroring and position when switching to a different image. For certain tasks, for example for comparing two images, it can be helpful to keep these properties.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Keep.qml" line="31"/>
        <source>Keep Zoom, Rotation, Flip, Position</source>
        <extracomment>Remember all these levels when switching between images</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>KeepVisible</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/KeepVisible.qml" line="16"/>
        <source>Visibility</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/KeepVisible.qml" line="17"/>
        <source>The thumbnails normally fade out when not needed, however, they can be set to stay visible. The main image is shrunk to fit into the free space. When it is zoomed in the thumbnails can be set to fade out automatically.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/KeepVisible.qml" line="48"/>
        <source>Keep thumbnails visible as long as the main image is not zoomed in</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/KeepVisible.qml" line="34"/>
        <source>Keep thumbnails visible, don&apos;t hide them past screen edge</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Label.qml" line="19"/>
        <source>Label on Thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Label.qml" line="20"/>
        <source>PhotoQt can write a label with some information on the thumbnails. Currently, only the filename is available. The slider adjusts the fontsize of the text for the filename.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Label.qml" line="36"/>
        <source>Write Filename</source>
        <extracomment>Settings: Write the filename on a thumbnail</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Label.qml" line="49"/>
        <source>Fontsize</source>
        <extracomment>Settings: Write the filename with this fontsize on a thumbnail</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Language</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Language.qml" line="19"/>
        <source>Language</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/other/Language.qml" line="20"/>
        <source>There are a good few different languages available. Thanks to everybody who took the time to translate PhotoQt!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>LiftUp</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/LiftUp.qml" line="19"/>
        <source>Lift-Up of Thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/LiftUp.qml" line="20"/>
        <source>When a thumbnail is hovered, it is lifted up some pixels. Here you can increase/decrease this value according to your personal preference.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>LoadingIndicator</name>
    <message>
        <location filename="../../qml/mainview/LoadingIndicator.qml" line="98"/>
        <source>Loading</source>
        <extracomment>Used as in 'Loading the image at the moment'. Please try to keep as short as possible!</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Loop</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Loop.qml" line="17"/>
        <source>Looping</source>
        <extracomment>Refers to looping through the folder, i.e., from the last image go back to the first one (and vice versa)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Loop.qml" line="18"/>
        <source>PhotoQt can loop over the images in the folder, i.e., when reaching the last image it continues to the first one and vice versa. If disabled, it will stop at the first/last image.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Loop.qml" line="27"/>
        <source>Loop through images in folder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainHandler</name>
    <message>
        <location filename="../../cplusplus/mainhandler.cpp" line="133"/>
        <location filename="../../cplusplus/mainhandler.cpp" line="363"/>
        <source>Image Viewer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/mainhandler.cpp" line="368"/>
        <source>Hide/Show PhotoQt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../cplusplus/mainhandler.cpp" line="370"/>
        <source>Quit PhotoQt</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="36"/>
        <source>Open File</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Abrir Fichero</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="38"/>
        <source>Settings</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="40"/>
        <source>Set as Wallpaper</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Establecer como Fondo de pantalla</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="42"/>
        <source>Slideshow</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="44"/>
        <source>setup</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'setting up a slideshow'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="46"/>
        <source>quickstart</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'quickstarting a slideshow'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="48"/>
        <source>Filter Images in Folder</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="50"/>
        <source>Show/Hide Metadata</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="52"/>
        <source>Show/Hide Histogram</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="54"/>
        <source>About PhotoQt</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Acerca de PhotoQt</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="56"/>
        <source>Hide (System Tray)</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Ocular (Bandeja del Sistema)</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="58"/>
        <source>Quit</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation>Terminar</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="63"/>
        <source>Go to</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Go To some image'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="65"/>
        <source>previous</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'go to previous image'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="67"/>
        <source>next</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'go to next image'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="69"/>
        <source>first</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'go to first image'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="71"/>
        <source>last</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'go to last image'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="73"/>
        <source>Zoom</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Zoom image'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="85"/>
        <location filename="../../qml/mainview/MainMenu.qml" line="93"/>
        <source>reset</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Reset rotation of image'. Keep short!
----------
This is an entry in the main menu on the right, used as in 'Reset flip/mirror of image'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="79"/>
        <source>Rotate</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Rotate image'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="81"/>
        <source>left</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Rotate image left'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="83"/>
        <source>right</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Rotate image right'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="87"/>
        <source>Flip</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Flip/Mirror image'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="89"/>
        <source>horizontal</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Flip/Mirror image horizontally'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="91"/>
        <source>vertical</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'Flip/Mirror image vertically'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="95"/>
        <source>File</source>
        <extracomment>This is an entry in the main menu on the right, used to refer to the current file (specifically the file, not directly the image). Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="97"/>
        <source>rename</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'rename file'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="99"/>
        <source>copy</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'copy file'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="101"/>
        <source>move</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'move file'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="103"/>
        <source>delete</source>
        <extracomment>This is an entry in the main menu on the right, used as in 'delete file'. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="108"/>
        <source>Scale Image</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="110"/>
        <source>Open in default file manager</source>
        <extracomment>This is an entry in the main menu on the right. Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="149"/>
        <source>Close PhotoQt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="165"/>
        <source>Main Menu</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MainMenu.qml" line="332"/>
        <source>Click here to go to the online manual for help regarding shortcuts, settings, features, ...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Management</name>
    <message>
        <location filename="../../qml/filemanagement/Management.qml" line="47"/>
        <source>Copy</source>
        <extracomment>As in 'Copy current image to a new location'. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Management.qml" line="52"/>
        <source>Delete</source>
        <extracomment>As in 'Delete current image'. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Management.qml" line="57"/>
        <source>Move</source>
        <extracomment>As in 'Move current image to a new location'. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Management.qml" line="62"/>
        <source>Rename</source>
        <extracomment>As in 'Rename current image'. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MetaData</name>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="72"/>
        <source>Metadata</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="112"/>
        <source>No File Loaded</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="138"/>
        <source>File Format Not Supported</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="164"/>
        <source>Invalid File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="227"/>
        <source>Keep Open</source>
        <extracomment>Used as in 'Keep the metadata element open even if the cursor leaves it'</extracomment>
        <translation>Mantener Abierto</translation>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="272"/>
        <source>Click to open GPS position with online map</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="19"/>
        <source>Meta Information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="20"/>
        <source>PhotoQt can display a number of meta information about the image. Here you can choose which ones to show and which ones to hide.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MouseTrigger</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MouseTrigger.qml" line="18"/>
        <source>Enable &apos;Hot Edge&apos;</source>
        <extracomment>The hot edge refers to the left and right screen edge (here in particular only to the left one). When the mouse cursor enters the hot edge area, then the metadata element is shown</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MouseTrigger.qml" line="19"/>
        <source>Per default the info widget can be shown two ways: Moving the mouse cursor to the left screen edge to fade it in temporarily (as long as the mouse is hovering it), or permanently by clicking the checkbox (checkbox only stored per session, can&apos;t be saved permanently!). Alternatively the widget can also be triggered by shortcut or main menu item. On demand the mouse triggering can be disabled, so that the widget would only show on shortcut/menu item.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MouseTrigger.qml" line="31"/>
        <source>DISABLE Hot Edge</source>
        <extracomment>The hot edge refers to the area on the very left of the screen that triggers the showing of the metadata element when the mouse enters it</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MouseWheelSensitivity</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/MouseWheelSensitivity.qml" line="18"/>
        <source>Mouse Wheel Sensitivity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/MouseWheelSensitivity.qml" line="19"/>
        <source>The mouse can be used for various things, including many types of shortcuts. The sensitivity of the mouse wheel defines the distance the wheel has to be moved before triggering a shortcut.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/MouseWheelSensitivity.qml" line="34"/>
        <source>Not at all sensitive</source>
        <extracomment>Refers to the sensitivity of the mouse wheel</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/MouseWheelSensitivity.qml" line="59"/>
        <source>Very sensitive</source>
        <extracomment>Refers to the sensitivity of the mouse wheel</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Move</name>
    <message>
        <location filename="../../qml/filemanagement/Move.qml" line="18"/>
        <source>Use the file dialog to select a destination location.</source>
        <extracomment>The destination location is a location on the computer to move a file to</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Move.qml" line="37"/>
        <source>Move Image to...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OnlineMap</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/OnlineMap.qml" line="17"/>
        <source>Online Map for GPS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/OnlineMap.qml" line="18"/>
        <source>If your image includes a GPS location, then a click on the location text will load this location in an online map using your default external browser. Here you can choose which online service to use (suggestions for other online maps always welcome).</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Opacity</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/Opacity.qml" line="17"/>
        <source>Opacity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/Opacity.qml" line="18"/>
        <source>By default, the metadata widget is overlapping the main image, thus you might prefer a different alpha value for opacity to increase/decrease readability. Values can be in the range of 0-255.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenFile</name>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="185"/>
        <source>Move focus between Places/Folders/Fileview</source>
        <extracomment>Refers to the three areas in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="187"/>
        <source>Go up/down an entry</source>
        <extracomment>Entry refers to the list of files and folders loaded in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="189"/>
        <source>Move 5 entries up/down</source>
        <extracomment>Entry refers to the list of files and folders loaded in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="191"/>
        <source>Move to the first/last entry</source>
        <extracomment>Entry refers to the list of files and folders loaded in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="193"/>
        <source>Go one folder level up</source>
        <extracomment>This refers to loading the parent folder of the currently loaded folder in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="195"/>
        <source>Go backwards/forwards in history</source>
        <extracomment>The history is the list of visited folders in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="197"/>
        <source>Load the currently highlighted item</source>
        <extracomment>Item refers to the image highlighted in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="199"/>
        <source>Zoom files in/out</source>
        <extracomment>The files is the list of files in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="201"/>
        <source>or</source>
        <extracomment>The files/folders is the list of files/folders in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="201"/>
        <source>Show/Hide hidden files/folders</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/OpenFile.qml" line="202"/>
        <source>Cancel</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Other</name>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="30"/>
        <source>Warning: &apos;feh&apos; doesn&apos;t seem to be installed!</source>
        <extracomment>&quot;feh&quot; is a fixed name (name of a tool), please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="42"/>
        <source>Warning: &apos;nitrogen&apos; doesn&apos;t seem to be installed!</source>
        <extracomment>&quot;nitrogen&quot; is a fixed name (name of a tool), please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="54"/>
        <source>Warning: Both &apos;feh&apos; and &apos;nitrogen&apos; don&apos;t seem to be installed!</source>
        <extracomment>&quot;feh&quot; and &quot;nitrogen&quot; are a fixed names (names of tools), please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="66"/>
        <source>PhotoQt can use &apos;feh&apos; or &apos;nitrogen&apos; to change the background of the desktop.</source>
        <extracomment>&quot;feh&quot; and &quot;nitrogen&quot; are a fixed names (names of tools), please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="69"/>
        <source>This is intended particularly for window managers that don&apos;t natively support wallpapers (like Blackbox, Fluxbox, or Openbox).</source>
        <extracomment>&quot;Blackbox&quot;, &quot;Fluxbox&quot; and &quot;Openbox&quot; are fixed names, please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="85"/>
        <location filename="../../qml/wallpaper/modules/Other.qml" line="92"/>
        <source>Use</source>
        <extracomment>Used as in &quot;Use 'feh'&quot; (feh is a tool)
----------
Used as in &quot;Use 'nitrogen'&quot; (nitrogen is a tool)</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OverlayColor</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="17"/>
        <source>Overlay Color</source>
        <extracomment>This refers to the background color of PhotoQt, behind the main image (the part not covered by the image itself)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="18"/>
        <source>Here you can adjust the background colour of PhotoQt (of the part not covered by an image). When using compositing or a background image, then you can also specify an alpha value, i.e. the transparency of the coloured overlay layer. When neither compositing is enabled nor a background image is set, then this colour will be the non-transparent background of PhotoQt.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="44"/>
        <source>Red:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="66"/>
        <source>Green:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="88"/>
        <source>Blue:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="111"/>
        <source>Alpha:</source>
        <extracomment>This refers to the alpha value of a color (i.e., how opaque/transparent the colour is)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/OverlayColor.qml" line="176"/>
        <source>Preview</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PixmapCache</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/PixmapCache.qml" line="18"/>
        <source>Pixmap Cache</source>
        <extracomment>The pixmap cache is used to cache loaded images so they can be loaded much quicker a second time</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/PixmapCache.qml" line="19"/>
        <source>Here you can adjust the size of the pixmap cache. This cache holds the loaded image elements that have been displayed. This doesn&apos;t help when first displaying an image, but can speed up its second display significantly. On the other hand, it does increase the memory in use, up to the limit set here. If you disable the cache altogether (value of 0), then each time an image is displayed, it is loaded fresh from the harddrive.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Plasma5</name>
    <message>
        <location filename="../../qml/wallpaper/modules/Plasma5.qml" line="23"/>
        <source>Sorry, Plasma 5 doesn&apos;t yet offer the feature to change the wallpaper except from their own system settings. Hopefully this will change soon, but until then there&apos;s nothing I can do about that.</source>
        <extracomment>&quot;Plasma 5&quot; is a fixed name, please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QuickInfo</name>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="101"/>
        <source>Filter:</source>
        <extracomment>Used as in 'Filter images'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="159"/>
        <source>Show counter</source>
        <extracomment>The counter shows the position of the currently loaded image in the folder</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="163"/>
        <source>Show filepath</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="167"/>
        <source>Show filename</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/QuickInfo.qml" line="172"/>
        <source>Show closing &apos;x&apos;</source>
        <extracomment>The clsoing 'x' is the button in the top right corner of the screen for closing PhotoQt</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Quickinfo</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="16"/>
        <source>Show Quickinfo (Text Labels)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="17"/>
        <source>PhotoQt shows certain information about the current image and the folder in the top left corner of the screen. You can choose which information in particular to show there. This also includes the &apos;x&apos; for closing PhotoQt in the top right corner.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="30"/>
        <source>Counter</source>
        <extracomment>The counter shows the current image position in the folder</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="35"/>
        <source>Filepath</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="40"/>
        <source>Filename</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Quickinfo.qml" line="45"/>
        <source>Exit button (&apos;x&apos; in top right corner)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ReOpenFile</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ReOpenFile.qml" line="16"/>
        <source>Re-open last used image at startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ReOpenFile.qml" line="17"/>
        <source>At startup, you can set PhotoQt to re-open the last used image and directory. This doesn&apos;t keep any zooming/scaling/mirroring from before. If you pass an image to PhotoQt on the command line, it will always favor the passed-on image.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/ReOpenFile.qml" line="29"/>
        <source>Re-open last used image</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Rename</name>
    <message>
        <location filename="../../qml/filemanagement/Rename.qml" line="19"/>
        <source>Rename File</source>
        <translation>Renombrar Fichero</translation>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Rename.qml" line="78"/>
        <source>Save</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/filemanagement/Rename.qml" line="88"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>RotateFlip</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/RotateFlip.qml" line="17"/>
        <source>Automatic Rotate/Flip</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/RotateFlip.qml" line="18"/>
        <source>Some cameras can detect - while taking the photo - whether the camera was turned and might store this information in the image exif data. If PhotoQt finds this information, it can rotate the image accordingly or simply ignore that information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/RotateFlip.qml" line="34"/>
        <source>Never rotate/flip images</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/RotateFlip.qml" line="40"/>
        <source>Always rotate/flip images</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Scale</name>
    <message>
        <location filename="../../qml/other/Scale.qml" line="23"/>
        <source>Scale Image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="40"/>
        <source>Current Size:</source>
        <extracomment>'Size&quot; here refers to the dimensions (numbers of pixel), expressed here as 'width x height', NOT the filesize</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="76"/>
        <source>Error! Something went wrong, unable to scale image...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="107"/>
        <source>New width:</source>
        <extracomment>The width (number of pixels) of the image</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="115"/>
        <source>New height:</source>
        <extracomment>The height (number of pixels) of the image</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="192"/>
        <source>Aspect Ratio</source>
        <extracomment>This is the ratio of the image = width/height</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="230"/>
        <source>Quality</source>
        <extracomment>Refers to the quality of scaling an image</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="274"/>
        <source>Scale in place</source>
        <extracomment>Scale as in &quot;Scale image&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="290"/>
        <source>Scale into new file</source>
        <extracomment>Scale as in &quot;Scale image&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="293"/>
        <source>Save file as...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Scale.qml" line="310"/>
        <source>Don&apos;t scale</source>
        <extracomment>Scale as in &quot;scale image&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ScaleUnsupported</name>
    <message>
        <location filename="../../qml/other/ScaleUnsupported.qml" line="31"/>
        <source>Sorry, this fileformat cannot be scaled with PhotoQt yet!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/ScaleUnsupported.qml" line="42"/>
        <source>Okay, I understand</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Set</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="85"/>
        <source>quit</source>
        <extracomment>Shortcuts: KEEP THIS STRING SHORT! It is displayed for external shortcuts as an option to quit PhotoQt after executing shortcut</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="92"/>
        <source>Quit PhotoQt when executing shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="93"/>
        <source>Keep PhotoQt running when executing shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="148"/>
        <source>The command goes here</source>
        <extracomment>Shortcuts: This is the command/executable to be executed (external shortcut)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/Set.qml" line="201"/>
        <source>Delete shortcut</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SettingInfoOverlay</name>
    <message>
        <location filename="../../qml/settingsmanager/SettingInfoOverlay.qml" line="116"/>
        <source>Click to close</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SettingsManager</name>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="63"/>
        <source>Look and Feel</source>
        <extracomment>The look of PhotoQt and how it feels and behaves</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="86"/>
        <source>Thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="119"/>
        <source>Metadata</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="139"/>
        <source>Fileformats</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="160"/>
        <source>Other Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="180"/>
        <source>Shortcuts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="237"/>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="349"/>
        <source>Restore Default Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="263"/>
        <source>Exit and Discard Changes</source>
        <extracomment>Changes here refer to changes in the settings manager</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="282"/>
        <source>Save Changes and Exit</source>
        <extracomment>Changes here refer to changes in the settings manager</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="319"/>
        <source>Do you really want to clean up the database?</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="320"/>
        <source>This removes all obsolete thumbnails, thus possibly making PhotoQt a little faster.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="321"/>
        <source>This process might take a little while.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="323"/>
        <source>Yes, clean is good</source>
        <extracomment>Along the lines of &quot;Yes, clean the database for thumbnails caching&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="325"/>
        <source>No, don&apos;t have time for that</source>
        <extracomment>Along the lines of &quot;No, cleaning the database for thumbnails caching takes too long, don't do it&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="335"/>
        <source>Do you really want to ERASE the entire database?</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="352"/>
        <source>Yes, go ahead</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="368"/>
        <source>No, don&apos;t</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="338"/>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="351"/>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="366"/>
        <source>This step cannot be reversed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="340"/>
        <source>Yes, get rid of it all</source>
        <extracomment>Along the lines of &quot;Yes, empty the database for thumbnails caching&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="308"/>
        <source>Close settings manager</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="317"/>
        <source>Clean Database!</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="333"/>
        <source>Erase Database?</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="337"/>
        <source>This removes every single item from the database! This step should never really be necessary. Afterwards every thumbnail has to be re-created.</source>
        <extracomment>The database refers to the database used for thumbnail caching</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="342"/>
        <source>No, I want to keep it</source>
        <extracomment>Along the lines of &quot;No, don't empty the database for thumbnails caching, I want to keep it&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="350"/>
        <source>Are you sure you want to revert back to the default settings?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="354"/>
        <source>No, thanks</source>
        <extracomment>Used in settings manager when asking for confirmation for restoring default settings (written on button)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="364"/>
        <source>Set Default Shortcuts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="365"/>
        <source>Are you sure you want to reset the shortcuts to the default set?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="367"/>
        <source>Yes, please</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="396"/>
        <source>Go to the next tab</source>
        <extracomment>The tab refers to the tabs in the settings manager</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="398"/>
        <source>Go to the previous tab</source>
        <extracomment>The tab refers to the tabs in the settings manager</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="400"/>
        <source>Switch to tab 1 to 5</source>
        <extracomment>The tab refers to the tabs in the settings manager</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="401"/>
        <source>Save settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/SettingsManager.qml" line="402"/>
        <source>Discard settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabThumbnails.qml" line="53"/>
        <location filename="../../qml/settingsmanager/tabs/TabOther.qml" line="53"/>
        <location filename="../../qml/settingsmanager/tabs/TabMetadata.qml" line="53"/>
        <location filename="../../qml/settingsmanager/tabs/TabLookAndFeel.qml" line="54"/>
        <location filename="../../qml/settingsmanager/tabs/TabFileformats.qml" line="53"/>
        <source>Move your mouse cursor over (or click on) the different settings titles to see more information.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ShortcutNotifier</name>
    <message>
        <location filename="../../qml/elements/ShortcutNotifier.qml" line="12"/>
        <source>Shortcuts</source>
        <extracomment>Keep short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/ShortcutNotifier.qml" line="13"/>
        <source>You can use the following shortcuts for navigation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/ShortcutNotifier.qml" line="19"/>
        <source>Got it!</source>
        <extracomment>In the sense of 'I understand it'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/elements/ShortcutNotifier.qml" line="22"/>
        <source>Don&apos;t show again</source>
        <extracomment>In the sense of 'Don't show me that confirmation element again'</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ShortcutsContainer</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/shortcuts/ShortcutsContainer.qml" line="77"/>
        <source>External</source>
        <extracomment>One of the shortcuts categories</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SlideshowBar</name>
    <message>
        <location filename="../../qml/slideshow/SlideshowBar.qml" line="49"/>
        <source>Play Slideshow</source>
        <extracomment>Stays alone like that, not part of a full sentence. Written on button to allow user to play a currently paused slideshow.</extracomment>
        <translation>Iniciar Presentación</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowBar.qml" line="51"/>
        <source>Pause Slideshow</source>
        <extracomment>Stays alone like that, not part of a full sentence. Written on button to allow user to pause a currently playing slideshow.</extracomment>
        <translation>Pausar Presentación</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowBar.qml" line="66"/>
        <source>Music Volume:</source>
        <translation>Volumen de la música:</translation>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowBar.qml" line="94"/>
        <source>Quit Slideshow</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SlideshowSettings</name>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="10"/>
        <source>Slideshow Setup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="25"/>
        <source>There are several settings that can be adjusted for a slideshow, like the time between the image, if and how long the transition between the images should be, and also a music file can be specified that will be played in the background.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="32"/>
        <source>Once you have set the desired options, you can also start a slideshow the next time via &apos;Quickstart&apos;, i.e. skipping this settings window.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="43"/>
        <source>Time in between</source>
        <extracomment>This refers to the time the slideshow waits before loading the next image</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="50"/>
        <source>Adjust the time between the images, i.e., how long the slideshow will wait before loading the next image.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="91"/>
        <source>Smooth Transition</source>
        <extracomment>This refers to the transition between two images, how quickly they fade into each other (if at all)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="98"/>
        <source>Here you can set whether you want the images to fade into each other and how fast they are to do that.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="113"/>
        <source>No Transition</source>
        <extracomment>This refers to the fading between images. No transition means that the new images simply replaces the old image instantly</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="130"/>
        <source>Long Transition</source>
        <extracomment>This refers to the fading between images. A long transition means that two images fade very slowly into each other</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="143"/>
        <source>Shuffle and Loop</source>
        <extracomment>Shuffle means putting the list of all the files in the current folder into random order. Loop means that the slideshow will start again from the bginning when it reaches the last image.</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="150"/>
        <source>If you want PhotoQt to loop over all images (i.e., once it shows the last image it starts from the beginning), or if you want PhotoQt to load your images in random order, you can check either or both boxes below. Note in the case of shuffling that no image will be shown twice before every image has been shown once.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="157"/>
        <source>Loop over images</source>
        <extracomment>This means that once the last image is reaches PhotoQt will start again from the first one</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="164"/>
        <source>Shuffle images</source>
        <extracomment>This means to put the list of files into random order.</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="177"/>
        <source>Quickinfo</source>
        <extracomment>The quickinfo refers to the labels (like position in the folder, filename, closing 'x') that are normally shown on the main image</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="185"/>
        <source>Depending on your setup, PhotoQt displays some information at the top edge, like position in current directory or file path/name. Here you can disable them temporarily for the slideshow.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="191"/>
        <source>Hide Quickinfo</source>
        <extracomment>The quickinfo refers to the labels (like position in the folder, filename, closing 'x') that are normally shown on the main image</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="203"/>
        <source>Background Music</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="210"/>
        <source>Some might like to listen to some music while the slideshow is running. Here you can select a music file you want to be played in the background.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="217"/>
        <source>Enable Music</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="247"/>
        <source>Click here to select a music file...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="270"/>
        <source>Okay, lets start</source>
        <extracomment>In the sense of 'ok, save the slideshow settings and lets start with the slideshow'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="275"/>
        <source>Wait, maybe later</source>
        <extracomment>In the sense of, 'no, don't save the slideshow settings and don't start a slideshow'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="280"/>
        <source>Save changes, but don&apos;t start just yet</source>
        <extracomment>In the sense of 'ok, save the slideshow settings, but do not start a slideshow'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="307"/>
        <source>Select music file...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="308"/>
        <source>Music Files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/slideshow/SlideshowSettings.qml" line="309"/>
        <source>All Files</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SortBy</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="17"/>
        <source>Sort Images</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="19"/>
        <source>Images in the current folder can be sorted in varios ways. They can be sorted by filename, natural name (e.g., file10.jpg comes after file9.jpg and not after file1.jpg), filesize, and date, all of that both ascending or descending.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="34"/>
        <source>Sort by:</source>
        <extracomment>As in &quot;Sort the images by some criteria&quot;</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="43"/>
        <source>Name</source>
        <extracomment>Refers to the filename</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="45"/>
        <source>Natural Name</source>
        <extracomment>Sorting by natural name means file10.jpg comes after file9.jpg and not after file1.jpg</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="47"/>
        <source>Date</source>
        <extracomment>The date the file was created</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="48"/>
        <source>Filesize</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="57"/>
        <source>Ascending</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/SortBy.qml" line="65"/>
        <source>Descending</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Spacing</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Spacing.qml" line="18"/>
        <source>Spacing Between Thumbnails</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/Spacing.qml" line="19"/>
        <source>The thumbnails are shown in a row at the lower or upper edge (depending on your setup). They are lined up side by side. Per default, there&apos;s no empty space between them.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Startup</name>
    <message>
        <location filename="../../qml/other/Startup.qml" line="85"/>
        <source>Welcome to PhotoQt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="100"/>
        <source>PhotoQt was successfully installed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="101"/>
        <source>An image viewer packed with features and adjustable in every detail awaits you... Go, enjoy :-)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="102"/>
        <source>PhotoQt was successfully updated!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="103"/>
        <source>Many new features and bug fixes await you... Go, enjoy :-)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/other/Startup.qml" line="113"/>
        <source>Lets get started!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Strings</name>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="8"/>
        <source>Alt</source>
        <extracomment>Refers to a keyboard modifier</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="10"/>
        <source>Ctrl</source>
        <extracomment>Refers to a keyboard modifier</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="12"/>
        <source>Shift</source>
        <extracomment>Refers to a keyboard modifier</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="14"/>
        <source>Page Up</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="16"/>
        <source>Page Down</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="18"/>
        <source>Meta</source>
        <extracomment>Refers to the key that usually has the 'Windows' symbol on it</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="20"/>
        <source>Keypad</source>
        <extracomment>Refers to the key that triggers the number block on keyboards</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="22"/>
        <source>Escape</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="24"/>
        <source>Right</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="26"/>
        <source>Left</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="28"/>
        <source>Up</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="30"/>
        <source>Down</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="32"/>
        <source>Space</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="34"/>
        <source>Delete</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="36"/>
        <source>Backspace</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="38"/>
        <source>Home</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="40"/>
        <source>End</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="42"/>
        <source>Insert</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="44"/>
        <source>Tab</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="46"/>
        <source>Return</source>
        <extracomment>'Return' refers to the enter key of the number block - please try to make the translations of 'Return' and 'Enter' (the main button) different if possible!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="48"/>
        <source>Enter</source>
        <extracomment>'Enter' refers to the main enter key - please try to make the translations of 'Return' (in the number block) and 'Enter' different if possible!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="54"/>
        <source>Left Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="56"/>
        <source>Right Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="58"/>
        <source>Middle Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="60"/>
        <source>Wheel Up</source>
        <extracomment>Refers to the mouse wheel</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="62"/>
        <source>Wheel Down</source>
        <extracomment>Refers to the mouse wheel</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="64"/>
        <source>East</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="66"/>
        <source>South</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="68"/>
        <source>West</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/vars/Strings.qml" line="70"/>
        <source>North</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TabFileformats</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabFileformats.qml" line="43"/>
        <source>Fileformats</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TabLookAndFeel</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabLookAndFeel.qml" line="44"/>
        <source>Look and Feel</source>
        <extracomment>The look of PhotoQt and how it feels and behaves</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TabMetadata</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabMetadata.qml" line="43"/>
        <source>Image Metadata</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabMetadata.qml" line="65"/>
        <source>PhotoQt can display different information of and about each image. The element for this information is hidden on the left side of the screen and fades in when the mouse cursor gets close to the left screen edge and/or when the set shortcut is triggered. On demand, the triggering by mouse movement can be disabled by checking the box below.</source>
        <extracomment>Introduction text of metadata tab in settings manager</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TabOther</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabOther.qml" line="43"/>
        <source>Other Settings</source>
        <translation>Otras Opciones</translation>
    </message>
</context>
<context>
    <name>TabShortcuts</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="43"/>
        <source>Shortcuts</source>
        <translation>Atajos</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="75"/>
        <source>Pressing the left button of the mouse and moving it around can be used for moving an image around. If you put it to use for this purpose then any shortcut involving the left mouse button will have no effect! Note that it is not recommended to disable this if you do not have any other means to move an image around (e.g., touchscreen)!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="87"/>
        <source>Mouse: Left button click-and-move</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="52"/>
        <source>Here you can adjust the shortcuts, add new or remove existing ones, or change a key/mouse combination. The shortcuts are grouped into 4 different categories for internal commands plus a category for external commands. The boxes on the right side contain all the possible commands. To add a shortcut for one of the available functions simply click on it. This will automatically open another element where you can set the desired shortcut.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="97"/>
        <source>Set default shortcuts</source>
        <translation>Establecer atajos por defecto</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="106"/>
        <source>Navigation</source>
        <extracomment>A shortcuts category: navigating images</extracomment>
        <translation>Navegación </translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="107"/>
        <source>Open New File</source>
        <translation>Abrir nuevo fichero</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="108"/>
        <source>Filter Images in Folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="109"/>
        <source>Next Image</source>
        <translation>Siguiente Imagen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="110"/>
        <source>Previous Image</source>
        <translation>Imagen Anterior</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="111"/>
        <source>Go to first Image</source>
        <translation>Ir a la Primera Imagen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="112"/>
        <source>Go to last Image</source>
        <translation>Ir a la Última Imagen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="113"/>
        <source>Hide to System Tray (if enabled)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="114"/>
        <source>Quit PhotoQt</source>
        <translation>Salir de PhotoQt</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="122"/>
        <source>Image</source>
        <extracomment>A shortcuts category: image manipulation</extracomment>
        <translation>Imagen</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="123"/>
        <source>Zoom In</source>
        <translation>Aumentar Zoom</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="124"/>
        <source>Zoom Out</source>
        <translation>Disminuir Zoom</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="125"/>
        <source>Zoom to Actual Size</source>
        <translation>Zoom al tamaño real</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="126"/>
        <source>Reset Zoom</source>
        <translation>Resetear Zoom</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="127"/>
        <source>Rotate Right</source>
        <translation>Rotar a la Derecha</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="128"/>
        <source>Rotate Left</source>
        <translation>Rotar a la Izquierda</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="129"/>
        <source>Reset Rotation</source>
        <translation>Resetear Rotación</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="130"/>
        <source>Flip Horizontally</source>
        <translation>Invertir Horizontalmente</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="131"/>
        <source>Flip Vertically</source>
        <translation>Invertir Verticalmente</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="132"/>
        <source>Scale Image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="133"/>
        <source>Play/Pause image animation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="141"/>
        <source>File</source>
        <extracomment>A shortcuts category: file management</extracomment>
        <translation>Fichero</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="142"/>
        <source>Rename File</source>
        <translation>Renombrar Fichero</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="143"/>
        <source>Delete File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="144"/>
        <source>Delete File (without confirmation)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="145"/>
        <source>Copy File to a New Location</source>
        <translation>Copiar fichero a una nueva dirección</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="146"/>
        <source>Move File to a New Location</source>
        <translation>Mover fichero a una nueva dirección</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="147"/>
        <source>Copy Image to Clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="155"/>
        <source>Other</source>
        <extracomment>A shortcuts category: other functions</extracomment>
        <translation>Otro</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="156"/>
        <source>Hide/Show Exif Info</source>
        <translation>Ocultar/Mostrar Información Exif</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="162"/>
        <source>Show Histogram</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="163"/>
        <source>Upload to imgur.com (anonymously)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="164"/>
        <source>Upload to imgur.com user account</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="172"/>
        <source>External</source>
        <extracomment>A shortcuts category: external commands</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="245"/>
        <source/>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="157"/>
        <source>Show Settings</source>
        <translation>Configuraciones</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="158"/>
        <source>Start Slideshow</source>
        <translation>Iniciar Presentación</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="159"/>
        <source>Start Slideshow (Quickstart)</source>
        <translation>Iniciar Presentación (Inicio Rápido)</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="160"/>
        <source>About PhotoQt</source>
        <translation>Acerca de PhotoQt</translation>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabShortcuts.qml" line="161"/>
        <source>Set as Wallpaper</source>
        <translation>Establecer como Fondo de pantalla</translation>
    </message>
</context>
<context>
    <name>TabThumbnails</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/TabThumbnails.qml" line="43"/>
        <source>Thumbnails</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ThumbnailSize</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/ThumbnailSize.qml" line="18"/>
        <source>Thumbnail Size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/ThumbnailSize.qml" line="19"/>
        <source>Here you can adjust the thumbnail size. You can set it to any size between 20 and 256 pixel. Per default it is set to 80 pixel, but the optimal size depends on the screen resolution.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TopOrBottom</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/TopOrBottom.qml" line="18"/>
        <source>Top or Bottom</source>
        <extracomment>Refers to the top and bottom screen edges</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/TopOrBottom.qml" line="19"/>
        <source>Per default the bar with the thumbnails is shown at the lower screen edge. However, some might find it nice and handy to have the thumbnail bar at the upper edge.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/TopOrBottom.qml" line="36"/>
        <source>Show at lower edge</source>
        <extracomment>Edge refers to a screen edge</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/thumbnails/TopOrBottom.qml" line="44"/>
        <source>Show at upper edge</source>
        <extracomment>Edge refers to a screen edge</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Transition</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Transition.qml" line="19"/>
        <source>Smooth Transition</source>
        <extracomment>The transition refers to images fading into one another when switching between them</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Transition.qml" line="20"/>
        <source>Switching between images can be done smoothly, the new image can be set to fade into the old image. &apos;No transition&apos; means, that the previous image is simply replaced instantly by the new image.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Transition.qml" line="34"/>
        <source>No Transition</source>
        <extracomment>No transition means that images are simply replaced when switching between them, no cross-fading</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/Transition.qml" line="57"/>
        <source>Long Transition</source>
        <extracomment>A very long transition between images, they slowly fade into each other</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TransparencyMarker</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TransparencyMarker.qml" line="17"/>
        <source>Transparency Marker</source>
        <extracomment>Refers to looping through the folder, i.e., from the last image go back to the first one (and vice versa)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TransparencyMarker.qml" line="18"/>
        <source>Transparency in image viewers is often signalled by displaying a pattern of light and dark grey squares. PhotoQt can do the same, by default it will, however, show transparent areas as transparent.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TransparencyMarker.qml" line="27"/>
        <source>Show Transparency Marker</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="17"/>
        <source>Hide to Tray Icon</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="18"/>
        <source>PhotoQt can make use of a tray icon in the system tray. It can also hide to the system tray when closing it instead of quitting. It is also possible to start PhotoQt already minimised to the tray (e.g. at system startup) when called with &quot;--start-in-tray&quot;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="33"/>
        <source>No tray icon</source>
        <extracomment>The tray icon is the icon in the system tray</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="40"/>
        <source>Hide to tray icon</source>
        <extracomment>The tray icon is the icon in the system tray</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/TrayIcon.qml" line="46"/>
        <source>Show tray icon, but don&apos;t hide to it</source>
        <extracomment>The tray icon is the icon in the system tray</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TweaksFileType</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksFileType.qml" line="24"/>
        <source>All supported images</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksFileType.qml" line="26"/>
        <location filename="../../qml/openfile/tweaks/TweaksFileType.qml" line="28"/>
        <location filename="../../qml/openfile/tweaks/TweaksFileType.qml" line="30"/>
        <source>images</source>
        <extracomment>Used as in 'Qt images'
----------
Used as in 'GraphicsMagick images'
----------
Used as in 'LibRaw images'</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TweaksPreview</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksPreview.qml" line="46"/>
        <source>En-/Disable hover preview</source>
        <extracomment>The hover preview shows the image behind the files in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksPreview.qml" line="85"/>
        <source>Use HIGH QUALITY preview</source>
        <extracomment>The preview shows the image behind the files in the element for opening files, use high quality preview</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TweaksRememberLocation</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksRememberLocation.qml" line="45"/>
        <source>Remember current folder between sessions</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TweaksThumbnails</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksThumbnails.qml" line="46"/>
        <source>En-/Disable image thumbnails</source>
        <extracomment>The thumbnails in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TweaksUserPlaces</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksUserPlaces.qml" line="46"/>
        <source>Show/Hide UserPlaces</source>
        <extracomment>The thumbnails in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TweaksViewMode</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksViewMode.qml" line="43"/>
        <source>Show files as list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksViewMode.qml" line="77"/>
        <source>Show files as grid</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TweaksZoom</name>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksZoom.qml" line="19"/>
        <source>Zoom:</source>
        <extracomment>As in 'Zoom the files shown'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/tweaks/TweaksZoom.qml" line="34"/>
        <source>Move slider to adjust the size of files</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UserPlaces</name>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="735"/>
        <source>Show standard locations</source>
        <extracomment>The standard/common folders (like Home, Desktop, ...)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="739"/>
        <source>Show user locations</source>
        <extracomment>The user set folders (or favorites) in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="743"/>
        <source>Show devices</source>
        <extracomment>The storage devices (like USB keys)</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="111"/>
        <source>Home</source>
        <extracomment>This is used as name of the HOME folder</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="75"/>
        <source>No categories enabled! They can be shown on right click!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="115"/>
        <source>Desktop</source>
        <extracomment>This is used as name of the DESKTOP folder</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="119"/>
        <source>Pictures</source>
        <extracomment>This is used as name of the PICTURES folder</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="123"/>
        <source>Downloads</source>
        <extracomment>This is used as name of the DOWNLOADS folder</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="205"/>
        <source>Standard</source>
        <extracomment>This is the category title of standard/common folders (like Home, Desktop, ...) in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="285"/>
        <source>No user places set yet. You can drag and drop folders here to bookmark them.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="475"/>
        <source>Places</source>
        <extracomment>This is the category title of user set folders (or favorites) in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="527"/>
        <source>Remove entry</source>
        <extracomment>Remove an entry from the list of user places (or favorites) in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/openfile/UserPlaces.qml" line="699"/>
        <source>Storage devices</source>
        <extracomment>This is the category title of storage devices to open (like USB keys) in the element for opening files</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="11"/>
        <source>Set as Wallpaper</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="29"/>
        <source>Window Manager</source>
        <translation>Gestor de Ventanas</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="37"/>
        <source>PhotoQt tries to detect your window manager according to the environment variables set by your system. If it still got it wrong, you can change the window manager manually.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="46"/>
        <source>Other</source>
        <extracomment>'Other' as in 'Other window managers'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="167"/>
        <source>Okay, do it!</source>
        <extracomment>Along the lines of: Set image as wallpaper</extracomment>
        <translation>Ok, hazlo.</translation>
    </message>
    <message>
        <location filename="../../qml/wallpaper/Wallpaper.qml" line="173"/>
        <source>Nooo, don&apos;t!</source>
        <extracomment>Along the lines of: Don't set image as wallpaper</extracomment>
        <translation>No, no lo hagas</translation>
    </message>
</context>
<context>
    <name>WindowMode</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/WindowMode.qml" line="16"/>
        <source>Window Mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/WindowMode.qml" line="17"/>
        <source>PhotoQt can be used both in fullscreen mode or as a normal window. It was designed with a fullscreen/maximised application in mind, thus it will look best when used that way, but will work just as well any other way.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/WindowMode.qml" line="29"/>
        <source>Run PhotoQt in Window Mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/lookandfeel/WindowMode.qml" line="35"/>
        <source>Show Window Decoration</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>XFCE4</name>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="32"/>
        <source>Warning: &apos;xfconf-query&apos; doesn&apos;t seem to be available! Are you sure XFCE4 is installed?</source>
        <extracomment>&quot;xfconf-query&quot; and &quot;XFCE4&quot; are fixed names, please don't translate</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="45"/>
        <source>The wallpaper can be set to any of the available monitors (one or any combination).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="63"/>
        <source>Screen</source>
        <extracomment>Used as in 'Screen #4'</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="99"/>
        <source>There are several picture options that can be set for the wallpaper image.</source>
        <extracomment>'picture options' refers to options like stretching the image to fill the background, or tile the image, center it, etc.</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="115"/>
        <source>Automatic</source>
        <extracomment>&quot;Automatic&quot; means automatically choose how to set the image as wallpaper</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="122"/>
        <source>Centered</source>
        <extracomment>&quot;Centered&quot; means set the image centered as wallpaper</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="129"/>
        <source>Tiled</source>
        <extracomment>&quot;Tiled&quot; means repeat the wallpaper image until the full screen is covered</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="136"/>
        <source>Stretched</source>
        <extracomment>&quot;Stretched&quot; means make the wallpaper image fill the screen without regard to its aspect ratio</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="143"/>
        <source>Scaled</source>
        <extracomment>&quot;Scaled&quot; means that the wallpaper image is scaled to properly fill the screen</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/wallpaper/modules/XFCE4.qml" line="150"/>
        <source>Zoomed</source>
        <extracomment>&quot;Zoomed&quot; means that the wallpaper image is zoomed to fill the screen</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>handlestuff</name>
    <message>
        <location filename="../../qml/openfile/handlestuff.js" line="193"/>
        <source>device</source>
        <extracomment>This is used for the name of a storage device (e.g., USB), as in '5 GB device'</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>imageprovider</name>
    <message>
        <location filename="../../cplusplus/imageprovider/imageproviderthumbnail.cpp" line="41"/>
        <location filename="../../cplusplus/imageprovider/imageproviderfull.cpp" line="41"/>
        <source>File failed to load, it doesn&apos;t exist!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>metadata</name>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="71"/>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="110"/>
        <location filename="../../qml/mainview/MetaData.qml" line="363"/>
        <source>Filename</source>
        <extracomment>Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="73"/>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="112"/>
        <location filename="../../qml/mainview/MetaData.qml" line="368"/>
        <source>Filesize</source>
        <extracomment>Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="75"/>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="114"/>
        <location filename="../../qml/mainview/MetaData.qml" line="373"/>
        <source>Image</source>
        <extracomment>Used as in &quot;Image 3/16&quot;. The numbers (position of image in folder) are added on automatically. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="77"/>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="116"/>
        <location filename="../../qml/mainview/MetaData.qml" line="379"/>
        <location filename="../../qml/mainview/MetaData.qml" line="383"/>
        <source>Dimensions</source>
        <extracomment>The dimensions of the loaded image. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="79"/>
        <location filename="../../qml/mainview/MetaData.qml" line="390"/>
        <source>Make</source>
        <extracomment>Exif image metadata: the make of the camera used to take the photo. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="81"/>
        <location filename="../../qml/mainview/MetaData.qml" line="392"/>
        <source>Model</source>
        <extracomment>Exif image metadata: the model of the camera used to take the photo. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="83"/>
        <location filename="../../qml/mainview/MetaData.qml" line="394"/>
        <source>Software</source>
        <extracomment>Exif image metadata: the software used to create the photo. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="85"/>
        <location filename="../../qml/mainview/MetaData.qml" line="397"/>
        <source>Time Photo was Taken</source>
        <extracomment>Exif image metadata: when the photo was taken. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="87"/>
        <location filename="../../qml/mainview/MetaData.qml" line="399"/>
        <source>Exposure Time</source>
        <extracomment>Exif image metadata: how long the sensor was exposed to the light. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="89"/>
        <location filename="../../qml/mainview/MetaData.qml" line="401"/>
        <source>Flash</source>
        <extracomment>Exif image metadata: the flash setting when the photo was taken. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="92"/>
        <location filename="../../qml/mainview/MetaData.qml" line="404"/>
        <source>Scene Type</source>
        <extracomment>Exif image metadata: the specific scene type the camera used for the photo. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="94"/>
        <location filename="../../qml/mainview/MetaData.qml" line="406"/>
        <source>Focal Length</source>
        <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/Focal_length . Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="96"/>
        <source>F-Number</source>
        <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/F-number . Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="98"/>
        <location filename="../../qml/mainview/MetaData.qml" line="410"/>
        <source>Light Source</source>
        <extracomment>Exif image metadata: What type of light the camera detected. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="100"/>
        <location filename="../../qml/mainview/MetaData.qml" line="413"/>
        <source>Keywords</source>
        <extracomment>IPTC image metadata: A description of the image by the user/software. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="102"/>
        <location filename="../../qml/mainview/MetaData.qml" line="415"/>
        <source>Location</source>
        <extracomment>IPTC image metadata: The CITY the imge was taken in. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="104"/>
        <location filename="../../qml/mainview/MetaData.qml" line="417"/>
        <source>Copyright</source>
        <extracomment>IPTC image metadata. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/settingsmanager/tabs/metadata/MetaData.qml" line="106"/>
        <location filename="../../qml/mainview/MetaData.qml" line="420"/>
        <source>GPS Position</source>
        <extracomment>Exif image metadata. Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../../qml/mainview/MetaData.qml" line="408"/>
        <source>F Number</source>
        <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/F-number . Keep string short!</extracomment>
        <translation type="unfinished"/>
    </message>
</context>
</TS>